﻿import { Component } from 'react';
export default [
  {
    path: '/main',
    name: '首页',
    component: './main',
  },
  {
    path: '/',
    redirect: '/main',
  },
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  /*{
    path: '/welcome',
    name: 'welcome',

    component: './Welcome',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    component: './Admin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',

        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
  },*/

  // 湛江综保
  {
    name: '区域账册管理',
    path: '/accountBook',
    icon: 'AccountBook',
    routes: [
      {
        name: '物流账册',

        path: '/accountBook/bws',
        routes: [
          {
            name: '物流账册备案',

            path: '/accountBook/bws/declare',
            component: './accoutBook/bws',
          },
          {
            name: '物流账册变更',

            path: '/accountBook/bws/change',
            component: './accoutBook/bws',
          },
          {
            name: '物流账册查询',

            path: '/accountBook/bws/search',
            component: './accoutBook/bws',
          },
        ],
      },
      {
        name: '加工贸易账册',

        path: '/accountBook/ems',
        routes: [
          {
            name: '加工贸易账册备案',

            path: '/accountBook/ems/declare',
            component: './accoutBook/ems',
          },
          {
            name: '加工贸易账册变更',

            path: '/accountBook/ems/change',
            component: './accoutBook/ems',
          },
          {
            name: '加工贸易账册查询',

            path: '/accountBook/ems/search',
            component: './accoutBook/ems',
          },
        ],
      },
      {
        name: '加工贸易耗料单',

        path: '/accountBook/cmb',
        routes: [
          {
            name: '耗料单备案',

            path: '/accountBook/cmb/declare',
            component: './accoutBook/cmb',
          },
          {
            name: '耗料单变更',

            path: '/accountBook/cmb/change',
            component: './accoutBook/cmb',
          },
          {
            name: '耗料单查询',

            path: '/accountBook/cmb/search',
            component: './accoutBook/cmb',
          },
        ],
      },
      {
        name: '加工贸易账册报核',

        path: '/accountBook/dcr',
        routes: [
          {
            name: '加工贸易账册报核申报',

            path: '/accountBook/dcr/declare',
            component: './dashboard/analysis',
          },
          {
            name: '加工贸易账册报核查询',

            path: '/accountBook/dcr/change',
            component: './dashboard/analysis',
          },
        ],
      },
    ],
  },
  {
    name: '区域货物流转管理',
    path: '/cargoCirculation',
    icon: 'table',
    routes: [
      {
        name: '业务申报表',
        path: '/CargoCirculation/Apply',
        routes: [
          {
            name: '业务申报表备案',

            path: '/CargoCirculation/Apply/declare',
            component: './CargoCirculation/Apply',
          },
          {
            name: '业务申报表变更',

            path: '/CargoCirculation/Apply/change',
            component: './CargoCirculation/Apply',
          },
          {
            name: '业务申报表结案',

            path: '/CargoCirculation/Apply/finsh',
            component: './CargoCirculation/Apply',
          },
          {
            name: '业务申报表查询',

            path: '/CargoCirculation/Apply/search',
            component: './CargoCirculation/Apply',
          },
        ],
      },
      {
        name: '出入库单',
        path: '/CargoCirculation/stock',
        routes: [
          {
            name: '出入库单备案',

            path: '/CargoCirculation/stock/declare',
            component: './CargoCirculation/stock',
          },
          {
            name: '出入库单变更',

            path: '/CargoCirculation/stock/change',
            component: './CargoCirculation/stock',
          },
          {
            name: '出入库单查询',

            path: '/CargoCirculation/stock/search',
            component: './CargoCirculation/stock',
          },
        ],
      },
      {
        name: '核注清单',
        path: '/cargoCirculation/invt',
        routes: [
          {
            name: '进口核注清单',
            path: '/cargoCirculation/invt/in/declare',
            component: './cargoCirculation/invt',
          },
          {
            name: '出口核注清单',
            path: '/cargoCirculation/invt/out/declare',
            component: './cargoCirculation/invt',
          },
          {
            name: '核注清单修改',
            path: '/cargoCirculation/invt/change',
            component: './cargoCirculation/invt',
          },
          {
            name: '核注清单查询',
            path: '/cargoCirculation/invt/search',
            component: './cargoCirculation/invt',
          },
        ],
      },
      {
        name: '集报清单',
        path: 'CargoCirculation/invt/collect',
        component: './cargoCirculation/invt',
      },
      {
        name: '核放单',
        path: 'CargoCirculation/Passport',
        routes: [
          {
            name: '核放单申报',

            path: '/CargoCirculation/Passport/Declare',
            component: './dashboard/analysis',
          },
          {
            name: '核放单查询',

            path: '/CargoCirculation/Passport/Search',
            component: './dashboard/analysis',
          },
          {
            name: '两步核放单申报',

            path: '/CargoCirculation/DoublePassport/Declare',
            component: './dashboard/analysis',
          },
          {
            name: '两步核放单查询',

            path: '/CargoCirculation/DoublePassport/Search',
            component: './dashboard/analysis',
          },
          {
            name: '核放单授权',

            path: '/CargoCirculation/PassportAuth/Change',
            component: './dashboard/analysis',
          },
          {
            name: '核放单授权查询',

            path: '/CargoCirculation/PassportAuth/Search',
            component: './dashboard/analysis',
          },
        ],
      },
    ],
  },
  {
    name: '货物状态分类监管',
    path: 'CargoStatus',
    icon: 'RotateLeft',
  },
  {
    name: '企业联网监管',
    path: 'NetworkSupervision',
    icon: 'DeploymentUnit',
  },
  {
    name: '工单式核算核销',
    path: 'AccountingWwriteOff',
    icon: 'Shake',
  },
  {
    name: '大宗散货',
    path: '/bulkCargo',
    icon: 'OneToOne',
    routes: [
      {
        name: '进境入区调拨申请',
        path: 'fpdbAllocEntryHead/in',
        routes: [
          {
            name: '调拨入区申请单申报',
            path: 'declare',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨入区申请单变更',
            path: 'change',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨入区申请单作废',
            path: 'invalid',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨入区申请单查询',
            path: 'search',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
        ],
      },
      {
        name: '出区离境调拨申请',
        path: 'fpdbAllocEntryHead/out',
        routes: [
          {
            name: '调拨出区申请单申报',
            path: 'declare',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨出区申请单变更',
            path: 'change',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨出区申请单作废',
            path: 'invalid',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨出区申请单查询',
            path: 'search',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
          {
            name: '调拨出区汇总清单',
            path: 'summary',
            component: './bulkCargo/fpdbAllocEntryHead',
          },
        ],
      },
    ],
  },
  {
    name: '车辆管理',
    path: '/vehicle',
    icon: 'Car',
    routes: [
      {
        name: '车辆备案申报',
        path: '/vehicle/declare',
        component: './vehicle',
      },
      {
        name: '车辆备案查询',
        path: '/vehicle/search',
        component: './vehicle',
      },
    ],
  },
];
