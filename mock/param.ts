import { Request, Response } from 'express';

const allParameters = [
  // 功能区
  { paramType: 'functionCode', value: '516501', text: '物流区' },
  { paramType: 'functionCode', value: '516502', text: '加工区' },
  { paramType: 'functionCode', value: '516503', text: '港口二期' },

  // 区块
  { paramType: 'blockCode', value: '', text: '全部' },
  { paramType: 'blockCode', value: '01', text: '加工区' },
  { paramType: 'blockCode', value: '02', text: '物流区' },
  { paramType: 'blockCode', value: '03', text: '港口二期' },

  // 审核状态
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100100', text: '变更预录入/暂存成功' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100200', text: '变更退单' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100500', text: '变更待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100520', text: '变更转人工待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100560', text: '变更转岗' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100600', text: '变更预录入' },
  { paramType: 'approveStatus', value: 'FPDBJJRQBG100700', text: '变更作废' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100100', text: '预录入' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100200', text: '退单' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100500', text: '待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100501', text: '撤销待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100502', text: '作废' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100510', text: '退单待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100511', text: '核销待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100520', text: '转人工待审批' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100560', text: '转岗' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100600', text: '变更暂存成功' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100700', text: '作废' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100990', text: '审批通过' },
  { paramType: 'approveStatus', value: 'FPDBJJRQ100999', text: '已核销' },

  // 调拨类型
  { paramType: 'allocationType', value: '1', text: '进境入区' },
  { paramType: 'allocationType', value: '2', text: '出区离境' },

  // 单证状态
  { paramType: 'stepId', value: 'G2Invt100100', text: '预录入' },
  { paramType: 'stepId', value: 'G2Invt100200', text: '已申报' },
  { paramType: 'stepId', value: 'G2Invt100300', text: '通过' },
  { paramType: 'stepId', value: 'G2Invt100400', text: '退单' },
  { paramType: 'stepId', value: 'G2Invt100500', text: '查验' },
  { paramType: 'stepId', value: 'G2Invt100600', text: '查验通过' },
  { paramType: 'stepId', value: 'G2Invt100700', text: '转人工' },
  { paramType: 'stepId', value: 'G2Invt100900', text: '作废' },
  { paramType: 'stepId', value: 'G2Invt200100', text: '变更预录入' },
  { paramType: 'stepId', value: 'G2Invt200200', text: '变更待审批' },
  { paramType: 'stepId', value: 'G2Invt200300', text: '变更审批通过' },
  { paramType: 'stepId', value: 'G2Invt200400', text: '变更退回' },
  { paramType: 'stepId', value: 'G2Invt200700', text: '变更人工审批' },
  { paramType: 'stepId', value: 'G2Invt300200', text: '作废待审核' },
  { paramType: 'stepId', value: 'G2Invt300300', text: '作废退单' },
  { paramType: 'stepId', value: 'G2Invt300400', text: '作废转人工' },

  // 核注清单类型
  { paramType: 'bondInvtTypecd', value: '0', text: '普通清单' },
  { paramType: 'bondInvtTypecd', value: '3', text: '先入区后报关' },
  { paramType: 'bondInvtTypecd', value: '6', text: '区内流转' },
  { paramType: 'bondInvtTypecd', value: '7', text: '区港联动' },
  { paramType: 'bondInvtTypecd', value: '8', text: '保税电商' },
  { paramType: 'bondInvtTypecd', value: '9', text: '一纳成品内销' },

  // 进出口标记
  { paramType: 'ioFlag', value: 'I', text: '进口' },
  { paramType: 'ioFlag', value: 'E', text: '出口' },

  // 清单状态
  { paramType: 'invtStucd', value: '1', text: '申报' },
  { paramType: 'invtStucd', value: '3', text: '退单' },
  { paramType: 'invtStucd', value: '0', text: '审核通过' },
  { paramType: 'invtStucd', value: '03', text: '删单' },

  // 报关标志
  { paramType: 'dclcusFlag', value: '1', text: '报关' },
  { paramType: 'dclcusFlag', value: '2', text: '非报关' },

  // 报关类型
  { paramType: 'dclcusTypecd', value: '1', text: '关联报关' },
  { paramType: 'dclcusTypecd', value: '2', text: '对应报关' },

  // 报关单类型
  { paramType: 'entryType', value: '1', text: '进口报关单' },
  { paramType: 'entryType', value: '2', text: '出口报关单' },
  { paramType: 'entryType', value: '3', text: '进境备案清单' },
  { paramType: 'entryType', value: '4', text: '出境备案清单' },
  { paramType: 'entryType', value: '5', text: '进境两单一审备案清单' },
  { paramType: 'entryType', value: '6', text: '出境两单一审备案清单' },
  { paramType: 'entryType', value: '7', text: '进境备案清单（简化）' },
  { paramType: 'entryType', value: '8', text: '出境备案清单（简化）' },
  { paramType: 'entryType', value: '9', text: '转关提前进口报关单' },
  { paramType: 'entryType', value: 'A', text: '转关提前出口报关单' },
  { paramType: 'entryType', value: 'B', text: '转关提前进境备案清单' },
  { paramType: 'entryType', value: 'C', text: '转关提前出境备案清单' },
  { paramType: 'entryType', value: 'D', text: '转关提前进境备案清单（简化）' },
  { paramType: 'entryType', value: 'E', text: '转关提前出境备案清单（简化）' },
  { paramType: 'entryType', value: 'F', text: '出口二次转关单' },
  { paramType: 'entryType', value: 'e', text: '进口两步申报一次录入报关单' },
  { paramType: 'entryType', value: 'f', text: '进口两步申报一次录入备案清单' },

  // 料件/成品标志
  { paramType: 'mp', value: 'I', text: '料件' },
  { paramType: 'mp', value: 'E', text: '成品' },

  // 流转类型
  { paramType: 'transferMode', value: '0', text: '非流转类' },
  { paramType: 'transferMode', value: '1', text: '跨关区流转类' },
  { paramType: 'transferMode', value: 'A', text: '加工贸易深加工结转' },
  { paramType: 'transferMode', value: 'B', text: '加工贸易余料结转' },
  { paramType: 'transferMode', value: 'C', text: '不作价设备结转' },
  { paramType: 'transferMode', value: 'D', text: '区间深加工结转' },
  { paramType: 'transferMode', value: 'E', text: '区间料件结转' },
  { paramType: 'transferMode', value: '1', text: '流转类' },
];

export type UrlParams = {
  paramType?: string;
  values?: string[];
};
function getParameterList(req: Request, res: Response) {
  const { paramType, values } = req.query as UrlParams;

  let result;
  if (values != undefined && values.length > 0) {
    result = allParameters
      .filter((item) => item.paramType == paramType && values?.includes(item.value))
      .map((item) => ({ label: item.text, value: item.value }));
  } else {
    result = allParameters
      .filter((item) => item.paramType == paramType)
      .map((item) => ({ label: item.text, value: item.value }));
  }
  return res.json({
    success: true,
    data: result,
  });
}

function getStepParameterList(req: Request, res: Response) {
  let stepParameters = allParameters
    .filter((item) => item.paramType == 'stepId')
    .map((item) => ({ label: item.text, value: item.value }));

  const { paramType } = req.query;
  if (paramType == 'declare') {
    stepParameters = stepParameters.filter((item) =>
      ['G2Invt100100', 'G2Invt100200', 'G2Invt100300', 'G2Invt100400', 'G2Invt100700'].includes(
        item.value,
      ),
    );
  } else if (paramType == 'change') {
    stepParameters = stepParameters.filter((item) =>
      ['G2Invt100300', 'G2Invt200100', 'G2Invt200300', 'G2Invt200400', 'G2Invt300300'].includes(
        item.value,
      ),
    );
  }

  return res.json({
    success: true,
    data: stepParameters,
  });
}

export default {
  'GET /api/parameterList': getParameterList,
  'GET /api/stepParameterList': getStepParameterList,
};
