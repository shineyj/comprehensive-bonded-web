import type { Request, Response } from 'express';
import { parse } from 'url';
import type {
  TableListItem,
  TableListParams,
} from '@/services/bulkCargo/fpdbAllocEntryHead/typings';

// mock tableListDataSource
const getList = (allocationType: string, pageType: string) => {
  const tableListDataSource: TableListItem[] = [];
  let stepId = 'FPDBJJRQ100100';
  if('declare' == pageType){
    stepId = 'FPDBJJRQ100100';
  }else if('change' == pageType){
    stepId = 'FPDBJJRQBG100600';
  }else if('declare' == pageType){
    stepId = 'FPDBJJRQ100990';
  }else if('declare' == pageType){
    stepId = 'FPDBJJRQ100990';
  }
  for (let i = 0; i < 5; i++) {
    tableListDataSource.push({
      id: '100' + i,
      preEntryNo: 'FPDBJJRQ110069607521110400000' + i,
      allocationNo: 'FPDBJJRQ5165190601' + i,
      allocationType: allocationType,
      tradeName: '北京世纪周峰商业设备有限公司',
      departureBlock: '港口二期',
      destinationBlock: '加工区',
      stepId: stepId,
      createDate: '2021/11/4 9:54:04',
      declareDate: '2021/11/4 9:54:04',
      approveDate: '2021/11/4 9:54:04',
      enterpriseBooksNo: 'L0100A18A00' + i,
      mark: ''
    });
  }
  return tableListDataSource;
};



function getFpdbAllocEntryHead(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query as unknown as TableListParams;

  const allocationType = params.allocationType || '1';
  const pageType = params.pageType || 'declare';
  const tableListDataSource: TableListItem[] = getList(allocationType, pageType);
  let dataSource = [...tableListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  if (params.preEntryNo) {
    dataSource = dataSource.filter((data) => data.preEntryNo?.includes(params.preEntryNo || ''));
  }

  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: dataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/fpdbAllocEntryHead': getFpdbAllocEntryHead,
};
