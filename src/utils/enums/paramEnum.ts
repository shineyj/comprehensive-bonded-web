export enum ParamKeyEnum {
  FUNCTION_COD = 'functionCode',
  BOND_INVT_TYPECD = 'bondInvtTypecd',
  IO_FLAG = 'ioFlag',
  STEP_ID = 'stepId',
  INVT_STUCD = 'invtStucd',
  DCLCUS_FLAG = 'dclcusFlag',
  DCLCUS_TYPECD = 'dclcusTypecd',
  ENTRY_TYPE = 'entryType',
  M_P = 'mp',
  TRANSFER_MODE = 'transferMode',
}
