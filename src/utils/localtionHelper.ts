import { history } from 'umi';

export function getLastPathName(): string {
  return getLastPathNameByIndex(0);
  // const pathName = history.location.pathname;
  // return pathName.substring(pathName.lastIndexOf('/') + 1);
}

export function getLastPathNameByIndex(index: number): string {
  const pathNames = history.location.pathname.split('/').reverse();
  return pathNames[index];
}

export function getPathQueryParam(paramName: string) {
  const queryString = history.location.search.substring(1);
  const queryPairs = queryString.split('&');
  for (let i = 0; i < queryPairs.length; i++) {
    const pos = queryPairs[i].indexOf('=');
    if (pos == -1) continue;

    const name = queryPairs[i].substring(0, pos);
    if (name != paramName) continue;

    const value = queryPairs[i].substring(pos + 1);
    return decodeURIComponent(value);
  }

  return '';
}
