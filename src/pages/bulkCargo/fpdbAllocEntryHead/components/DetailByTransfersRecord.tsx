import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { useRef } from 'react';
//调拨记录
const columns: ProColumns<{}>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: 30,
    align: 'center',
  },
  {
    title: '子清单编号',
    dataIndex: 'childListNo',
    width: 180,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '核放单编号',
    dataIndex: 'allocaHfdNo',
    width: 150,
    align: 'center',
    order: 1,
  },
  {
    title: '登记编号',
    dataIndex: 'nsec',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '过卡时间',
    dataIndex: 'overCardTime',
    width: 180,
    align: 'center',
    hideInSearch: true,
  },
];

const TransfersRecord: React.FC = () => {
  const actionRef = useRef<ActionType>();
  return (
    <>
      <ProTable<{}>
        columns={columns}
        actionRef={actionRef}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          x: 1000,
        }}
        bordered
        tableAlertRender={false}
        search={false}
        toolBarRender={false}
      />
    </>
  );
};

export default TransfersRecord;
