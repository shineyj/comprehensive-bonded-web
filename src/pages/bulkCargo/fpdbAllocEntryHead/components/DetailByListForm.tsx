import { ModalForm, ProFormText } from '@ant-design/pro-form';
import {Button, Col, Row} from 'antd';
import React from 'react';
import type { TableBodyListItem } from '@/services/bulkCargo/fpdbAllocEntryHead/typings';
import {SearchOutlined} from "@ant-design/icons";

type FormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: TableBodyListItem) => Promise<void>;
  modalVisible: boolean;
  values?: Partial<TableBodyListItem>;
};

const DetailByListForm: React.FC<FormProps> = (props) => {
  return (
    <ModalForm
      title="录入申请单表体"
      width="1200px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      onFinish={props.onSubmit}
      submitter={{
        searchConfig: {
          submitText: '保存',
          resetText: '取消',
        },
      }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preEntryNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={7}>
          <ProFormText
            name="productDuty"
            label="商品货号"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={1} style={{textAlign:"right"}}>
          <Button
            icon={<SearchOutlined />}
            style={{width:"100%"}}
            onClick={() => {
              //
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="allocationNo" label="申请单编号" disabled/>
        </Col>
        <Col span={7}>
          <ProFormText
            name="productCode"
            label="商品编号"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={1} style={{textAlign:"right"}}>
          <Button
            icon={<SearchOutlined />}
            style={{width:"100%"}}
            onClick={() => {
              //
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="productName" label="商品名称" rules={[{ required: true, message: '这是必填项' }]} />
        </Col>
        <Col span={8}>
          <ProFormText name="specificationModel" label="规格型号" rules={[{ required: true, message: '这是必填项' }]} />
        </Col>
        <Col span={7}>
          <ProFormText
            name="allocaDeclareCode"
            label="申报单位"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={1} style={{textAlign:"right"}}>
          <Button
            icon={<SearchOutlined />}
            style={{width:"100%"}}
            onClick={() => {
              //
            }}
          />
        </Col>
        <Col span={7}>
          <ProFormText
            name="allocaLUnit"
            label="法定单位"
            placeholder=""
          />
        </Col>
        <Col span={1} style={{textAlign:"right"}}>
          <Button
            icon={<SearchOutlined />}
            style={{width:"100%"}}
            onClick={() => {
              //
            }}
          />
        </Col>
        <Col span={7}>
          <ProFormText
            name="allocaLUnitTwo"
            label="第二法定单位"
            placeholder=""
          />
        </Col>
        <Col span={1} style={{textAlign:"right"}}>
          <Button
            icon={<SearchOutlined />}
            style={{width:"100%"}}
            onClick={() => {
              //
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="declareAlNum" label="申请调拨数(申报)"  />
        </Col>
        <Col span={8}>
          <ProFormText name="legalAlNum" label="申请调拨数(法定)"  />
        </Col>
        <Col span={8}>
          <ProFormText name="legalAlNumTwo" label="申请调拨数(第二法定)"  />
        </Col>
        <Col span={8}>
          <ProFormText name="extendField1" label="备案序号"  />
        </Col>
        <Col span={8}>
          <ProFormText name="remark" label="备注"  />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default DetailByListForm;
