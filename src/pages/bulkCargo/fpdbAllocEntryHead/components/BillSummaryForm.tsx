import { ModalForm } from '@ant-design/pro-form';
import {Button, message, Modal} from 'antd';
const { confirm } = Modal;
import React, { useState } from 'react';
import type { ProColumns } from '@ant-design/pro-table';
import { EditableProTable } from '@ant-design/pro-table';
import {CopyOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import type { TableBodyListItem, FrmFpdbAllocEntryHeadList } from '@/services/bulkCargo/fpdbAllocEntryHead/typings';

type FormProps = {
  onCancel: (flag?: boolean) => void;
  modalVisible: boolean;
  values?: Partial<TableBodyListItem>;
};

const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

//生成核注清单确认
function genConfirm() {
  confirm({
    title: '提示',
    icon: <ExclamationCircleOutlined />,
    content: '确定要执行生成核注清单操作吗?',
    onOk() {
      console.log('OK');
      message.success('操作成功');
    },
    onCancel() {
      console.log('Cancel');
    },
  });
}

const defaultData: FrmFpdbAllocEntryHeadList[] = [
  {
    id: 1,
    allocationNo: 'FPDBJJRQ5165210400639',
    productCode: '2008199910',
    productDuty: '1',
    productName: '其他方法制作或保藏的红松子仁',
    specificationModel: '123',
    allocaDeclareCodeName: '006|套',
    allocaLegalUnitName: '035|千克',
    allocaLegalUnitTwoName: '|',
    allocaDeclareNumSj: '29',
    allocaLegalNumSj: '90',
    allocaLegalNumTwoSj: '0',
    allocaUnit: '321',
    extendField1: '1',
  },
];

//调拨出区汇总清单-清单汇总-模态窗
const BillSummaryForm: React.FC<FormProps> = (props) => {

  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [dataSource, setDataSource] = useState<FrmFpdbAllocEntryHeadList[]>([]);
  const [selectedRowsState, setSelectedRows] = useState<FrmFpdbAllocEntryHeadList[]>([]);
  const columns: ProColumns<FrmFpdbAllocEntryHeadList>[] = [
    {
      title: '申请单编号',
      dataIndex: 'allocationNo',
      editable: false,// 不允许编辑
      width: 150,
    },
    {
      title: '商品编码',
      dataIndex: 'productCode',
      editable: false,
      width: 80,
    },
    {
      title: '商品序号',
      dataIndex: 'productDuty',
      editable: false,
      width: 70,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      editable: false,
      width: 180,
    },
    {
      title: '规格型号',
      dataIndex: 'specificationModel',
      editable: false,
      width: 100,
    },
    {
      title: '申报单位',
      dataIndex: 'allocaDeclareCodeName',
      editable: false,
      width: 80,
    },
    {
      title: '法定单位',
      dataIndex: 'allocaLegalUnitName',
      editable: false,
      width: 80,
    },
    {
      title: '第二法定单位',
      dataIndex: 'allocaLegalUnitTwoName',
      editable: false,
      width: 80,
    },
    {
      title: '申请调拨数(申报)',
      dataIndex: 'allocaDeclareNumSj',
      width: 100,
    },
    {
      title: '申请调拨数(法定)',
      dataIndex: 'allocaLegalNumSj',
      width: 100,
    },
    {
      title: '申请调拨数(第二法定)',
      dataIndex: 'allocaLegalNumTwoSj',
      width: 120,
    },
    {
      title: '申报价格',
      dataIndex: 'allocaUnit',
      editable: false,
      width: 80,
    },
    {
      title: '备案序号',
      dataIndex: 'extendField1',
      editable: false,
      width: 80,
    },
    {
      title: '操作',
      valueType: 'option',
      width: 120,
      fixed: 'right',
      render: (text, record, _, action) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          编辑
        </a>,
      ],
    },
  ];

  return (
    <ModalForm
      title="清单汇总"
      width="1200px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      /*不显示确认按钮*/
      submitter={{
        resetButtonProps: {
          type: 'dashed',
        },
        submitButtonProps: {
          style: {
            display: 'none',
          },
        },
      }}
    >
      <EditableProTable<FrmFpdbAllocEntryHeadList>
        rowKey="id"
        headerTitle="表体"
        maxLength={5}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          x: 2000,
        }}
        bordered={true}
        recordCreatorProps={false}
        tableAlertRender={false}
        rowSelection={{
          onChange: (_, selectedRow) => {
            setSelectedRows(selectedRow);
          },
        }}
        toolBarRender={() => [
          <Button
            key="button"
            icon={<CopyOutlined />}
            type="primary"
            ghost
            disabled={selectedRowsState.length == 0}
            onClick={() => {
              //确认
              genConfirm();
            }}
          >
            生成核注清单
          </Button>
        ]}
        columns={columns}
        request={async () => ({
          data: defaultData,
          total: 3,
          success: true,
        })}
        value={dataSource}
        onChange={setDataSource}
        editable={{
          type: 'multiple',
          editableKeys,
          onSave: async (rowKey, data, row) => {
            console.log(rowKey, data, row);
            await waitTime(1000);
            message.success('保存成功');
          },
          onChange: setEditableRowKeys,
          /*defaultDom = {save,cancel,delete} 可以酌情添加和使用*/
          actionRender: (row, config, defaultDom) => [defaultDom.save, defaultDom.cancel],
        }}
      />
    </ModalForm>
  );
};

export default BillSummaryForm;
