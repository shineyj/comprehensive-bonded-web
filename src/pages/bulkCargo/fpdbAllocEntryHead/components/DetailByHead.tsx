import ProDescriptions from '@ant-design/pro-descriptions';
import { getParamList } from '@/services/ant-design-pro/param';

//表头
const DetailHead: React.FC = () => {
  return (
    <ProDescriptions
      title="详细信息"
      columns={[
        {
          title: '预录入号',
          key: 'preEntryNo',
          dataIndex: 'preEntryNo',
        },
        {
          title: '申请单编号',
          key: 'allocationNo',
          dataIndex: 'allocationNo',
        },
        {
          title: '经营企业编码',
          key: 'tradeCode',
          dataIndex: 'tradeCode',
        },
        {
          title: '经营企业名称',
          key: 'tradeName',
          dataIndex: 'tradeName',
        },
        {
          title: '调拨类型',
          key: 'allocationType',
          dataIndex: 'allocationType',
          valueType: 'select',
          request: () => getParamList('allocationType'),
        },
        {
          title: '启运区块',
          key: 'departureBlock',
          dataIndex: 'departureBlock',
          valueType: 'select',
          request: () => getParamList('blockCode'),
        },
        {
          title: '目的区块',
          key: 'destinationBlock',
          dataIndex: 'destinationBlock',
          valueType: 'select',
          request: () => getParamList('blockCode'),
        },
        {
          title: '账册编号',
          key: 'enterpriseBooksNo',
          dataIndex: 'enterpriseBooksNo',
        },
        {
          title: '核注清单核扣标志',
          key: 'checkListMark',
          dataIndex: 'checkListMark',
        },
        {
          title: '核销状态',
          key: 'hxState',
          dataIndex: 'hxState',
        },
        {
          title: '申报企业编码',
          key: 'declareCode',
          dataIndex: 'declareCode',
        },
        {
          title: '申报企业名称',
          key: 'declareName',
          dataIndex: 'declareName',
        },
        {
          title: '申报调拨总数',
          key: 'declareAllocaNum',
          dataIndex: 'declareAllocaNum',
        },
        {
          title: '法定调拨总数',
          key: 'legalAllocaNum',
          dataIndex: 'legalAllocaNum',
        },
        {
          title: '第二法定调拨总数',
          key: 'legalAllocaNumTwo',
          dataIndex: 'legalAllocaNumTwo',
        },
        {
          title: '创建人',
          key: 'createPerson',
          dataIndex: 'createPerson',
        },
        {
          title: '创建时间',
          key: 'createDate',
          dataIndex: 'declareName',
        },
        {
          title: '区域代码',
          key: 'customsCode',
          dataIndex: 'customsCode',
        },
        {
          title: '申报日期',
          key: 'declareDate',
          dataIndex: 'declareDate',
        },
        {
          title: '审批日期',
          key: 'approvalDate',
          dataIndex: 'approvalDate',
        },
        {
          title: '前置环节',
          key: 'preStepId',
          dataIndex: 'preStepId',
        },
        {
          title: '操作类型(对应前置环节)',
          key: 'operType',
          dataIndex: 'operType',
        },
        {
          title: '审批状态',
          key: 'stepId',
          dataIndex: 'stepId',
          valueType: 'select',
          request: () => getParamList('approveStatus'),
        },
        {
          title: '操作结果(对应前置环节)',
          key: 'operResult',
          dataIndex: 'operResult',
        },
        {
          title: '申报人',
          key: 'declarePerson',
          dataIndex: 'declarePerson',
        },
        {
          title: '审批人',
          key: 'approvePerson',
          dataIndex: 'approvePerson',
        },
        {
          title: '外部审批意见',
          key: 'outerComment',
          dataIndex: 'outerComment',
        },
        {
          title: '内部审批意见',
          key: 'innerComment',
          dataIndex: 'innerComment',
        },
        {
          title: '录入单位名称',
          key: 'inputName',
          dataIndex: 'inputName',
        },
        {
          title: '录入单位代码',
          key: 'inputCode',
          dataIndex: 'inputCode',
        },
        {
          title: '备注',
          key: 'remark',
          dataIndex: 'remark',
        },
        {
          title: '账册类型',
          key: 'emsType',
          dataIndex: 'emsType',
        },
        {
          title: '成品料件',
          key: 'eDropFp',
          dataIndex: 'eDropFp',
        },
      ]}
    />
  );
};

export default DetailHead;
