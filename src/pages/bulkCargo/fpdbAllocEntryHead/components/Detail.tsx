import { RollbackOutlined, SendOutlined, EditOutlined } from '@ant-design/icons';
import { GridContent, PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Modal, message } from 'antd';
import React, { Fragment, useState } from 'react';
import type {
  AdvancedState,
  TableListItem,
} from '@/services/bulkCargo/fpdbAllocEntryHead/typings';
const { confirm } = Modal;
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { history } from 'umi';

import stylesDetail from './Detail.less';

import DetailHead from './DetailByHead';
import DetailList from './DetailByList';
import TransfersRecord from './DetailByTransfersRecord';
import ApproveList from './DetailByApproveList';
import CreateForm from './CreateForm';

type DetailProps = {
  record: TableListItem | undefined;
  onBack: (flag?: boolean) => void;
};

const contentList = {
  head: <DetailHead />,
  list: <DetailList canEdit={false} />,
  transfers: <TransfersRecord />,
  approveList: <ApproveList />,
};

//申报确认
function declareConfirm() {
  confirm({
    title: '提示',
    icon: <ExclamationCircleOutlined />,
    content: '确定要进行申报吗?',
    onOk() {
      console.log('OK');
      message.success('操作成功');
    },
    onCancel() {
      console.log('Cancel');
    },
  });
}

const Detail: React.FC<DetailProps> = (props) => {
  const pageType = history.location.pathname.substring(
    history.location.pathname.lastIndexOf('/') + 1,
  );
  // console.log(pageType);

  const [tabStatus, seTabStatus] = useState<AdvancedState>({
    operationKey: 'tab1',
    tabActiveKey: 'head',
  });

  const onTabChange = (tabActiveKey: string) => {
    seTabStatus({ ...tabStatus, tabActiveKey });
  };

  const action = (
    <Fragment>
      <Button
        type="primary"
        icon={<SendOutlined />}
        /* 调拨入区申请单查询的时候隐藏 */
        hidden={'search' == pageType}
        onClick={() => {
          declareConfirm();
        }}
      >
        申报
      </Button>
      <Button
        type="default"
        icon={<EditOutlined />}
        /* 调拨入区申请单查询的时候隐藏 */
        hidden={'search' == pageType}
        onClick={() => {
          handleModalVisible(true);
        }}
      >
        修改
      </Button>
      <Button
        icon={<RollbackOutlined />}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </Fragment>
  );

  const [createModalVisible, handleModalVisible] = useState<boolean>(false);

  return (
    <PageContainer
      header={{
        title: '预录入号：FPDBJJRQ1100696075211104000000	',
        breadcrumb: {},
      }}
      extra={action}
      className={stylesDetail.pageHeader}
      tabActiveKey={tabStatus.tabActiveKey}
      onTabChange={onTabChange}
      tabList={[
        {
          key: 'head',
          tab: '表头',
        },
        {
          key: 'list',
          tab: '表体',
        },
        {
          key: 'transfers',
          tab: '调拨记录',
        },
        {
          key: 'approveList',
          tab: '审核历史',
        },
      ]}
    >
      <div className={stylesDetail.main}>
        <GridContent>
          <Card className={stylesDetail.tabsCard} bordered={false}>
            {contentList[tabStatus.tabActiveKey]}
          </Card>
        </GridContent>
      </div>

      <CreateForm
        id={'1'}
        modalVisible={createModalVisible}
        onSubmit={async (value) => {
          // 保存的操作
        }}
        onCancel={() => {
          handleModalVisible(false);
        }}
      />
    </PageContainer>
  );
};

export default Detail;
