import { SearchOutlined } from '@ant-design/icons';
import { ModalForm, ProFormSelect, ProFormText } from '@ant-design/pro-form';
import {Button, Col, Row, Tabs, Space, message} from 'antd';
import React, {useState} from 'react';
const { TabPane } = Tabs;
import type { TableBodyListItem } from '@/services/bulkCargo/fpdbAllocEntryHead/typings';
import DetailList from './DetailByList';
import ModalSelectEntInfor from "@/components/ModalSelectEntInfo";

type FormProps = {
  id: string;
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: TableBodyListItem) => Promise<void>;
  modalVisible: boolean;
  values?: Partial<TableBodyListItem>;
};

const CreateForm: React.FC<FormProps> = (props) => {

  /**模态窗控制，选择经营企业*/
  const [modalFormVisible, setModalFormVisible] = useState<boolean>(false);

  return (
    <ModalForm
      title={props.id == '' ? '申请单表头新增' : '申请单表头修改'}
      width="1200px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      /*隐藏模态窗的按钮*/
      submitter={false}
    >
      <Tabs>
        <TabPane tab="表头" key="1">
          <Row>
            <Col span={8}>
              <ProFormText name="preEntryNo" label="预录入号" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormText name="allocationNo" label="申请单编号" placeholder="" disabled />
            </Col>
            <Col span={7}>
              <ProFormText
                name="tradeCode"
                label="经营企业编码"
                placeholder=""
                disabled
                rules={[{ required: true, message: '这是必填项' }]}
              />
            </Col>
            <Col span={1} style={{textAlign:"right"}}>
              <Button
                icon={<SearchOutlined />}
                style={{width:"100%"}}
                onClick={() => {
                  //
                  setModalFormVisible(true);
                }}
              />
            </Col>
            <Col span={8}>
              <ProFormText name="tradeName" label="经营企业名称" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormSelect
                name="departureBlock"
                label="启运区块"
                valueEnum={{
                  '01': { text: '加工区' },
                  '02': { text: '物流区' },
                  '03': { text: '港口二期' },
                }}
                initialValue="03"
                rules={[{ required: true, message: '这是必填项' }]}
              />
            </Col>
            <Col span={8}>
              <ProFormSelect
                name="destinationBlock"
                label="目的区块"
                valueEnum={{ '01': '加工区', '02': '物流区', '03': '港口二期' }}
                initialValue="01"
                rules={[{ required: true, message: '这是必填项' }]}
              />
            </Col>
            <Col span={8}>
              <ProFormText name="emsType" label="账册类型" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormText name="enterpriseBooksNo" label="账册编号" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormSelect
                name="eDropFp"
                label="成品料件"
                valueEnum={{ F: '成品', p: '料件' }}
                initialValue="F"
                rules={[{ required: true, message: '这是必填项' }]}
              />
            </Col>
            <Col span={8}>
              <ProFormSelect
                name="eDropFp"
                label="调拨类型"
                valueEnum={{ 1: '进境入区' }}
                initialValue="1"
                disabled
              />
            </Col>
            <Col span={8}>
              <ProFormText name="declareAllocaNum" label="申报调拨总数" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormText name="legalAllocaNum" label="法定调拨总数" placeholder="" disabled />
            </Col>
            <Col span={8}>
              <ProFormText
                name="legalAllocaNumTwo"
                label="第二法定调拨总数"
                placeholder=""
                disabled
              />
            </Col>
            <Col span={8}>
              <ProFormText name="remark" label="备注" placeholder="" />
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              &nbsp;
            </Col>
            <Col span={8}>
              &nbsp;
            </Col>
            <Col span={8} style={{textAlign:"right"}}>
              <Space size={'large'}>
              <Button
                type="primary"
                onClick={() => {
                  //
                  message.success('保存成功');
                  props.onCancel();
                }}
              >
                保存
              </Button>
              <Button
                onClick={() => {
                  props.onCancel();
                }}
              >
                取消
              </Button>
              </Space>
            </Col>
          </Row>
        </TabPane>
        <TabPane tab="表体" key="2">
          <DetailList canEdit={true} />
        </TabPane>
      </Tabs>
      <ModalSelectEntInfor
        modalVisible={modalFormVisible}
        onSubmit={async (row) => {
          /*提交*/
          console.log(row);
          message.success('成功')
          setModalFormVisible(false);
        }}
        onCancel={() => {
          setModalFormVisible(false);
        }}></ModalSelectEntInfor>
    </ModalForm>
  );
};

export default CreateForm;
