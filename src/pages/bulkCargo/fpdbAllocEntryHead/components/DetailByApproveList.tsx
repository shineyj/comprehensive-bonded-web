import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { useRef } from 'react';
//审核历史
const columns: ProColumns<{}>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: 30,
    align: 'center',
  },
  {
    title: '预录入号',
    dataIndex: 'preEntryNo',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申请单编号',
    dataIndex: 'allocationNo',
    width: 150,
    align: 'center',
    order: 1,
  },
  {
    title: '当前环节',
    dataIndex: 'stepId',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '前置环节',
    dataIndex: 'preStepId',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '操作类型',
    dataIndex: 'operType',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '操作结果',
    dataIndex: 'operResult',
    width: 150,
    align: 'center',
    order: 1,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDate',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报人',
    dataIndex: 'declarePerson',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报单位代码',
    dataIndex: 'declareCode',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报单位名称',
    dataIndex: 'declareName',
    width: 150,
    align: 'center',
    order: 1,
  },
  {
    title: '审批时间',
    dataIndex: 'approveDate',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '审批人',
    dataIndex: 'approvePerson',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '外部审批意间',
    dataIndex: 'outerComment',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '内部审批意见',
    dataIndex: 'innerComment',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '经营单位代码',
    dataIndex: 'tradeCode',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '经营单位名称',
    dataIndex: 'tradeName',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '录入单位代码',
    dataIndex: 'inputCode',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '录入单位名称',
    dataIndex: 'inputName',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建人',
    dataIndex: 'createPerson',
    width: 150,
    align: 'center',
    hideInSearch: true,
  },
];

const ApproveList: React.FC = () => {
  const actionRef = useRef<ActionType>();
  return (
    <>
      <ProTable<{}>
        columns={columns}
        actionRef={actionRef}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          x: 3500,
        }}
        bordered
        tableAlertRender={false}
        search={false}
        toolBarRender={false}
      />
    </>
  );
};

export default ApproveList;
