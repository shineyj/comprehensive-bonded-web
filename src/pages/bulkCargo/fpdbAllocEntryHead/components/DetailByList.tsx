import {
  PlusOutlined,
  ExportOutlined,
  SearchOutlined,
  SwapLeftOutlined,
  DeleteOutlined,
  ImportOutlined
} from '@ant-design/icons';
import type { ActionType } from '@ant-design/pro-table';
import {Table, Button, Row, Col, Form, Input} from 'antd';
import {useRef, useState} from 'react';
import type { TableBodyListItem } from '@/services/bulkCargo/fpdbAllocEntryHead/typings';
import DetailByListForm from './DetailByListForm'

type DetailListProps = {
  canEdit: boolean;
};

//表体
const DetailList: React.FC<DetailListProps> = (props) => {

  const columns = [
    {
      dataIndex: 'index',
      valueType: 'index',
      width: 30,
      align: 'center',
    },
    {
      title: '操作',
      width: 120,
      align: 'center',
      valueType: 'option',
      hideInTable: !props.canEdit,
      render: (text, record, _, action) => [
        <a key="editable">修改</a>,
        <a href="" target="_blank" rel="noopener noreferrer" key="view">
          删除
        </a>,
      ],
    },
    {
      title: '预录入号',
      dataIndex: 'preEntryNo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '申请单编号',
      dataIndex: 'allocationNo',
      width: 150,
      align: 'center',
      order: 1,
    },
    {
      title: '商品货号',
      dataIndex: 'productDuty',
      width: 150,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '商品编号',
      dataIndex: 'productCode',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '规格型号',
      dataIndex: 'specificationModel',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '申报单位',
      dataIndex: 'allocaDeclareCode',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '法定单位',
      dataIndex: 'allocaLUnit',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '第二法定单位',
      dataIndex: 'allocaLUnitTwo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '申请调拨数(申报)',
      dataIndex: 'declareAlNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '申请调拨数(法定)',
      dataIndex: 'legalAlNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '申请调拨数(第二法定)',
      dataIndex: 'legalAlNumTwo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账核扣数(申报)',
      dataIndex: 'accountChDeclareNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账核扣数(法定)',
      dataIndex: 'accountChLegalNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账核扣数(法定)',
      dataIndex: 'accountChLegalNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账核扣数(第二法定)',
      dataIndex: 'accountChLegalNumTwo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账预扣数(申报)',
      dataIndex: 'accountAdDeclareNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账预扣数(法定)',
      dataIndex: 'accountAdLegalNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '记账预扣数(第二法定)',
      dataIndex: 'accountAdLegalNumTwo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '实际调拨数(申报)',
      dataIndex: 'actualAlDeclareNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '实际调拨数(法定)',
      dataIndex: 'actualAlLegalNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '实际调拨数(第二法定)',
      dataIndex: 'actualAlLegalNumTwo',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '已记账未调拨数(申报)',
      dataIndex: 'accountUDeclareNumT',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '已记账未调拨数(法定)',
      dataIndex: 'accountULegalNum',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '已记账未调拨数(第二法定)',
      dataIndex: 'accountULegalNumTwo',
      width: 220,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '备案序号',
      dataIndex: 'extendField1',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: 180,
      align: 'center',
      hideInSearch: true,
    },
  ];

  const actionRef = useRef<ActionType>();
  const [formVisible, setFormVisible] = useState<boolean>(false);
  const [form] = Form.useForm();
  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="申请单编号">
              <Form.Item name="allocationNo" noStyle>
                <Input name="allocationNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={16} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />}>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />} hidden={!props.canEdit}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger hidden={!props.canEdit}>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />} hidden={!props.canEdit}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />} hidden={!props.canEdit}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>
      <Table<TableBodyListItem>
        columns={columns}
        actionRef={actionRef}
        scroll={{
          x: 4500,
        }}
        bordered
        size="middle"
        pagination={{ pageSize: 10, total: 100, showQuickJumper: true }}
        /*tableAlertRender={false}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={() => [
          props.canEdit && (
            <Button key="button" icon={<PlusOutlined />} onClick={()=>{
              setFormVisible(true);
            }}>
              录入
            </Button>
          ),
          props.canEdit && (
            <Button key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          ),
        ]}*/
      />
      <DetailByListForm
        modalVisible={formVisible}
        onSubmit={async (record) => {
          //
          setFormVisible(false);
        }}
        onCancel={()=>{
          setFormVisible(false);
        }}></DetailByListForm>
    </>
  );
};

export default DetailList;
