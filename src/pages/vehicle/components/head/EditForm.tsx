import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { vehicle } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<vehicle>>();
  return (
    <ProForm<vehicle>
      formRef={formRef}
      formKey="vehicleCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="车牌号"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="车辆类型"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="主管海关"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="申报类型"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="监管车海关编码"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="所属企业编码"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="preNo" label="所属企业信用代码" placeholder="" />
        </Col>
        <Col span={8}>
          <ProFormText
            name="preNo"
            label="所属企业名称"
            placeholder=""
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="preNo" label="车辆自重" placeholder="" />
        </Col>
        <Col span={8}>
          <ProFormText name="preNo" label="集装箱号" placeholder="" />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="车架号" disabled />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="车架重"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="集装箱箱型"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
