import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    title: '商品序号',
    dataIndex: 'gNo',
    width: '100',
    sorter: true,
  },
  {
    title: '商品料号',
    dataIndex: 'copGNo',
    width: '200',
  },
  {
    title: '商品编码',
    dataIndex: 'codeTs',
    width: '120',
  },
  {
    title: '商品名称',
    dataIndex: 'gName',
    width: '180',
  },
  {
    title: '规格型号',
    dataIndex: 'gModel',
    width: '300',
  },
  {
    title: '申报计量单位',
    dataIndex: 'unit',
    width: '130',
  },
  {
    title: '法定第一单位',
    dataIndex: 'unit1',
    width: '130',
  },
  {
    title: '法定第二单位',
    dataIndex: 'unit2',
    width: '130',
  },
  {
    title: '申报单价',
    dataIndex: 'decPrice',
    width: '120',
  },
  {
    title: '辅料标记',
    dataIndex: 'adjmtrMarkcd',
    width: '120',
  },
  {
    title: '修改标记',
    dataIndex: 'modifyMark',
    width: '120',
  },
];

const EmsImgDisplay: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="EmsImgSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="商品序号">
              <Form.Item name="gNo" noStyle>
                <Input name="gNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品料号">
              <Form.Item name="copGNo" noStyle>
                <Input name="copGNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品编码">
              <Form.Item name="codeTs" noStyle>
                <Input name="codeTs" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        scroll={{ x: true }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default EmsImgDisplay;
