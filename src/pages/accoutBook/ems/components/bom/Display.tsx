import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    title: '序号',
    dataIndex: 'bomId',
    width: '100',
    sorter: true,
  },
  {
    title: '成品序号',
    dataIndex: 'endprdSeqno',
    width: '100',
    sorter: true,
  },
  {
    title: '成品料号',
    dataIndex: 'endprdGdsMtno',
    width: '100',
  },
  {
    title: '成品商品编码',
    dataIndex: 'exgCodeTs',
    width: '100',
  },
  {
    title: '成品名称',
    dataIndex: 'exgGName',
    width: '100',
  },
  {
    title: '料件序号',
    dataIndex: 'mtpckSeqno',
    width: '100',
    sorter: true,
  },
  {
    title: '料件料号',
    dataIndex: 'mtpckGdsMtno',
    width: '100',
  },
  {
    title: '料件商品编码',
    dataIndex: 'imgCodeTs',
    width: '120',
  },
  {
    title: '料件名称',
    dataIndex: 'imgGName',
    width: '200',
  },
  {
    title: '单耗版本号',
    dataIndex: 'bomVersion',
    width: '120',
  },
  {
    title: '有形损耗率',
    dataIndex: 'tgblLossRate',
    width: '120',
  },
  {
    title: '无形损耗率',
    dataIndex: 'intgbLossRate',
    width: '120',
  },
  {
    title: '保税料件比例',
    dataIndex: 'bondMtpckPrpr',
    width: '180',
  },
  {
    title: '修改标记',
    dataIndex: 'modifyMark',
    width: '130',
  },
  {
    title: '单耗申报状态',
    dataIndex: 'ucnsDclStucd',
    width: '130',
  },
];

const BomDisplay: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="BomSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="成品序号">
              <Form.Item name="endprdSeqno" noStyle>
                <Input name="endprdSeqno" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="成品料号">
              <Form.Item name="endprdGdsMtno" noStyle>
                <Input name="endprdGdsMtno" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="成品商品编码">
              <Form.Item name="exgCodeTs" noStyle>
                <Input name="exgCodeTs" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="料件序号">
              <Form.Item name="mtpckSeqno" noStyle>
                <Input name="mtpckSeqno" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="料件料号">
              <Form.Item name="mtpckGdsMtno" noStyle>
                <Input name="mtpckGdsMtno" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="料件商品编码">
              <Form.Item name="imgCodeTs" noStyle>
                <Input name="imgCodeTs" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="单耗版本号">
              <Form.Item name="bomVersion" noStyle>
                <Input name="bomVersion" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        scroll={{ x: 1500 }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default BomDisplay;
