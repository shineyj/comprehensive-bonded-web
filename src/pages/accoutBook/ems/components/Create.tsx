import { AppleOutlined, AndroidOutlined, SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { Button, Card, Tabs } from 'antd';
import React from 'react';

import HeadForm from './head/Form';
import ExgDisplay from './exg/Display';
import ImgDisplay from './img/Display';
import BomDisplay from './bom/Display';
import AttachDisplay from './attach/Display';

type CreateProps = {
  onBack: () => void;
};

const { TabPane } = Tabs;

const Create: React.FC<CreateProps> = (props) => {
  const extraAction = (
    <>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        style={{ marginLeft: '10px' }}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </>
  );
  return (
    <Card title="录入加工贸易账册信息" extra={extraAction}>
      <Tabs defaultActiveKey="head">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              加工贸易账册表头
            </span>
          }
          key="head"
        >
          <HeadForm />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              成品表体
            </span>
          }
          key="emg"
        >
          <ExgDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              料件表体
            </span>
          }
          key="img"
        >
          <ImgDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              单耗明细
            </span>
          }
          key="BOM"
        >
          <BomDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              随附单证
            </span>
          }
          key="attach"
        >
          <AttachDisplay />
        </TabPane>
      </Tabs>
    </Card>
  );
};
export default Create;
