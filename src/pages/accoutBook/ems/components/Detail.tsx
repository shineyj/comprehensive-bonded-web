/** 加工账册明细 */
import { SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { GridContent, PageContainer, RouteContext } from '@ant-design/pro-layout';
import { useState, Fragment } from 'react';
import { Button, Card, Descriptions, Statistic } from 'antd';
import type { EmsItem } from '../data';

import HeadDescriptions from './head/Descriptions';
import ExgDisplay from './exg/Display';
import ImgDisplay from './img/Display';
import BomDisplay from './bom/Display';
import AttachDisplay from './attach/Display';

import stylesDetail from './Detail.less';
import ApproveCard from '@/components/ApproveCard';

const description = (
  <RouteContext.Consumer>
    {({ isMobile }) => (
      <Descriptions size="small" column={isMobile ? 1 : 2}>
        <Descriptions.Item label="主管海关">黄埔海关</Descriptions.Item>
        <Descriptions.Item label="账册编号">H010018A0450</Descriptions.Item>
        <Descriptions.Item label="经营单位代码">1100696075</Descriptions.Item>
        <Descriptions.Item label="经营单位名称">北京世纪周峰商业设备有限公司</Descriptions.Item>
        <Descriptions.Item label="录入时间">2020-08-13 09:59:51</Descriptions.Item>
        <Descriptions.Item label="申报时间">2020-08-13 09:59:51</Descriptions.Item>
      </Descriptions>
    )}
  </RouteContext.Consumer>
);

const extra = (
  <div className={stylesDetail.moreInfo}>
    <Statistic title="申报类型" value="备案申请" />
    <Statistic title="状态" value="待审批" />
  </div>
);

type ProfileDetail = {
  record: EmsItem | undefined;
  onBack: (flag?: boolean) => void;
};

const contentList = {
  head: <HeadDescriptions />,
  exg: <ExgDisplay />,
  img: <ImgDisplay />,
  bom: <BomDisplay />,
  attached: <AttachDisplay />,
  approveList: <ApproveCard />,
};

export type AdvancedState = {
  operationKey: string;
  tabActiveKey: string;
};

const Detail: React.FC<ProfileDetail> = (props) => {
  const [tabStatus, seTabStatus] = useState<AdvancedState>({
    operationKey: 'tab1',
    tabActiveKey: 'head',
  });

  const onTabChange = (tabActiveKey: string) => {
    seTabStatus({ ...tabStatus, tabActiveKey });
  };

  const action = (
    <Fragment>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </Fragment>
  );

  return (
    <PageContainer
      header={{
        title: '单号：201001801110',
        breadcrumb: {},
      }}
      extra={action}
      className={stylesDetail.pageHeader}
      content={description}
      extraContent={extra}
      tabActiveKey={tabStatus.tabActiveKey}
      onTabChange={onTabChange}
      tabList={[
        {
          key: 'head',
          tab: '账册表头',
        },
        {
          key: 'exg',
          tab: '成品表体',
        },
        {
          key: 'img',
          tab: '料件表体',
        },
        {
          key: 'bom',
          tab: '单耗明细',
        },
        {
          key: 'attached',
          tab: '随附单证',
        },
        {
          key: 'approveList',
          tab: '审批历史',
        },
      ]}
    >
      <div className={stylesDetail.main}>
        <GridContent>
          <Card className={stylesDetail.tabsCard} bordered={false}>
            {contentList[tabStatus.tabActiveKey]}
          </Card>
        </GridContent>
      </div>
    </PageContainer>
  );
};

export default Detail;
