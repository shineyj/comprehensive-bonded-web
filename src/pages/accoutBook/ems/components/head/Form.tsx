import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormDatePicker } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { EmsItem } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<EmsItem>>();
  return (
    <ProForm<EmsItem>
      formRef={formRef}
      formKey="emsCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="企业内部编号 " />
        </Col>
        <Col span={8}>
          <ProFormText name="emsNo" label="账册编号 " disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="emsClass"
            label="账册类型"
            valueEnum={{
              1: 'E账册',
              2: 'H账册',
              3: '耗料',
              4: '工单',
              5: '企业为单元',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="netwkEtpsArcrpNo" label="联网企业档案库编号 " />
        </Col>
        <Col span={8}>
          <ProFormText name="emsApprNo" label="批准证编号" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareType"
            label="申报类型"
            valueEnum={{
              1: '备案申请',
              2: '变更申请',
            }}
            initialValue="1"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="ownerCode"
            label="收货企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="ownerName"
            label="收货企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="rvsngdEtpsSccd"
            label="收货企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeCode"
            label="经营企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeName"
            label="经营企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="bizopEtpsSccd"
            label="经营企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="declareCode"
            label="申报企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="declareName"
            label="申报企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="dclEtpsSccd"
            label="申报企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="endDate"
            label="结束有效期"
            fieldProps={{ style: { width: '100%' } }}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="dclEtpsTypecd"
            label="申报企业类型	"
            valueEnum={{
              1: '企业',
              2: '代理企业',
              3: '报关行',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="modifyTimes" label="变更次数 " disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="maxTovrAmt" label="最大周转金额(万美元)" />
        </Col>
        <Col span={8}>
          <ProFormText name="maxApprImpAmt" label="最大进口额(万美元)" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="hchxType"
            label="核算方式"
            valueEnum={{
              1: '单耗',
              2: '耗料',
              3: '工单',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="hchxCycle" label="核算周期(天)" initialValue={180} />
        </Col>
        <Col span={8}>
          <ProFormText name="adjaccMarkcd" label="核算标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="adjaccTmsCnt" label="核算次数" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="rcntVclrTime" label="最近核销时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="hchxType"
            label="单耗申报环节"
            valueEnum={{
              1: '出口前',
              2: '报核前',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="ucnsVernoCntrFlag" label="单耗版本号标志" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="etpsPosesSadjaQuaFlag" label="核销方式" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="customsCode" label="主管海关" />
        </Col>
        <Col span={8}>
          <ProFormText name="rcaseMarkcd" label="涉案标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="dclMarkcd" label="申报标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="emapvStucd" label="审批状态" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="pauseChgMarkcd" label="暂停变更标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="createDate" label="创建时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="declareDate" label="申报时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="pauseImpexpMarkcd" label="暂停进出口标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="putrecApprTime" label="备案批准时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="chgApprTime" label="变更批准时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="UsageTypecd"
            label="账册用途"
            valueEnum={{
              1: '一般纳税人',
              2: '特殊行业',
              3: '保税维修',
              4: '委托加工',
              5: '保税研发',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
