import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { Col, message, Row } from 'antd';
import type { Ref } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useImperativeHandle, useRef } from 'react';
import type { EmgItem } from '../../data';
import { emgDetail } from '../../service';

const handleAdd = async (fields: EmgItem) => {
  const hide = message.loading('正在添加');
  try {
    //await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

type EmgFormProps = {
  onRef: Ref;
  preNo?: string;
  actionType?: string;
};
const EmgForm: React.FC<EmgFormProps> = (props) => {
  const formRef = useRef<ProFormInstance<EmgItem>>();

  useImperativeHandle(props.onRef, () => {
    // 需要将暴露的接口返回出去
    return {
      validate: formRef.current?.validateFields,
      submit: formRef.current?.submit,
      reset: formRef.current?.resetFields,
    };
  });

  const [formInitialValues, setFormInitialValues] = useState<EmgItem>({});

  useEffect(() => {
    if (!!props.preNo && props.actionType == 'edit') {
      emgDetail(props.preNo).then((res) => {
        if (res.success) {
          formRef.current?.setFieldsValue(res.data);
          setFormInitialValues(res.data);
        }
      });
    }
  }, [props.preNo]);

  return (
    <>
      <ProForm<EmgItem>
        formRef={formRef}
        formKey="emgForm"
        autoFocusFirstInput
        layout="horizontal"
        labelAlign="right"
        initialValues={formInitialValues}
        onFinish={handleAdd}
        labelCol={{ style: { width: '114px' } }}
        size="middle"
        //onFinish={}
        submitter={{
          searchConfig: {
            resetText: '重置',
            submitText: '提交',
          },
          // 配置按钮的属性
          resetButtonProps: {
            style: {
              // 隐藏按钮
              //display: 'none',
            },
          },
          submitButtonProps: {
            style: {
              // 隐藏按钮
              //display: 'none',
            },
          },
          render: () => {
            return false;
          },
        }}
      >
        <Row gutter={20}>
          <Col span={8}>
            <ProFormText name="gNo" label="商品序号" placeholder="" disabled />
          </Col>
          <Col span={8}>
            <ProFormText
              name="copGNo"
              label="商品料号"
              placeholder=""
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={8}>
            <ProFormSelect
              name="materialsProperties"
              label="物料性质"
              valueEnum={{
                E: '成品',
              }}
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={8}>
            <ProFormText
              name="codeTs"
              label="商品编码"
              placeholder=""
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={16}>
            <ProFormText
              name="gName"
              label="商品名称"
              placeholder=""
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={24}>
            <ProFormText
              name="gModel"
              label="规格型号"
              placeholder=""
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={8}>
            <ProFormText
              name="unit"
              label="计量单位"
              placeholder=""
              rules={[{ required: true, message: '这是必填项' }]}
            />
          </Col>
          <Col span={8}>
            <ProFormText name="unit1" label="第一法定单位" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormText name="unit2" label="第二法定单位" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormText name="dclQty" label="申报数量" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormText name="decPrice" label="申报单价" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormText name="curr" label="币制" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormSelect
              name="dutyMode"
              label="征免方式"
              valueEnum={{
                1: '照章征税',
                2: '折半征税',
                3: '全免',
                4: '特案',
                5: '征免性质',
                6: '保证金',
                7: '保函',
                8: '折半补税',
                9: '全额退税',
              }}
            />
          </Col>
          <Col span={8}>
            <ProFormText name="apprMaxSurpQty" label="批准最大余数量" placeholder="" />
          </Col>
          <Col span={8}>
            <ProFormText name="note" label="备注" placeholder="" />
          </Col>
        </Row>
      </ProForm>
    </>
  );
};

export default EmgForm;
