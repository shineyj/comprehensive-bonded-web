import ProDescriptions from '@ant-design/pro-descriptions';
import type { ExgItem } from '../../data';
import { emgDetail } from '../../service';

const columns = [
  {
    title: '商品序号',
    key: 'gNo',
    dataIndex: 'gNo',
  },
  {
    title: '商品料号',
    key: 'copGNo',
    dataIndex: 'copGNo',
  },
  {
    title: '物料性质',
    key: 'materialsProperties',
    dataIndex: 'materialsProperties',
  },
  {
    title: '商品编号',
    key: 'codeTs',
    dataIndex: 'codeTs',
  },
  {
    title: '商品名称',
    key: 'gName',
    dataIndex: 'gName',
    span: 16,
  },
  {
    title: '规格型号',
    key: 'gModel',
    dataIndex: 'gModel',
    span: 24,
  },
  {
    title: '计量单位',
    key: 'unit',
    dataIndex: 'unit',
  },
  {
    title: '第一法定单位',
    key: 'unit1',
    dataIndex: 'unit1',
  },
  {
    title: '第二法定单位',
    key: 'unit2',
    dataIndex: 'unit2',
  },
  {
    title: '申报数量',
    key: 'dclQty',
    dataIndex: 'dclQty',
  },
  {
    title: '申报单价',
    key: 'decPrice',
    dataIndex: 'decPrice',
  },
  {
    title: '币制',
    key: 'curr',
    dataIndex: 'curr',
  },
  {
    title: '征免方式',
    key: 'dutyMode',
    dataIndex: 'dutyMode',
  },
  {
    title: '批准最大余数量',
    key: 'apprMaxSurpQty',
    dataIndex: 'apprMaxSurpQty',
  },
  {
    title: '企业执行标记',
    key: 'etpsExeMarkcd',
    dataIndex: 'etpsExeMarkcd',
  },
  {
    title: '海关执行标记',
    key: 'cusmExeMarkcd',
    dataIndex: 'cusmExeMarkcd',
  },
  {
    title: '核销周期初始数量',
    key: 'vclrPridIniQty',
    dataIndex: 'vclrPridIniQty',
  },
  {
    title: '单耗质疑标志',
    key: 'ucnsTqsnFlag',
    dataIndex: 'ucnsTqsnFlag',
  },
  {
    title: '数量控制标记',
    key: 'qtyCntrMarkcd',
    dataIndex: 'qtyCntrMarkcd',
  },
  {
    title: '磋商标志',
    key: 'csttnFlag',
    dataIndex: 'csttnFlag',
  },
  {
    title: '记账清单编号',
    key: 'invtNo',
    dataIndex: 'invtNo',
  },
  {
    title: '记账清单商品序号',
    key: 'dclQty',
    dataIndex: 'invtGNo',
  },
  {
    title: '处理标志',
    key: 'modifyMark',
    dataIndex: 'modifyMark',
  },
  {
    title: '变更次数',
    key: 'modifyTimes',
    dataIndex: 'modifyTimes',
  },
  {
    title: '备注',
    key: 'note',
    dataIndex: 'note',
    span: 24,
  },
];

/** 暂时使用列表数据作为显示信息 */
const EmgDescriptions: React.FC<EmgItem> = (props) => {
  return (
    <>
      <ProDescriptions
        title="成品信息"
        bordered
        column={3}
        size="middle"
        columns={columns}
        request={() => emgDetail(props.preNo)}
        labelStyle={{ width: '165px' }}
      />
    </>
  );
};

export default EmgDescriptions;
