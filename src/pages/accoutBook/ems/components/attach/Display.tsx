import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    width: '100',
  },
  {
    title: '预录入号',
    dataIndex: 'preNo',
    width: '100',
  },
  {
    title: '随附单证序号',
    dataIndex: 'ACMP_FORM_SEQNO',
    width: '100',
  },
  {
    title: '随附单证类型代码',
    dataIndex: 'exgCodeTs',
    width: '100',
  },
  {
    title: '随附单证编号',
    dataIndex: 'exgGName',
    width: '100',
  },
  {
    title: '固定编号',
    dataIndex: 'mtpckSeqno',
    width: '100',
  },
  {
    title: '随附单证文件名称',
    dataIndex: 'mtpckGdsMtno',
    width: '100',
  },
];

const AttachDisplay: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="随附单证编号">
              <Form.Item name="acmpFormNo" noStyle>
                <Input name="acmpFormNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="随附单证文件名称">
              <Form.Item name="acmpFormFileNm" noStyle>
                <Input name="acmpFormFileNm" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="随附单证类型代码">
              <Form.Item name="acmpFormTypecd" noStyle>
                <Input name="acmpFormTypecd" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        scroll={{ x: 1500 }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default AttachDisplay;
