export type EmsItem = {
  preNo?: string;
  emsNo?: string;
  emsApprNo?: string;
  emsClass?: string;
  netwkEtpsArcrpNo?: string;
  declareType?: string;
  ownerCode?: string;
  ownerName?: string;
  rvsngdEtpsSccd?: string;
  tradeCode?: string;
  tradeName?: string;
  bizopEtpsSccd?: string;
  declareCode?: string;
  declareName?: string;
  dclEtpsSccd?: string;
  endDate?: string;
  dclEtpsTypecd?: string;
  modifyTimes?: string;
  maxTovrAmt?: string;
  maxApprImpAmt?: string;
  hchxType?: string;
  hchxCycle?: string;
  adjaccMarkcd?: string;
  adjaccTmsCnt?: string;
  rcntVclrTime?: string;
  equipMode?: string;
  ucnsVernoCntrFlag?: string;
  etpsPosesSadjaQuaFlag?: string;
  customsCode?: string;
  rcaseMarkcd?: string;
  dclMarkcd?: string;
  EemapvSyucd?: string;
  pauseChgMarkcd?: string;
  createDate?: string;
  declareDate?: string;
  pauseImpexpMarkcd?: string;
  putrecApprTime?: string;
  chgApprTime?: string;
  usageTypecd?: string;
  workNo?: string;
  delcareFlag?: string;
  note?: string;
  approveStatus?: string;
  inputTime?: string;
  approveDate?: string;
};

export type ExgItem = {
  preNo?: string;
  gNo?: string;
  copGNo?: string;
  materialsProperties?: string;
  codeTs?: string;
  gName?: string;
  gModel?: string;
  unit?: string;
  unit1?: string;
  unit2?: string;
  dclQty?: string;
  decPrice?: string;
  curr?: string;
  dutyMode?: string;
  apprMaxSurpQty?: string;
  note?: string;
  etpsExeMarkcd?: string;
  cusmExeMarkcd?: string;
  vclrPridIniQty?: string;
  ucnsTqsnFlag?: string;
  qtyCntrMarkcd?: string;
  csttnFlag?: string;
  invtNo?: string;
  invtGNo?: string;
  modifyMark?: string;
  modifyTimes?: string;
  indbTime?: string;
  adjmtrMarkcd?: string;
  csttnFlag?: string;
};

type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type ExgListData = {
  list: ExgItem[];
  pagination: Partial<TableListPagination>;
};

export type ExgTableListParams = {
  gNo?: string;
  copGNo?: string;
  codeTs?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  current?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

export type ImgItem = {
  preNo?: string;
  gNo?: string;
  copGNo?: string;
  materialsProperties?: string;
  codeTs?: string;
  gName?: string;
  gModel?: string;
  unit?: string;
  unit1?: string;
  unit2?: string;
  dclQty?: string;
  decPrice?: string;
  curr?: string;
  dutyMode?: string;
  apprMaxSurpQty?: string;
  note?: string;
  etpsExeMarkcd?: string;
  cusmExeMarkcd?: string;
  vclrPridIniQty?: string;
  ucnsTqsnFlag?: string;
  qtyCntrMarkcd?: string;
  csttnFlag?: string;
  invtNo?: string;
  invtGNo?: string;
  modifyMark?: string;
  modifyTimes?: string;
  indbTime?: string;
  adjmtrMarkcd?: string;
  csttnFlag?: string;
};

export type BomItem = {
  // 序号
  bomId?: string;
  // 成品序号
  endprdSeqno?: string;
  // 料件序号
  mtpckSeqno?: string;
  // 成品料号
  endprdGdsMtno?: string;
  // 料件料号
  mtpckGdsMtno?: string;
  // 单耗数量
  ucnsQty?: string;
  // 有形损耗率
  tgblLossRate?: string;
  // 无形损耗率
  intgbLossRate?: string;
  // 净耗数量
  decCm?: string;
  // 保税料件比例
  bondMtpckPrpr?: string;
  // 非保税料件比例
  txtFBS_MATERIAL_RATIO?: string;
  // 单耗版本号
  bomVersion?: string;
  // 结束有效期
  dclTime?: string;
  // 单耗申报状态
  ucnsDclStucd?: string;
  // 料件商品编码
  imgCodeTs?: string;
  // 料件成交单位
  imgUnit?: string;
  // 企业执行标记代码
  etpsExeMarkcd?: string;
  // 料件商品名称
  imgGName?: string;
  // 成品商品编码
  exgCodeTs?: string;
  // 成品成交单位
  exgUnit?: string;
  // 变更次数
  modifyTimes?: string;
  // 成品商品名称
  exgGName?: string;
  // 处理标志
  modifyMark?: string;
  // 备注
  note?: string;
};
