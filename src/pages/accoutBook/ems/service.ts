// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import type { ExgItem, EmsItem } from './data.d';

/** Ems */
/** 获取加工账册表头列表 GET /api/ems */
export async function ems(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: EmsItem[];
    total?: number;
    success?: boolean;
  }>('/api/ems', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** Emg */

/** 获取成品列表 GET /api/emg */
export async function emg(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: ExgItem[];
    total?: number;
    success?: boolean;
  }>('/api/emg', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取成品详情 GET /api/emgDetail */
export async function emgDetail(params: { preNo?: string }, options?: { [key: string]: any }) {
  return request<ExgItem>('/api/emgDetail', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** Img */

/** 获取料件列表 GET /api/img */
export async function img(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: ExgItem[];
    total?: number;
    success?: boolean;
  }>('/api/img', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
