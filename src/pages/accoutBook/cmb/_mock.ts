import type { CmbHead } from './data.d';
import type { Request, Response } from 'express';
import { parse } from 'url';

const getCmbList = () => {
  const cmbListDataSource: CmbHead[] = [];
  for (let i = 0; i < 21; i++) {
    cmbListDataSource.push({
      preNo: `0100C180000000000${i}`,
      cmbNo: `0100C180000000000${i}`,
      dclTypecd: '备案申请',
      CMTypecd: 'D',
      customsCode: '天津海关',
      cmBeginTime: '2018-05-18',
      cmEndTime: '2018-05-19',
      chgTmsCnt: '1',
      bizopEtpsNo: '1100696075',
      bizopEtpsName: '北京世纪周峰商业设备有限公司',
      bizopEtpsSCCD: '913401003437792391',
      rcvgdEtpsNo: '1100696075',
      rcvgdEtpsName: '北京世纪周峰商业设备有限公司',
      rcvgdEtpsSCCD: '913401003437792391',
      emapvStucd: '审批通过',
      putrecApprTime: '2018-05-18 13:11:43',
      chgApprTime: '2018-05-18 13:11:43',
      stucd: '通过',
      dclTime: '2018-05-18 13:11:43',
      dclMarkcd: '1',
      workNo: `0100C180000000000${i}`,
      delcareFlag: '1',
      note: '',
      approveStatus: '1',
      emsNo: 'T0100W000030',
      inputTime: '2020/08/13 09:59:51',
      declareDate: '2020/08/13 09:59:51',
      approveDate: '2020/08/13 09:59:51',
    });
  }
  return cmbListDataSource;
};

const cmbListDataSource: CmbHead[] = getCmbList();

function getCmb(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query;

  let dataSource = [...cmbListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: dataSource,
    total: cmbListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/cmb': getCmb,
};
