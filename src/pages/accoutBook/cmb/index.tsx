import {
  PlusOutlined,
  ImportOutlined,
  DeleteOutlined,
  SendOutlined,
  ExportOutlined,
  PrinterOutlined,
} from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';

import { Button } from 'antd';
import { useRef, useState } from 'react';
import { history } from 'umi';
import type { CmbHead } from './data';

import Create from './components/Create';
import { cmb } from './service';
import Detail from './components/Detail';

const columns: ProColumns<CmbHead>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '预录入号',
    dataIndex: 'preNo',
    width: 200,
    align: 'center',
    copyable: true,
    order: 6,
  },
  {
    title: '企业内部编号',
    dataIndex: 'workNo',
    width: 200,
    align: 'center',
    copyable: true,
    order: 4,
  },
  {
    title: '耗料单编号',
    dataIndex: 'cmbNo',
    width: 200,
    align: 'center',
    copyable: true,
    order: 5,
    search: false,
  },
  {
    title: '申报类型',
    dataIndex: 'dclTypecd',
    width: 140,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: { text: '备案申请' },
    },
    hideInSearch: true,
  },
  {
    title: '数据状态',
    dataIndex: 'approveStatus',
    width: 140,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      0: { text: '预录入' },
      1: { text: '审批通过' },
      2: { text: '退单' },
      3: { text: '转人工' },
    },
    order: 3,
  },
  {
    title: '主管海关',
    dataIndex: 'customsCode',
    width: 180,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '耗料单类型',
    dataIndex: 'CMTypecd',
    width: 180,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      D: { text: '正向耗料' },
      F: { text: '反向耗料' },
    },
  },
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    width: 180,
    align: 'center',
    hideInSearch: false,
  },
  {
    title: '经营单位编号',
    dataIndex: 'bizopEtpsNo',
    width: 180,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '经营企业名称',
    dataIndex: 'bizopEtpsName',
    width: 360,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTime',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '审批时间',
    dataIndex: 'approveDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTimeS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 2,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDateS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 1,
  },
];

export default () => {
  const pageType = history.location.pathname.substring(
    history.location.pathname.lastIndexOf('/') + 1,
  );

  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<CmbHead[]>([]);

  /**
   * 显示模式：create-录入界面、list-列表、detail-详情
   */
  const [showType, handleShowType] = useState<string>('list');

  const [doubleClickRowState, setDoubleClickRowState] = useState<CmbHead>();

  const listTable = (
    <>
      <ProTable<CmbHead>
        columns={columns}
        actionRef={actionRef}
        request={cmb}
        pagination={{
          pageSize: 10,
        }}
        rowKey="preNo"
        bordered
        scroll={{
          x: 2200,
        }}
        rowSelection={{
          onChange: (_, selectedRow) => {
            setSelectedRows(selectedRow);
          },
        }}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={() => [
          'declare' == pageType && (
            <Button
              icon={<PlusOutlined />}
              onClick={() => {
                handleShowType('create');
              }}
            >
              录入
            </Button>
          ),
          'declare' == pageType && <Button icon={<ImportOutlined />}>导入</Button>,
          'declare' == pageType && (
            <Button icon={<DeleteOutlined />} danger disabled={selectedRowsState.length == 0}>
              删除
            </Button>
          ),
          'search' != pageType && (
            <Button
              icon={<SendOutlined />}
              type="primary"
              ghost
              disabled={selectedRowsState.length == 0}
            >
              申报
            </Button>
          ),
          <Button icon={<ExportOutlined />}>导出</Button>,
          <Button icon={<PrinterOutlined />}>打印</Button>,
        ]}
        onRow={(record) => {
          return {
            onClick: (event) => {}, // 点击行
            onDoubleClick: (event) => {
              setDoubleClickRowState(record);
              handleShowType('detail');
            },
            onContextMenu: (event) => {},
            onMouseEnter: (event) => {}, // 鼠标移入行
            onMouseLeave: (event) => {},
          };
        }}
      />
    </>
  );
  switch (showType) {
    case 'list':
      return listTable;
      break;
    case 'create':
      return (
        <Create
          onBack={() => {
            handleShowType('list');
          }}
        />
      );
      break;
    case 'detail':
      return <Detail record={doubleClickRowState} onBack={() => handleShowType('list')} />;
      break;
    default:
      return listTable;
  }
};
