import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormDatePicker } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { CmbHead } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<CmbHead>>();
  return (
    <ProForm<CmbHead>
      formRef={formRef}
      formKey="emsCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="企业内部编号 " />
        </Col>
        <Col span={8}>
          <ProFormText name="emsNo" label="耗料单编号 " disabled />
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="declareType"
            label="申报类型"
            valueEnum={{
              1: '备案申请',
              2: '变更申请',
            }}
            initialValue="1"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareType"
            label="耗料单类型"
            valueEnum={{
              1: '备案申请',
              2: '变更申请',
            }}
            initialValue="1"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="ownerCode"
            label="账册编号	"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="customsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="customsCode"
            label="耗料单开始时间"
            fieldProps={{ style: { width: '100%' } }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="customsCode"
            label="耗料单截止时间"
            fieldProps={{ style: { width: '100%' } }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="customsCode" label="变更次数" disabled />
        </Col>
        <Col span={8}>
          <ProFormText
            name="ownerName"
            label="收发货企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="ownerName"
            label="收发货企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="rvsngdEtpsSccd"
            label="收发货企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeCode"
            label="经营企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeName"
            label="经营企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="bizopEtpsSccd"
            label="经营企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="declareCode" label="审批标记" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="declareName" label="备案审批时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="dclEtpsSccd" label="变更审批时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="endDate" label="状态	" disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
            disabled
          />
        </Col>
        <Col span={24}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
