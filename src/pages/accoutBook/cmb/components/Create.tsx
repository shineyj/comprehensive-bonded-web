import { AppleOutlined, AndroidOutlined, SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { Button, Card, Tabs } from 'antd';
import React from 'react';

import CmbHeadForm from './head/EidtForm';
import BillDisplay from './bill/Display';
import ImgDisplay from './img/Display';
import ListDisplay from './list/Display';

type CreateProps = {
  onBack: () => void;
};

const { TabPane } = Tabs;

const Create: React.FC<CreateProps> = (props) => {
  const extraAction = (
    <>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        style={{ marginLeft: '10px' }}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </>
  );
  return (
    <Card title="录入耗料单信息" extra={extraAction}>
      <Tabs defaultActiveKey="head">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              耗料单表头
            </span>
          }
          key="head"
        >
          <CmbHeadForm />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              耗料单清单
            </span>
          }
          key="bill"
        >
          <BillDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              耗料单料件
            </span>
          }
          key="img"
        >
          <ImgDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              耗料单边角料
            </span>
          }
          key="cmb"
        >
          <ListDisplay />
        </TabPane>
      </Tabs>
    </Card>
  );
};
export default Create;
