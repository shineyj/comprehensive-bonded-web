export type CmbHead = {
  preNo?: string;
  cmbNo?: string;
  dclTypecd?: string;
  CMTypecd?: string;
  customsCode?: string;
  cmBeginTime?: string;
  cmEndTime?: string;
  chgTmsCnt?: string;
  bizopEtpsNo?: string;
  bizopEtpsName?: string;
  bizopEtpsSCCD?: string;
  rcvgdEtpsNo?: string;
  rcvgdEtpsName?: string;
  rcvgdEtpsSCCD?: string;
  emapvStucd?: string;
  putrecApprTime?: string;
  chgApprTime?: string;
  stucd?: string;
  dclTime?: string;
  dclMarkcd?: string;
  workNo?: string;
  delcareFlag?: string;
  note?: string;
  approveStatus?: string;
  emsNo?: string;
  // 录入时间
  inputTime;
  // 申报时间
  declareDate: string;
  // 审批时间
  approveDate: string;
};

export type CmbBill = {
  prNo?: string;
  cmbNo?: string;
  bondInvtNo?: string;
  modfMarkcd?: string;
  note?: string;
};

export type CmbImg = {
  prNo?: string;
  cmbNo?: string;
  mtpckSeqNo: string;
  mtpckSeqNoCopGNo: string;
  ucnsNetUseupQty: string;
  modfMarkcd?: string;
  note?: string;
};

export type CmbList = {
  prNo?: string;
  cmbNo?: string;
  mtpckSeqNo: string;
  gdecd: string;
  gdsName: string;
  gdsSpecModelDesc: string;
  dclUnitcd: string;
  dclQty: string;
  modfMarkcd: string;
};
