// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import type { CmbHead } from './data.d';

/** CmbHead */
/** 获取加工账册表头列表 GET /api/cmb */

export async function cmb(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: CmbHead[];
    total?: number;
    success?: boolean;
  }>('/api/cmb', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
