import type { DcrHead } from './data.d';
import type { Request, Response } from 'express';
import { parse } from 'url';

const getCmbList = () => {
  const dcrListDataSource: DcrHead[] = [];
  for (let i = 0; i < 21; i++) {
    dcrListDataSource.push({
      preNo: `EMSBH01002000003${i + 1}`,
      emsNo: 'H010018A0443',
      workNo: `EMSBH01002000003${i + 1}`,
      chgoffTmsCnt: '1',
      chgoffBeginTime: '2020/11/29 0:00:00',
      chgoffDueTime: '2020/11/29 0:00:00',
      declareDate: '2020/11/29 0:00:00',
      bizopEtpsno: '1100696075',
      bizopEtpsNm: '北京世纪周峰商业设备有限公司',
      bizopEtpsSccd: '',
      rcvgdEtpsno: '1100696075',
      rcvgdEtpsNm: '北京世纪周峰商业设备有限公司',
      declareCode: '1100696075',
      declareName: '北京世纪周峰商业设备有限公司',
      impInvtTotalCnt: '1',
      expInvtTotalCnt: '2',
      chgoffTypecd: '1',
      impMtpckTotalAmt: '22',
      expEndprdTotalAmt: '33',
      chgoffDclTime: '2020-12-31',
      customsCode: '北京海关',
      dSPS_MARKCD: '年度申报',
      cHGOFF_MTPCK_TOTAL_CNT: 'string',
      cHGOFF_ENDPRD_TOTAL_CNT: 'string',
      cHGOFF_LFMT_TOTAL_CNT: 'string',
      cHGOFF_IMPR_TOTAL_CNT: 'string',
      delcareFlag: '1',
      note: 'string',
      approveStatus: '1',
    });
  }
  return dcrListDataSource;
};

const dcrListDataSource: DcrHead[] = getCmbList();

function getDcr(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query;

  let dataSource = [...dcrListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: [],
    total: dcrListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/dcr': getDcr,
};
