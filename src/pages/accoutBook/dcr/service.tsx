// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import type { DcrHead } from './data.d';

/** DcrHead */
/** 获取加工账册报核表头列表 GET /api/dcr */

export async function dcr(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: DcrHead[];
    total?: number;
    success?: boolean;
  }>('/api/dcr', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
