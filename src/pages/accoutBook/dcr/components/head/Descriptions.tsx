import { EditOutlined } from '@ant-design/icons';
import ProDescriptions from '@ant-design/pro-descriptions';
import { Button } from 'antd';

const HeadDescriptions: React.FC = () => {
  return (
    <ProDescriptions
      title="详细信息"
      bordered
      request={async () => {
        return Promise.resolve({
          success: true,
          data: {
            preNo: 'W51656180010',
            workNo: 'W51656180010',
            emsNo: 'T5165W000075',
            declareType: '1',
            houseTypecd: 'A',
            bwlTypecd: 'A',
            appendTypecd: '1',
            usageTypecd: '1',
            dclEtpsTypecd: '1',
            customsCode: '5165|黄埔海关',
            endDate: '2022-12-31',
            modifyTimes: '0',
            houseNo: '443066K516',
            houseName: '广州亚晋国际货运代理有限公司',
            storeArea: '48016.82',
            storeVol: '111',
            houseAddress:
              '广州市南沙区龙穴岛南沙综合保税区物流区港荣三街天运仓二层5-1至5-12、6-1至6-12、7-1至7-12',
            tradeCode: '443066K516',
            tradeName: '广州亚晋国际货运代理有限公司',
            bizopEtpsSccd: '91440115088041914L',
            declareCode: '443066K516',
            declareName: '广州亚晋国际货运代理有限公司',
            dclEtpsSccd: '91440115088041914L',
            contactEr: '练应彬',
            contactTele: '020-39089295',
            dclMarkcd: '1',
            taxTypecd: 'N',
            emapvStucd: '1',
            declareDate: '2021-06-18',
            putrecApprTime: '2021-06-18',
            chgApprTime: '2021-06-18',
          },
        });
      }}
      columns={[
        {
          title: '预录入号',
          key: 'preNo',
          dataIndex: 'preNo',
        },
        {
          title: '企业内部编号',
          key: 'workNo',
          dataIndex: 'workNo',
        },
        {
          title: '帐册编号',
          key: 'emsNo',
          dataIndex: 'emsNo',
        },
        {
          title: '申报类型',
          key: 'dclTypecd',
          dataIndex: 'dclTypecd',
          valueType: 'select',
          valueEnum: {
            1: { text: '正常申报' },
            2: { text: '补充申报' },
            3: { text: '海关处置' },
          },
        },
        {
          title: '主管海关',
          key: 'customsCode',
          dataIndex: 'customsCode',
        },
        {
          title: '报核开始时间',
          key: 'CMTypecd',
          dataIndex: 'houseTypecd',
        },
        {
          title: '报核截止时间',
          key: 'emsNo',
          dataIndex: 'emsNo',
        },

        {
          title: '报核申报时间',
          key: 'cmBeginTime',
          dataIndex: 'cmBeginTime',
        },
        {
          title: '经营企业代码',
          key: 'bizopEtpsNo',
          dataIndex: 'bizopEtpsNo',
          valueType: 'date',
        },
        {
          title: '经营企业名称',
          key: 'bizopEtpsName"',
          dataIndex: 'bizopEtpsName',
        },
        {
          title: '经营企业信用代码',
          key: 'bizopEtpsSCCD',
          dataIndex: 'bizopEtpsSCCD',
        },
        {
          title: '收发货企业代码',
          key: 'rcvgdEtpsNo',
          dataIndex: 'rcvgdEtpsNo',
        },
        {
          title: '收货企业名称',
          key: 'rcvgdEtpsName',
          dataIndex: 'rcvgdEtpsName',
        },
        {
          title: '收发货企业信用代码',
          key: 'rcvgdEtpsSCCD',
          dataIndex: 'rcvgdEtpsSCCD',
        },
        {
          title: '进口清单总份数',
          key: 'cmEndTime',
          dataIndex: 'cmEndTime',
        },
        {
          title: '出口清单总份数',
          key: 'chgTmsCnt',
          dataIndex: 'chgTmsCnt',
        },

        {
          title: '报核料件总数',
          key: 'emapvStucd',
          dataIndex: 'emapvStucd',
        },
        {
          title: '报核成品总数',
          key: 'putrecApprTime',
          dataIndex: 'putrecApprTime',
        },
        {
          title: '报核边角料总数',
          key: 'chgApprTime',
          dataIndex: 'chgApprTime',
        },
        {
          title: '报核残次品总数',
          key: 'stucd',
          dataIndex: 'stucd',
        },
        {
          title: '进口料件总金额',
          key: 'declareDate',
          dataIndex: 'declareDate',
        },
        {
          title: '出口成品总金额',
          key: 'declareName',
          dataIndex: 'declareName',
        },
        {
          title: '申报标志',
          key: 'delcareFlag',
          dataIndex: 'delcareFlag',
        },
        {
          title: '备注',
          key: 'note',
          dataIndex: 'note',
        },
        {
          title: '操作',
          valueType: 'option',
          render: () => [
            <Button type="default" icon={<EditOutlined />}>
              修改
            </Button>,
          ],
        },
      ]}
    />
  );
};

export default HeadDescriptions;
