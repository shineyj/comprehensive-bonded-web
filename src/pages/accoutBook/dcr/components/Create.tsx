import { AppleOutlined, AndroidOutlined, SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { Button, Card, Tabs } from 'antd';
import React from 'react';

import HeadForm from './head/EditForm';
import BillDisplay from './bill/Display';
import ImgDisplay from './img/Display';
import AttachDisplay from './attach/Display';

type CreateProps = {
  onBack: () => void;
};

const { TabPane } = Tabs;

const Create: React.FC<CreateProps> = (props) => {
  const extraAction = (
    <>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        style={{ marginLeft: '10px' }}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </>
  );
  return (
    <Card title="录入加工贸易账册报核信息" extra={extraAction}>
      <Tabs defaultActiveKey="head">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              报核表头
            </span>
          }
          key="head"
        >
          <HeadForm />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              报核清单
            </span>
          }
          key="bill"
        >
          <BillDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              报核表体
            </span>
          }
          key="list"
        >
          <ImgDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              随附单证
            </span>
          }
          key="attach"
        >
          <AttachDisplay />
        </TabPane>
      </Tabs>
    </Card>
  );
};
export default Create;
