import {
  PlusOutlined,
  DeleteOutlined,
  SendOutlined,
  ImportOutlined,
  PrinterOutlined,
  ExportOutlined,
} from '@ant-design/icons';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { Button, message } from 'antd';
import { history } from 'umi';
import { useRef, useState } from 'react';
import type { TableListItem } from './data';
import { bws } from './service';

import CreateForm from './components/CreateForm';
import Detail from './components/Detail';

const handleAdd = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    //await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

const columns: ProColumns<TableListItem>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '预录入号',
    dataIndex: 'preNo',
    width: 180,
    align: 'center',
    copyable: true,
    order: 6,
  },
  {
    title: '企业内部编号',
    dataIndex: 'workNo',
    width: 180,
    align: 'center',
    copyable: true,
    order: 4,
  },
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    width: 140,
    align: 'center',
    copyable: true,
    order: 5,
  },
  {
    title: '业务类型',
    dataIndex: 'declareType',
    width: 120,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: { text: '备案申请' },
    },
    hideInSearch: true,
  },
  {
    title: '数据状态',
    dataIndex: 'approveStatus',
    width: 120,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      0: { text: '预录入' },
      1: { text: '审批通过' },
      2: { text: '退单' },
      3: { text: '转人工' },
    },
    order: 3,
  },
  {
    title: '主管海关',
    dataIndex: 'customsCode',
    width: 180,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '结束有效期',
    dataIndex: 'endDate',
    width: 150,
    align: 'center',
    valueType: 'date',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '经营单位代码',
    dataIndex: 'tradeCode',
    width: 120,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '经营单位名称',
    dataIndex: 'tradeName',
    width: 450,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTime',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '审批时间',
    dataIndex: 'approveDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTimeS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 2,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDateS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 1,
  },
];

export default () => {
  const pageType = history.location.pathname.substring(
    history.location.pathname.lastIndexOf('/') + 1,
  );
  console.log(pageType);
  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  /**
   * 新建窗口的弹窗
   */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * 详情是否显示
   */
  const [detailVisible, handleDetailVisible] = useState<boolean>(false);

  const [doubleClickRowState, setDoubleClickRowState] = useState<TableListItem>();

  const listTable = (
    <>
      <ProTable<TableListItem>
        columns={columns}
        actionRef={actionRef}
        request={bws}
        pagination={{
          pageSize: 10,
        }}
        bordered
        scroll={{
          x: 2000,
        }}
        rowSelection={{
          onChange: (_, selectedRow) => {
            setSelectedRows(selectedRow);
          },
        }}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={() => [
          'declare' == pageType && (
            <Button
              icon={<PlusOutlined />}
              onClick={() => {
                handleModalVisible(true);
              }}
            >
              录入
            </Button>
          ),
          'declare' == pageType && <Button icon={<ImportOutlined />}>导入</Button>,
          'declare' == pageType && (
            <Button icon={<DeleteOutlined />} danger disabled={selectedRowsState.length == 0}>
              删除
            </Button>
          ),
          'search' != pageType && (
            <Button
              icon={<SendOutlined />}
              type="primary"
              ghost
              disabled={selectedRowsState.length == 0}
            >
              申报
            </Button>
          ),
          <Button icon={<ExportOutlined />}>导出</Button>,
          <Button icon={<PrinterOutlined />}>打印</Button>,
        ]}
        rowKey="preNo"
        onRow={(record) => {
          return {
            onClick: (event) => {}, // 点击行
            onDoubleClick: (event) => {
              setDoubleClickRowState(record);
              handleDetailVisible(true);
            },
            onContextMenu: (event) => {},
            onMouseEnter: (event) => {}, // 鼠标移入行
            onMouseLeave: (event) => {},
          };
        }}
      />

      <CreateForm
        modalVisible={createModalVisible}
        onSubmit={async (value) => {
          const success = await handleAdd(value);
          if (success) {
            handleModalVisible(false);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleModalVisible(false);
        }}
      />
    </>
  );

  if (!detailVisible) {
    return listTable;
  } else {
    return (
      <Detail
        record={doubleClickRowState}
        onBack={() => {
          handleDetailVisible(false);
        }}
      />
    );
  }
};
