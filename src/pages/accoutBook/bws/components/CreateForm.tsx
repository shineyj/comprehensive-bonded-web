import {
  ModalForm,
  ProFormDatePicker,
  ProFormDigit,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-form';
import { Col, Row } from 'antd';
import React from 'react';
import type { TableListItem } from '../data';

const bwlTypecd = {
  A: '保税物流中心A',
  B: '保税物流中心B',
  D: '公共保税仓库',
  E: '液体保税仓库',
  F: '寄售维修保税仓库',
  G: '暂为空',
  H: '特殊商品保税仓库',
  I: '备料保税仓库',
  P: '出口配送监管仓',
  J: '为国内结转监管仓',
  K: '保税区',
  L: '出口加工区',
  M: '保税物流园区',
  N: '保税港区',
  Z: '综合保税区',
  Q: '跨境工业园区',
  S: '特殊区域设备账册',
};

type CreateFormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: TableListItem) => Promise<void>;
  modalVisible: boolean;
  values?: Partial<TableListItem>;
};

const CreateForm: React.FC<CreateFormProps> = (props) => {
  return (
    <ModalForm
      title="录入物流账册信息"
      width="1100px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      onFinish={props.onSubmit}
      submitter={{
        searchConfig: {
          submitText: '保存',
          resetText: '取消',
        },
      }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="企业内部编号 " />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareType"
            label="申报类型"
            valueEnum={{
              1: '备案申请',
              2: '变更申请',
            }}
            initialValue="1"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormSelect name="houseTypecd" label="企业类型" valueEnum={bwlTypecd} />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="appendTypecd"
            label="记账模式"
            valueEnum={{ 1: '可累计', 2: '不可累计' }}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect name="bwlTypecd" label="区域场所类别" valueEnum={bwlTypecd} />
        </Col>
        <Col span={8}>
          <ProFormText
            name="houseNo"
            label="仓库编号"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="houseName"
            label="仓库名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormDigit name="storeVol" label="仓库体积" />
        </Col>
        <Col span={8}>
          <ProFormDigit
            name="storeArea"
            label="仓库面积"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={16}>
          <ProFormText
            name="houseAddress"
            label="仓库地址"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="dclEtpsTypecd"
            label="申报企业类型	"
            valueEnum={{
              1: '企业',
              2: '代理企业',
              3: '报关行',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="usageTypecd"
            label="账册用途	"
            valueEnum={{
              1: '跨境电商',
              2: '出口跨境电商',
              3: '海外仓',
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="endDate"
            label="结束有效期"
            fieldProps={{ style: { width: '100%' } }}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="contactEr"
            label="联系人"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="contactTele"
            label="联系电话"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="customsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeCode"
            label="经营企业代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeName"
            label="经营企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="bizopEtpsSccd"
            label="经营企业信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="declareCode" label="申报企业代码" />
        </Col>
        <Col span={8}>
          <ProFormText name="declareName" label="申报企业名称" />
        </Col>
        <Col span={8}>
          <ProFormText name="dclEtpsSccd" label="申报企业信用代码" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="taxTypecd"
            label="退税标志代码"
            valueEnum={{
              N: '入区入场所退税标志(否)',
              Y: '入区入场所退税标志(是)',
            }}
          />
        </Col>
        <Col span={16}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default CreateForm;
