import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    align: 'center',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '商品序号',
    dataIndex: 'gNo',
    align: 'center',
  },
  {
    title: '商品料号',
    dataIndex: 'copGNo',
    align: 'center',
  },
  {
    title: '商品编码',
    dataIndex: 'codeTs',
    align: 'center',
  },
  {
    title: '商品名称',
    dataIndex: 'gName',
    align: 'center',
  },
  {
    title: '规格型号',
    dataIndex: 'gModel',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '处理标志',
    dataIndex: 'modifyMark',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '国别',
    dataIndex: 'countryCode',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报计量单位',
    dataIndex: 'unit',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '法定第一单位',
    dataIndex: 'unit1',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '法定第二单位',
    dataIndex: 'unit2',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报单价',
    dataIndex: 'decPrice',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报币制',
    dataIndex: 'curr',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '记账清单编号',
    dataIndex: 'invtNo',
    align: 'center',
  },
  {
    title: '记账清单商品序号',
    dataIndex: 'invtGNo',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '平均美元单价',
    dataIndex: 'avgPrice',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '库存美元总价',
    dataIndex: 'totalAmt',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '存储(监管)期限',
    dataIndex: 'limitDate',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '最近入仓日期',
    dataIndex: 'inDate',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '最近出仓日期',
    dataIndex: 'outDate',
    align: 'center',
    hideInSearch: true,
  },
];

const ListDisplay: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="随附单证编号">
              <Form.Item name="acmpFormNo" noStyle>
                <Input name="acmpFormNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="随附单证文件名称">
              <Form.Item name="acmpFormFileNm" noStyle>
                <Input name="acmpFormFileNm" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="随附单证类型代码">
              <Form.Item name="acmpFormTypecd" noStyle>
                <Input name="acmpFormTypecd" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        scroll={{ x: 2500 }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default ListDisplay;
