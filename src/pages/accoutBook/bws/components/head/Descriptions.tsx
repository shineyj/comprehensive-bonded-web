import { EditOutlined } from '@ant-design/icons';
import ProDescriptions from '@ant-design/pro-descriptions';
import { Button } from 'antd';

const bwlTypecd = {
  A: '保税物流中心A',
  B: '保税物流中心B',
  D: '公共保税仓库',
  E: '液体保税仓库',
  F: '寄售维修保税仓库',
  G: '暂为空',
  H: '特殊商品保税仓库',
  I: '备料保税仓库',
  P: '出口配送监管仓',
  J: '为国内结转监管仓',
  K: '保税区',
  L: '出口加工区',
  M: '保税物流园区',
  N: '保税港区',
  Z: '综合保税区',
  Q: '跨境工业园区',
  S: '特殊区域设备账册',
};

const HeadDescriptions: React.FC = () => {
  return (
    <ProDescriptions
      title="详细信息"
      bordered
      request={async () => {
        return Promise.resolve({
          success: true,
          data: {
            preNo: 'W51656180010',
            workNo: 'W51656180010',
            emsNo: 'T5165W000075',
            declareType: '1',
            houseTypecd: 'A',
            bwlTypecd: 'A',
            appendTypecd: '1',
            usageTypecd: '1',
            dclEtpsTypecd: '1',
            customsCode: '5165|黄埔海关',
            endDate: '2022-12-31',
            modifyTimes: '0',
            houseNo: '443066K516',
            houseName: '广州亚晋国际货运代理有限公司',
            storeArea: '48016.82',
            storeVol: '111',
            houseAddress:
              '广州市南沙区龙穴岛南沙综合保税区物流区港荣三街天运仓二层5-1至5-12、6-1至6-12、7-1至7-12',
            tradeCode: '443066K516',
            tradeName: '广州亚晋国际货运代理有限公司',
            bizopEtpsSccd: '91440115088041914L',
            declareCode: '443066K516',
            declareName: '广州亚晋国际货运代理有限公司',
            dclEtpsSccd: '91440115088041914L',
            contactEr: '练应彬',
            contactTele: '020-39089295',
            dclMarkcd: '1',
            taxTypecd: 'N',
            emapvStucd: '1',
            declareDate: '2021-06-18',
            putrecApprTime: '2021-06-18',
            chgApprTime: '2021-06-18',
          },
        });
      }}
      columns={[
        {
          title: '预录入号',
          key: 'preNo',
          dataIndex: 'preNo',
        },
        {
          title: '企业内部编号',
          key: 'workNo',
          dataIndex: 'workNo',
        },
        {
          title: '账册编号',
          key: 'emsNo',
          dataIndex: 'emsNo',
        },
        {
          title: '申报类型',
          key: 'declareType',
          dataIndex: 'declareType',
          valueType: 'select',
          valueEnum: {
            1: { text: '备案申请' },
            2: { text: '变更申请' },
          },
        },
        {
          title: '企业类型',
          key: 'houseTypecd',
          dataIndex: 'houseTypecd',
          valueType: 'select',
          valueEnum: bwlTypecd,
        },
        {
          title: '区域场所类别',
          key: 'bwlTypecd',
          dataIndex: 'bwlTypecd',
          valueType: 'select',
          valueEnum: bwlTypecd,
        },
        {
          title: '记账模式',
          key: 'appendTypecd',
          dataIndex: 'appendTypecd',
          valueType: 'select',
          valueEnum: { 1: '可累计', 2: '不可累计' },
        },
        {
          title: '账册用途',
          key: 'usageTypecd',
          dataIndex: 'usageTypecd',
          valueType: 'select',
          valueEnum: { 1: '跨境电商', 2: '出口跨境电商', 3: '海外仓' },
        },
        {
          title: '申报企业类型',
          key: 'dclEtpsTypecd',
          dataIndex: 'dclEtpsTypecd',
          valueType: 'select',
          valueEnum: { 1: '企业', 2: '代理企业', 3: '报关行' },
        },
        {
          title: '主管海关',
          key: 'customsCode',
          dataIndex: 'customsCode',
        },
        {
          title: '结束有效期',
          key: 'endDate',
          dataIndex: 'endDate',
          valueType: 'date',
        },
        {
          title: '变更次数',
          key: 'modifyTimes"',
          dataIndex: 'modifyTimes',
        },
        {
          title: '仓库编号',
          key: 'houseNo',
          dataIndex: 'houseNo',
        },
        {
          title: '仓库名称',
          key: 'houseName',
          dataIndex: 'houseName',
        },
        {
          title: '仓库面积',
          key: 'storeArea',
          dataIndex: 'storeArea',
        },
        {
          title: '仓库体积',
          key: 'storeVol',
          dataIndex: 'storeVol',
        },
        {
          title: '仓库地址',
          key: 'houseAddress',
          dataIndex: 'houseAddress',
          span: 2,
        },
        {
          title: '经营企业代码',
          key: 'tradeCode',
          dataIndex: 'tradeCode',
        },
        {
          title: '经营企业名称',
          key: 'tradeName',
          dataIndex: 'tradeName',
        },
        {
          title: '经营企业信用代码',
          key: 'bizopEtpsSccd',
          dataIndex: 'bizopEtpsSccd',
        },
        {
          title: '申报企业代码',
          key: 'declareCode',
          dataIndex: 'declareCode',
        },
        {
          title: '申报企业名称',
          key: 'declareName',
          dataIndex: 'declareName',
        },
        {
          title: '申报企业信用代码',
          key: 'dclEtpsSccd',
          dataIndex: 'dclEtpsSccd',
        },
        {
          title: '联系人',
          key: 'contactEr',
          dataIndex: 'contactEr',
        },
        {
          title: '联系电话',
          key: 'contactTele',
          dataIndex: 'contactTele',
        },
        {
          title: '申报标志',
          key: 'declareType',
          dataIndex: 'declareType',
          valueType: 'select',
          valueEnum: { 0: '暂存', 1: '申报' },
        },
        {
          title: '申报标记',
          key: 'dclMarkcd',
          dataIndex: 'dclMarkcd',
          valueType: 'select',
          valueEnum: { 1: '电子口岸申报' },
        },
        {
          title: '退税标志代码',
          key: 'taxTypecd',
          dataIndex: 'taxTypecd',
          valueType: 'select',
          valueEnum: { N: '入区入场所退税标志(否)', Y: '入区入场所退税标志(是)' },
        },
        {
          title: '审批状态',
          key: 'emapvStucd',
          dataIndex: 'emapvStucd',
          valueType: 'select',
          valueEnum: { 1: '通过' },
        },
        {
          title: '申报时间',
          key: 'declareDate',
          dataIndex: 'declareDate',
          valueType: 'dateTime',
        },
        {
          title: '备案批准时间',
          key: 'putrecApprTime',
          dataIndex: 'putrecApprTime',
          valueType: 'dateTime',
        },
        {
          title: '变更批准时间',
          key: 'chgApprTime',
          dataIndex: 'chgApprTime',
          valueType: 'dateTime',
        },
        {
          title: '操作',
          valueType: 'option',
          render: () => [
            <Button type="default" icon={<EditOutlined />}>
              修改
            </Button>,
          ],
        },
      ]}
    />
  );
};

export default HeadDescriptions;
