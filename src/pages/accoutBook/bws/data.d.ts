export type TableListItem = {
  // 企业内部编号
  workNo: string;
  // 预录入号
  preNo: string;
  // 账册编号
  emsNo: string;
  // 业务类型
  declareType: string;
  // 仓库编号
  houseNo?: string;
  // 仓库名称
  houseName?: string;
  // 区域场所类别
  bwlTypecd?: string;
  // 企业类型
  houseTypecd?: string;
  // 记账模式
  appendTypecd?: string;
  // 结束有效期
  endDate?: string;
  // 仓库地址
  houseAddress?: string;
  // 账册用途
  usageTypecd?: string;
  // 仓库体积
  storeVol?: string;
  // 仓库面积
  storeArea?: string;
  // 申报企业类型
  dclEtpsTypecd?: string;
  // 经营企业代码
  tradeCode?: string;
  // 经营企业名称
  tradeName?: string;
  // 经营企业信用代码
  bizopEtpsSccd?: string;
  // 申报企业代码
  declareCode?: string;
  // 申报企业名称
  declareName?: string;
  // 申报企业信用代码
  dclEtpsSccd?: string;
  // 联系人
  contactEr?: string;
  // 联系电话
  contactTele?: string;
  // 主管海关
  customsCode?: string;
  // 申报标记
  dclMarkcd?: string;
  // 退税标志代码
  taxTypecd?: string;
  // 暂停变更标记
  pauseChgMarkcd?: string;
  // 暂停进出口标记
  pauseImpexpMarkcd?: string;
  // 企业内部编码
  workNo?: string;
  // 申报标志
  delcareType?: string;
  // 备注
  note?: string;
  // 审批状态
  approveStatus: string;
  // 录入时间
  inputTime;
  // 申报时间
  declareDate: string;
  // 审批时间
  approveDate: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  preNo?: string;
  emsNo?: string;
  workNo?: string;
  approveStatus?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

export type FormValueType = {
  // 预录入号
  preNo?: string;
  // 账册编号
  emsNo?: string;
  // 申报类型
  declareType?: string;
  // 仓库编号
  houseNo?: string;
  // 仓库名称
  houseName?: string;
  // 区域场所类别
  bwlTypecd?: string;
  // 企业类型
  houseTypecd?: string;
  // 记账模式
  appendTypecd?: string;
  // 结束有效期
  endDate?: string;
  // 仓库地址
  houseAddress?: string;
  // 账册用途
  usageTypecd?: string;
  // 仓库体积
  storeVol?: string;
  // 仓库面积
  storeArea?: string;
  // 申报企业类型
  dclEtpsTypecd?: string;
  // 经营企业代码
  tradeCode?: string;
  // 经营企业名称
  tradeName?: string;
  // 经营企业信用代码
  bizopEtpsSccd?: string;
  // 申报企业代码
  declareCode?: string;
  // 申报企业名称
  declareName?: string;
  // 申报企业信用代码
  dclEtpsSccd?: string;
  // 联系人
  contactEr?: string;
  // 联系电话
  contactTele?: string;
  // 主管海关
  customsCode?: string;
  // 申报标记
  dclMarkcd?: string;
  // 退税标志代码
  taxTypecd?: string;
  // 暂停变更标记
  pauseChgMarkcd?: string;
  // 暂停进出口标记
  pauseImpexpMarkcd?: string;
  // 企业内部编码
  workNo?: string;
  // 申报标志
  delcareType?: string;
  // 备注
  note?: string;
};

export type AdvancedState = {
  operationKey: string;
  tabActiveKey: string;
};

export const bwlTypecd = {
  A: '保税物流中心A',
  B: '保税物流中心B',
  D: '公共保税仓库',
  E: '液体保税仓库',
  F: '寄售维修保税仓库',
  G: '暂为空',
  H: '特殊商品保税仓库',
  I: '备料保税仓库',
  P: '出口配送监管仓',
  J: '为国内结转监管仓',
  K: '保税区',
  L: '出口加工区',
  M: '保税物流园区',
  N: '保税港区',
  Z: '综合保税区',
  Q: '跨境工业园区',
  S: '特殊区域设备账册',
};

export type imgTableListItem = {
  emsNo?: string;
  copGNo?: string;
  gNo?: string;
  codeTs?: string;
  gName?: string;
  gModel?: string;
  unit?: string;
  unit1?: string;
  unit2?: string;
  decPrice?: number;
  curr?: string;
  countryCode?: string;
  cusmExeMarkcd?: string;
  inType?: string;
  modifyTimes?: string;
  invtNo?: string;
  invtGNo?: string;
  limitDate?: string;
  prevdIncQty?: number;
  prevdRedcQty?: number;
  inDate?: string;
  actlIncQty?: number;
  actlRedcQty?: number;
  outDate?: string;
  inQty?: number;
  inLawfQty?: number;
  inSecdLawfQty?: number;
  avgPrice?: number;
  totalAmt?: number;
  note?: string;
  modifyMark?: string;
};
