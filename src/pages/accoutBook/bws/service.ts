// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { TableListItem } from './data';

/** 获取物流列表 GET /api/bws */
export async function bws(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableListItem[];
    total?: number;
    success?: boolean;
  }>('/api/bws', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
