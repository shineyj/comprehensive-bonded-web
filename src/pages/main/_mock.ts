import moment from 'moment';
import type { Request, Response } from 'express';
import type { AnalysisData } from './data.d';

const bussinessChartData = [];
for (let i = 0; i < 24; i += 1) {
  const date = moment(1000 * 60 * 60 * i).format('HH:mm');
  //console.log(date);
  bussinessChartData.push({
    date,
    type: '业务申报表',
    value: Math.floor(Math.random() * 100) + 10,
  });
  bussinessChartData.push({
    date,
    type: '出入库单',
    value: Math.floor(Math.random() * 100) + 10,
  });
  bussinessChartData.push({
    date,
    type: '核注清单',
    value: Math.floor(Math.random() * 100) + 10,
  });
  bussinessChartData.push({
    date,
    type: '集报清单',
    value: Math.floor(Math.random() * 100) + 10,
  });
  bussinessChartData.push({
    date,
    type: '核放单',
    value: Math.floor(Math.random() * 100) + 10,
  });
  bussinessChartData.push({
    date,
    type: '两步核放单',
    value: Math.floor(Math.random() * 100) + 10,
  });
}

const exceptionChartData = [];
for (let i = 0; i < 24; i += 1) {
  const date = moment(1000 * 60 * 60 * i).format('HH:mm');
  //console.log(date);
  exceptionChartData.push({
    date,
    type: '业务申报表',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
  exceptionChartData.push({
    date,
    type: '出入库单',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
  exceptionChartData.push({
    date,
    type: '核注清单',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
  exceptionChartData.push({
    date,
    type: '集报清单',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
  exceptionChartData.push({
    date,
    type: '核放单',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
  exceptionChartData.push({
    date,
    type: '两步核放单',
    value: Math.floor(Math.random() * 20) + 24 - i,
  });
}

const backChartData = [];
for (let i = 0; i < 24; i += 1) {
  const date = moment(1000 * 60 * 60 * i).format('HH:mm');
  backChartData.push({
    date,
    type: '业务申报表',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
  backChartData.push({
    date,
    type: '出入库单',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
  backChartData.push({
    date,
    type: '核注清单',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
  backChartData.push({
    date,
    type: '集报清单',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
  backChartData.push({
    date,
    type: '核放单',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
  backChartData.push({
    date,
    type: '两步核放单',
    value: Math.floor(Math.random() * 10) + 24 - i,
  });
}


const getBussinessChartData: AnalysisData = {
  bussinessChartData,
  exceptionChartData,
  backChartData,
};

const getChartData = (_: Request, res: Response) => {
  return res.json({
    data: getBussinessChartData,
  });
};

export default {
  'GET  /api/getChartData': getChartData,
};
