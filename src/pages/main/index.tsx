import type { FC } from 'react';
import { Suspense } from 'react';
import { GridContent } from '@ant-design/pro-layout';
import LineChartForData from './components/LineChartForData';
import { useRequest } from 'umi';

import { getChartData } from './service';
import type { AnalysisData } from './data.d';


type AnalysisProps = {
  dashboardAndanalysis: AnalysisData;
  loading: boolean;
};


const Analysis: FC<AnalysisProps> = () => {
  const { loading, data } = useRequest(getChartData);
  return (
    <GridContent>
      <>
        <Suspense fallback={null}>
          <LineChartForData
            title="单证每天业务量"
            loading={loading}
            charData={data?.bussinessChartData || []}
          />
        </Suspense>
        <Suspense fallback={null}>
          <LineChartForData
            title="每天异常单据量"
            loading={loading}
            charData={data?.exceptionChartData || []}
          />
        </Suspense>
        <Suspense fallback={null}>
          <LineChartForData
            title="每天海关退单业务单证数"
            loading={loading}
            charData={data?.backChartData || []}
          />
        </Suspense>
      </>
    </GridContent>
  );
};

export default Analysis;
