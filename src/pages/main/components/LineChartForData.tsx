import { Card, Tabs } from 'antd';
import { Line } from '@ant-design/charts';
import type { DataItem } from '../data.d';

import styles from '../style.less';


const { TabPane } = Tabs;

const LineChartForData = ({
  title,
  loading,
  charData,
}: {
  title: string;
  loading: boolean;
  charData: DataItem[];
}) => (
  <Card loading={loading} className={styles.offlineCard} bordered={false} style={{ marginTop: 32 }}>
    <Tabs activeKey="biz">
      <TabPane tab={title} key="biz">
        <div style={{ padding: '0 24px' }}>
          <Line
            forceFit
            height={400}
            data={charData}
            responsive
            xField="date"
            yField="value"
            seriesField="type"
            interactions={[
              {
                type: 'slider',
                cfg: {},
              },
            ]}
            legend={{
              position: 'top-center',
            }}
          />
        </div>
      </TabPane>
    </Tabs>
  </Card>
);

export default LineChartForData;
