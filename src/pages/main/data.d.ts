import { DataItem } from '@antv/g2plot/esm/interface/config';
export { DataItem };

export interface AnalysisData {
  bussinessChartData: DataItem[];
  exceptionChartData: DataItem[];
  backChartData: DataItem[];
}
