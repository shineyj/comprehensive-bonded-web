import { request } from 'umi';
import type { AnalysisData } from './data';

export async function getChartData(): Promise<{ data: AnalysisData }> {
  return request('/api/getChartData');
}
