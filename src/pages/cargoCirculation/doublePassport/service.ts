// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import type { DoublePassportHead } from './data.d';

/** DoublePassportHead */
/** 获取两步核放单表头列表 GET /api/doublePassport */

export async function doublePassport(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: DoublePassportHead[];
    total?: number;
    success?: boolean;
  }>('/api/doublePassport', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
