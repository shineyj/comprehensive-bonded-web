import type { DoublePassportHead } from './data.d';
import type { Request, Response } from 'express';
import { parse } from 'url';

const getDoublePassportList = () => {
  const doublePassportListDataSource: DoublePassportHead[] = [];
  for (let i = 0; i < 21; i++) {
    doublePassportListDataSource.push({
      preNo: `5165X2100000146487${i + 1}`,
      workNo: `5165X2100000146487${i + 1}`,
      passportNo: 'Z5165I211109T00000000039',
      masterCuscd: '5165|南沙保税',
      dclTypecd: '1',
      areainEtpsNo: '44306650D5',
      areainEtpsNm: '广州中远海运跨境电子商务有限公司',
      areainEtpsSccd: '9144011619044818X3',
      vehicleNo: '粤AP4477',
      vehicleIcNo: '',
      containerNo: '',
      dclErConc: '13662374788',
      dclEtpsNo: '44306650D5',
      dclEtpsNm: '广州中远海运跨境电子商务有限公司',
      dclEtpsSccd: '9144011619044818X3',
      inputCode: '44306650D5',
      inputCreditCode: '44306650D5',
      inputName: '广州中远海运跨境电子商务有限公司',
      note: '1',
      delcareFlag: '1',
      approveStatus: '1',
    });
  }
  return doublePassportListDataSource;
};

const doublePassportListDataSource: DoublePassportHead[] = getDoublePassportList();

function getDoublePassport(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query;

  let dataSource = [...doublePassportListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: dataSource,
    total: doublePassportListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/doublePassport': getDoublePassport,
};
