import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { DoublePassportHead } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<DoublePassportHead>>();
  return (
    <ProForm<DoublePassportHead>
      formRef={formRef}
      formKey="passportCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="企业内部编号" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="IEMark"
            label="进出标志"
            valueEnum={{
              I: '进区',
              E: '出区',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>

        <Col span={8}>
          <ProFormText
            name="carNo"
            label="车牌号"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="customsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="icCode" label="IC卡号" />
        </Col>
        <Col span={8}>
          <ProFormText name="fContaNo" label="箱号" />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeCode"
            label="区内企业编码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeName"
            label="区内企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="areainEtpsSccd" label="区内企业社会信用代码" />
        </Col>
        <Col span={8}>
          <ProFormText
            name="dclTypecd"
            label="申报类型"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="dclErConc"
            label="申请人及联系方式"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
          />
        </Col>
        <Col span={16}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
