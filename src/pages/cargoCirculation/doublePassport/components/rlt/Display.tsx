import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    title: '关联单证号',
    dataIndex: 'entryId',
    align: 'center',

    sorter: true,
  },
];

const RltDisplay: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="BomSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="关联单证号">
              <Form.Item name="entryId" noStyle>
                <Input name="entryId" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default RltDisplay;
