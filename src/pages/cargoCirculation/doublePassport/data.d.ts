export type DoublePassportHead = {
  preNo?: string;
  workNo?: string;
  passportNo?: string;
  masterCuscd?: string;
  dclTypecd?: string;
  areainEtpsNo?: string;
  areainEtpsNm?: string;
  areainEtpsSccd?: string;
  vehicleNo?: string;
  vehicleIcNo?: string;
  containerNo?: string;
  dclErConc?: string;
  dclEtpsNo?: string;
  dclEtpsNm?: string;
  dclEtpsSccd?: string;
  inputCode?: string;
  inputCreditCode?: string;
  inputName?: string;
  note?: string;
  delcareFlag?: string;
  approveStatus?: string;
};

export type DoublePassportRlt = {
  preNo?: string;
  entryId?: string;
};
