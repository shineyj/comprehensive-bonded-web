import { SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { Button, Card } from 'antd';
import React from 'react';

import HeadForm from './head/EditForm';
type CreateProps = {
  onBack: () => void;
};

const Create: React.FC<CreateProps> = (props) => {
  const extraAction = (
    <>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        style={{ marginLeft: '10px' }}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </>
  );
  return (
    <Card title="录入核放单授权信息" extra={extraAction}>
      <HeadForm />
    </Card>
  );
};
export default Create;
