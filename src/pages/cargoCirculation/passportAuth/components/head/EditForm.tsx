import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { PassportAuth } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<PassportAuth>>();
  return (
    <ProForm<PassportAuth>
      formRef={formRef}
      formKey="passportCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '180px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="授权系统编号" disabled />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="授权企业海关编码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="授权企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="授权企业社会信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="被授权企业海关编码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="被授权企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="workNo"
            label="被授权企业社会信用代码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="BlsType"
            label="业务单据类型"
            valueEnum={{
              1: '核注清单',
              2: '出入库单',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="AuthType"
            label="权限类型"
            valueEnum={{
              1: '授予权限',
              0: '取消授权',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
