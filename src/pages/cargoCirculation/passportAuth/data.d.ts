export type PassportAuth = {
  preNo?: string;
  SysId?: string;
  EtpsNo?: string;
  EtpsSccd?: string;
  EtpsName?: string;
  BlsType?: string;
  BlsNo?: string;
  SyEtpsNo?: string;
  SyEtpsSccd?: string;
  SyEtpsName?: string;
  AuthType?: string;
};
