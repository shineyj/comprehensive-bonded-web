import {
  PlusOutlined,
  ImportOutlined,
  DeleteOutlined,
  SendOutlined,
  ExportOutlined,
  PrinterOutlined,
} from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';

import { Button } from 'antd';
import { useRef, useState } from 'react';
import { history } from 'umi';
import type { PassportHead } from './data';

import Create from './components/Create';
import { passport } from './service';
import Detail from './components/Detail';

const columns: ProColumns<PassportHead>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '预录入号',
    dataIndex: 'preNo',
    width: 200,
    align: 'center',
    copyable: true,
    order: 10,
  },
  {
    title: '企业内部编号',
    dataIndex: 'workNo',
    width: 200,
    align: 'center',
    copyable: true,
    order: 9,
  },
  {
    title: '核放单编号',
    dataIndex: 'passportNo',
    width: 150,
    align: 'center',
    order: 2,
  },
  {
    title: '申报类型',
    width: 120,
    dataIndex: 'passportNo',
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: '备案',
      3: '作废',
    },
  },
  {
    title: '数据状态',
    dataIndex: 'approveStatus',
    width: 140,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      0: { text: '预录入' },
      1: { text: '审批通过' },
      2: { text: '退单' },
      3: { text: '转人工' },
    },
    order: 7,
  },
  {
    title: '车牌号',
    dataIndex: 'carNo',
    width: 140,
    align: 'center',
    order: 3,
  },
  {
    title: 'IC卡号',
    dataIndex: 'icCode',
    width: 140,
    align: 'center',
  },
  {
    title: '核放单类型',
    dataIndex: 'passportTypecd',
    width: 180,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: '先入区后报关',
      2: '一线一体化进出区',
      3: '二线进出区',
      4: '非报关进出区',
      5: '卡口登记货物',
      6: '空车进出区',
    },
    order: 6,
  },
  {
    title: '绑定类型',
    dataIndex: 'bindTypecd',
    width: 180,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: '一车多票',
      2: '一票一车',
      3: '一票多车',
    },
    order: 4,
  },
  {
    title: '关联单证类型',
    dataIndex: 'rltTbTypecd',
    width: 180,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      1: '核注清单',
      2: '出入库单',
      3: '提运单',
    },
  },
  {
    title: '关联单证号',
    dataIndex: 'rltNo',
    width: 180,
    align: 'center',
    order: 9,
  },
  {
    title: '经营单位编号',
    dataIndex: 'tradeCode',
    width: 140,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '经营企业名称',
    dataIndex: 'tradeName',
    width: 360,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTime',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '审批时间',
    dataIndex: 'approveDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
  },
  {
    title: '录入时间',
    dataIndex: 'inputTimeS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 2,
  },
  {
    title: '申报时间',
    dataIndex: 'declareDateS',
    width: 260,
    align: 'center',
    valueType: 'dateRange',
    hideInTable: true,
    order: 3,
  },
];

export default () => {
  const pageType = history.location.pathname.substring(
    history.location.pathname.lastIndexOf('/') + 1,
  );

  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<PassportHead[]>([]);

  /**
   * 显示模式：create-录入界面、list-列表、detail-详情
   */
  const [showType, handleShowType] = useState<string>('list');

  const [doubleClickRowState, setDoubleClickRowState] = useState<PassportHead>();

  const listTable = (
    <>
      <ProTable<PassportHead>
        columns={columns}
        actionRef={actionRef}
        request={passport}
        pagination={{
          pageSize: 10,
        }}
        rowKey="preNo"
        bordered
        scroll={{
          x: 2500,
        }}
        rowSelection={{
          onChange: (_, selectedRow) => {
            setSelectedRows(selectedRow);
          },
        }}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={() => [
          'declare' == pageType && (
            <Button
              icon={<PlusOutlined />}
              onClick={() => {
                handleShowType('create');
              }}
            >
              录入
            </Button>
          ),
          'declare' == pageType && <Button icon={<ImportOutlined />}>导入</Button>,
          'declare' == pageType && (
            <Button icon={<DeleteOutlined />} danger disabled={selectedRowsState.length == 0}>
              删除
            </Button>
          ),
          'search' != pageType && (
            <Button
              icon={<SendOutlined />}
              type="primary"
              ghost
              disabled={selectedRowsState.length == 0}
            >
              申报
            </Button>
          ),
          <Button icon={<ExportOutlined />}>导出</Button>,
          <Button icon={<PrinterOutlined />}>打印</Button>,
        ]}
        onRow={(record) => {
          return {
            onClick: (event) => {}, // 点击行
            onDoubleClick: (event) => {
              setDoubleClickRowState(record);
              handleShowType('detail');
            },
            onContextMenu: (event) => {},
            onMouseEnter: (event) => {}, // 鼠标移入行
            onMouseLeave: (event) => {},
          };
        }}
      />
    </>
  );
  switch (showType) {
    case 'list':
      return listTable;
      break;
    case 'create':
      return (
        <Create
          onBack={() => {
            handleShowType('list');
          }}
        />
      );
      break;
    case 'detail':
      return <Detail record={doubleClickRowState} onBack={() => handleShowType('list')} />;
      break;
    default:
      return listTable;
  }
};
