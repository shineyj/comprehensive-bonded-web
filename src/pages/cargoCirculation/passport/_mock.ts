import type { PassportHead } from './data.d';
import type { Request, Response } from 'express';
import { parse } from 'url';

const getpassportList = () => {
  const passportListDataSource: PassportHead[] = [];
  for (let i = 0; i < 21; i++) {
    passportListDataSource.push({
      preNo: `0100X210000000000${i + 1}`,
      passportNo: `Z5165E21110900000000000${i + 1}`,
      passportTypecd: '1',
      IEMark: 'I',
      bindTypecd: '1',
      rltTbTypecd: '1',
      carNo: '浙A3D121',
      carWt: '1',
      icCode: '1',
      fContaNo: '1',
      fContaType: '1',
      fContaWt: '1',
      rltNo: 'QD516521E000137882',
      carframeNo: '',
      carframeWt: '1',
      totalWt: '1',
      totalNewWt: '1',
      goodsWt: '1',
      tradeCode: '443066K504',
      tradeName: '广州市宸烨物流有限公司',
      areainEtpsSccd: '914401150941678472',
      workNo: `0100X210000000000${i + 1}`,
      dclErConc: '111111111',
      templateId: '',
      customsCode: '北京海关',
      zsFlag: '1',
      col1: '1',
      delcareFlag: '1',
      note: '1',
      approveStatus: '1',
    });
  }
  return passportListDataSource;
};

const passportListDataSource: PassportHead[] = getpassportList();

function getPassport(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query;

  let dataSource = [...passportListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: dataSource,
    total: passportListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/passport': getPassport,
};
