import type { ProFormInstance } from '@ant-design/pro-form';
import { ProFormDatePicker } from '@ant-design/pro-form';
import { ProFormSelect } from '@ant-design/pro-form';
import { ProFormText } from '@ant-design/pro-form';
import ProForm from '@ant-design/pro-form';
import { useRef } from 'react';
import type { PassportHead } from '../../data';
import { Col, Row } from 'antd';

const HeadForm: React.FC = () => {
  const formRef = useRef<ProFormInstance<PassportHead>>();
  return (
    <ProForm<PassportHead>
      formRef={formRef}
      formKey="passportCreateForm"
      autoFocusFirstInput
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="workNo" label="企业内部编号" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="passportTypecd"
            label="核放单类型"
            valueEnum={{
              1: '先入区后报关',
              2: '一线一体化进出区',
              3: '二线进出区',
              4: '非报关进出区',
              5: '卡口登记货物',
              6: '空车进出区',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="IEMark"
            label="进出标志"
            valueEnum={{
              I: '进区',
              E: '出区',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="bindTypecd"
            label="绑定类型"
            valueEnum={{
              1: '一车多票',
              2: '一票一车',
              3: '一票多车',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="rltTbTypecd"
            label="关联单证类型"
            valueEnum={{
              1: '核注清单',
              2: '出入库单',
              3: '提运单',
            }}
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="carNo"
            label="车牌号"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="carWt"
            label="车自重"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="icCode" label="IC卡号" />
        </Col>
        <Col span={8}>
          <ProFormText name="fContaNo" label="箱号" />
        </Col>
        <Col span={8}>
          <ProFormText name="fContaType" label="箱型" />
        </Col>
        <Col span={8}>
          <ProFormText name="fContaWt" label="箱重" />
        </Col>
        <Col span={8}>
          <ProFormText name="rltNo" label="关联单证编号	" />
        </Col>
        <Col span={8}>
          <ProFormText name="carframeNo" label="车架号" />
        </Col>
        <Col span={8}>
          <ProFormText name="carframeWt" label="车架重" />
        </Col>
        <Col span={8}>
          <ProFormText name="totalWt" label="总重量" />
        </Col>
        <Col span={8}>
          <ProFormText name="totalNewWt" label="货物总净重" />
        </Col>
        <Col span={8}>
          <ProFormText name="goodsWt" label="货物总毛重" />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeCode"
            label="区内企业编码"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="tradeName"
            label="区内企业名称"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="areainEtpsSccd" label="区内企业社会信用代码" />
        </Col>
        <Col span={8}>
          <ProFormText
            name="dclErConc"
            label="申请人及联系方式"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="dclTypecd"
            label="申报类型"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="passTime" label="过一卡时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="secdPassTime" label="过二卡时间" disabled />
        </Col>
        <Col span={8}>
          <ProFormText
            name="customsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="declareFlag"
            label="申报标志"
            valueEnum={{
              0: '暂存',
              1: '申报',
            }}
          />
        </Col>
        <Col span={24}>
          <ProFormText name="note" label="备注" />
        </Col>
      </Row>
    </ProForm>
  );
};

export default HeadForm;
