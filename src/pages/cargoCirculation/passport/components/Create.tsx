import { AppleOutlined, AndroidOutlined, SendOutlined, RollbackOutlined } from '@ant-design/icons';
import { Button, Card, Tabs } from 'antd';
import React from 'react';

import HeadForm from './head/EditForm';
import ListDisplay from './list/Display';
import AcmpDisplay from './acmp/Display';

type CreateProps = {
  onBack: () => void;
};

const { TabPane } = Tabs;

const Create: React.FC<CreateProps> = (props) => {
  const extraAction = (
    <>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        style={{ marginLeft: '10px' }}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </>
  );
  return (
    <Card title="录入核放单信息" extra={extraAction}>
      <Tabs defaultActiveKey="head">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              核放单表头
            </span>
          }
          key="head"
        >
          <HeadForm />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              核放单表体
            </span>
          }
          key="list"
        >
          <ListDisplay />
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              核放单关联单证表体
            </span>
          }
          key="Acmp"
        >
          <AcmpDisplay />
        </TabPane>
      </Tabs>
    </Card>
  );
};
export default Create;
