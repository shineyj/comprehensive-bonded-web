// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import type { PassportHead } from './data.d';

/** DcrHead */
/** 获取核放单表头列表 GET /api/passport */

export async function passport(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: PassportHead[];
    total?: number;
    success?: boolean;
  }>('/api/passport', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
