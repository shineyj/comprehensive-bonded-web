import { Request, Response } from 'express';
import { InvtTableItem } from './data.d';

function genInvtList(pageSize: number) {
  const tableListDataSource: InvtTableItem[] = [];

  for (let i = 0; i < pageSize; i += 1) {
    tableListDataSource.push({
      invt_preent_no: `GZHZ010021I00001${i}`,
      bond_invt_no: `GZHZ010021I00001${i}`,
      work_no: `WorkNo${i}`,
      ems_no: `L0100A18A13${i}`,
      step_id: 'G2Invt100100',
      invt_stucd: '1',
      bond_invt_typecd: '0',
      create_date: new Date('2020-12-10'),
      apply_no: '',
      trade_code: '1100696075',
      trade_name: '北京世纪周峰商业设备有限公司',
      declare_date: new Date('2020-12-10'),
      approve_date: new Date('2020-12-10'),
    });
  }

  tableListDataSource.reverse();
  return tableListDataSource;
}

function getInvtPageList(req: Request, res: Response) {
  const dataSource = genInvtList(10);
  return res.json({
    data: dataSource,
    total: 50,
    success: true,
  });
}

export default {
  'GET /api/invt': getInvtPageList,
};
