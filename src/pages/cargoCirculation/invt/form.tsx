import {
  getCountryPickerSrcConfig,
  getCustomsPickerSrcConfig,
  getEmsPickerSrcConfig,
  getOutCompanyPickerSrcConfig,
  getTradePickerSrcConfig,
  getTransfPickerSrcConfig,
} from '@/pages/baseInfo';
import SelectTablePicker from '@/components/SelectTablePicker';
import { getParamList } from '@/services/ant-design-pro/param';
import { ParamKeyEnum } from '@/utils/enums/paramEnum';
import { PlusOutlined } from '@ant-design/icons';
import { ModalForm, ProFormDatePicker, ProFormSelect, ProFormText } from '@ant-design/pro-form';
import { Button, Row, Col, Form } from 'antd';
import { FC, useState } from 'react';
import { InvtHead } from './data.d';

type InvtHeadFormProp = {
  IOFlag: string;
  OnAfterSumbit?: () => void;
};

const InvtHeadForm: FC<InvtHeadFormProp> = (props) => {
  const [submitState, setSubmitSate] = useState(false);
  const [form] = Form.useForm();

  async function submitForm(formData: InvtHead, callBack?: () => void) {
    setSubmitSate(true);
    callBack && callBack();
    setSubmitSate(false);
  }

  return (
    <ModalForm
      form={form}
      title="录入核注清单信息"
      width="1100px"
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
      trigger={<Button icon={<PlusOutlined />}>新增</Button>}
      submitter={{
        submitButtonProps: { loading: submitState },
      }}
      modalProps={{
        destroyOnClose: true,
        bodyStyle: { maxHeight: '550px', overflowY: 'auto' },
      }}
      onFinish={async (formData) => submitForm(formData as InvtHead, props.OnAfterSumbit)}
    >
      <Row>
        <Col span={8}>
          <ProFormText
            name="invt_preent_no"
            label="清单预录入编号"
            disabled={true}
            placeholder=""
          />
        </Col>
        <Col span={8}>
          <ProFormText name="bond_invt_no" label="清单正式编号" disabled={true} placeholder="" />
        </Col>
        <Col span={8}>
          <ProFormText name="work_no" label="企业内部清单编号" rules={[{ required: true }]} />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="bond_invt_typecd"
            label="核注清单类型"
            rules={[{ required: true }]}
            request={() => getParamList(ParamKeyEnum.BOND_INVT_TYPECD)}
          />
        </Col>
        <Col span={8}>
          {/* <ProFormSelect name="ems_no" label="备案号" rules={[{ required: true }]} /> */}
          <SelectTablePicker
            name="ems_no"
            label="备案号（底账编号）"
            rules={[{ required: true }]}
            {...getEmsPickerSrcConfig()}
            params={{ good: 1 }}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="i_e_flag"
            label="进出口标记"
            rules={[{ required: true }]}
            disabled={true}
            initialValue={props.IOFlag}
            request={() => getParamList(ParamKeyEnum.IO_FLAG)}
          />
        </Col>

        <Col span={8}>
          {/* <ProFormSelect name="trade_code" label="经营单位编码" rules={[{ required: true }]} /> */}
          <SelectTablePicker
            name="trade_code"
            label="经营单位编码"
            rules={[{ required: true }]}
            {...getOutCompanyPickerSrcConfig()}
            onSelected={(recode) => {
              form.setFieldsValue({
                trade_name: recode.provider_name,
                bizop_etps_sccd: recode.uscCode,
              });
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="trade_name" label="经营单位名称" rules={[{ required: true }]} />
        </Col>
        <Col span={8}>
          <ProFormText
            name="bizop_etps_sccd"
            label="经营单位信用代码"
            rules={[{ required: true }]}
          />
        </Col>

        <Col span={8}>
          <SelectTablePicker
            name="owner_code"
            label="收发货单位编码"
            rules={[{ required: true }]}
            {...getOutCompanyPickerSrcConfig()}
            onSelected={(recode) => {
              form.setFieldsValue({
                owner_name: recode.provider_name,
                rvsngd_etps_sccd: recode.uscCode,
              });
            }}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="owner_name" label="收发货单位名称" />
        </Col>
        <Col span={8}>
          <ProFormText name="rvsngd_etps_sccd" label="收发货单位信用代码" />
        </Col>

        <Col span={8}>
          <SelectTablePicker
            name="declare_code"
            label="申报单位代码"
            rules={[{ required: true }]}
            {...getOutCompanyPickerSrcConfig()}
            onSelected={(recode) => {
              form.setFieldsValue({
                declare_name: recode.provider_name,
                dcl_etps_sccd: recode.uscCode,
              });
            }}
          />
          {/* <ProFormSelect name="declare_code" label="申报单位代码" rules={[{ required: true }]} /> */}
        </Col>
        <Col span={8}>
          <ProFormText name="declare_name" label="申报单位名称" />
        </Col>
        <Col span={8}>
          <ProFormText name="dcl_etps_sccd" label="申报单位信用代码" />
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="dclcus_flag"
            label="报关标志"
            rules={[{ required: true }]}
            request={() => getParamList(ParamKeyEnum.DCLCUS_FLAG)}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="dclcus_typecd"
            label="报关类型"
            request={() => getParamList(ParamKeyEnum.DCLCUS_TYPECD)}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="entry_type"
            label="报关单类型"
            request={() => getParamList(ParamKeyEnum.ENTRY_TYPE)}
          />
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="m_p"
            label="料件/成品标志"
            rules={[{ required: true }]}
            request={() => getParamList(ParamKeyEnum.M_P)}
          />
        </Col>
        <Col span={8}>
          <SelectTablePicker
            name="trade_mode"
            label="监管方式"
            rules={[{ required: true }]}
            {...getTradePickerSrcConfig()}
          />
          {/* <ProFormSelect name="trade_mode" label="监管方式" rules={[{ required: true }]} /> */}
        </Col>
        <Col span={8}>
          <SelectTablePicker
            name="traf_mode"
            label="运输方式"
            rules={[{ required: true }]}
            {...getTransfPickerSrcConfig()}
          />
          {/* <ProFormSelect
            name="traf_mode"
            label="运输方式"
            rules={[{ required: true }]}
          /> */}
        </Col>

        <Col span={8}>
          <SelectTablePicker
            name="i_e_port"
            label="进出境关别"
            rules={[{ required: true }]}
            {...getCustomsPickerSrcConfig()}
          />
          {/* <ProFormSelect name="i_e_port" label="进出境关别" rules={[{ required: true }]} /> */}
        </Col>
        <Col span={8}>
          <SelectTablePicker
            name="decl_port"
            label="申报地海关"
            rules={[{ required: true }]}
            {...getCustomsPickerSrcConfig()}
          />
          {/* <ProFormSelect name="decl_port" label="申报地海关" rules={[{ required: true }]} /> */}
        </Col>
        <Col span={8}>
          <SelectTablePicker
            name="trade_country"
            label="起运/运抵国(地区)"
            rules={[{ required: true }]}
            {...getCountryPickerSrcConfig()}
          />
          {/* <ProFormSelect
            name="trade_country"
            label="起运/运抵国(地区)"
            disabled={true}
            rules={[{ required: true }]}
          /> */}
        </Col>

        <Col span={8}>
          <ProFormText name="port_status" label="清单进出卡口状态" disabled={true} />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="transfer_mode"
            label="流转类型"
            request={() => getParamList(ParamKeyEnum.TRANSFER_MODE)}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="apply_no" label="申报表编号" />
        </Col>

        <Col span={8}>
          <ProFormSelect name="vrfded_markcd" label="核扣标志" disabled />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="prevd_time"
            label="预核扣日期"
            fieldProps={{ style: { width: '100%' } }}
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="formal_vrfded_time"
            label="正式核扣日期"
            fieldProps={{ style: { width: '100%' } }}
            disabled
          />
        </Col>

        <Col span={8}>
          <ProFormText name="entry_id" label="报关单编号" disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect name="area_code" label="区域代码" />
        </Col>
        <Col span={8}>
          <ProFormText name="invt_stucd" label="清单状态" disabled />
        </Col>

        <Col span={8}>
          <ProFormText name="relative_id" label="关联报关单编号" />
        </Col>
        <Col span={8}>
          <ProFormText name="correspond_no" label="关联清单编号" />
        </Col>
        <Col span={8}>
          <ProFormText name="rlt_putrec_no" label="关联备案号" />
        </Col>

        <Col span={8}>
          <ProFormText name="entry_no" label="对应报关单编号" />
        </Col>
        <Col span={8}>
          <ProFormText name="body_count" label="商品项数" />
        </Col>
        <Col span={8}>
          <SelectTablePicker
            name="customscode"
            label="主管海关"
            rules={[{ required: true }]}
            {...getCustomsPickerSrcConfig()}
          />
          {/* <ProFormSelect name="customscode" label="主管海关" required /> */}
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="function_code"
            label="功能区"
            request={() => getParamList(ParamKeyEnum.FUNCTION_COD)}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect name="gen_decflag" label="是否生成报关" />
        </Col>
        <Col span={8}>
          <ProFormSelect name="delcareflag" label="申报标志" />
        </Col>

        <Col span={8}>
          <ProFormText name="relative_doc_no" label="关联单证号" />
        </Col>
        <Col span={8}>
          <ProFormText name="gross_wt" label="总毛重" />
        </Col>
        <Col span={8}>
          <ProFormText name="pack_no" label="总件数" />
        </Col>

        <Col span={24}>
          <ProFormText name="remark" label="备注" />
        </Col>

        <Col span={8}>
          <ProFormText name="input_code" label="录入单位代码" disabled required />
        </Col>
        <Col span={8}>
          <ProFormText name="input_name" label="录入单位名称" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="copcodescc" label="录入单位信用代码" disabled />
        </Col>

        <Col span={8}>
          <ProFormDatePicker
            name="d_date"
            label="报关单申报日期"
            fieldProps={{ style: { width: '100%' } }}
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="create_date"
            label="录入日期"
            fieldProps={{ style: { width: '100%' } }}
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="declare_date"
            label="清单申报日期"
            fieldProps={{ style: { width: '100%' } }}
            disabled
          />
        </Col>

        <Col span={8}>
          <ProFormText name="op_ic_card" label="申报人IC卡号" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="op_cert_no" label="申报人证书号" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="chg_tms_cnt" label="修改次数（删除）" disabled />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default InvtHeadForm;
