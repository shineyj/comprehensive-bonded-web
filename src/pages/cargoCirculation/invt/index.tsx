import type { FC } from 'react';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { InvtTableItem } from './data';
import { getParamList, getStepParameterList } from '@/services/ant-design-pro/param';
import { ParamKeyEnum } from '@/utils/enums/paramEnum';
import { getLastPathName, getLastPathNameByIndex } from '@/utils/localtionHelper';
import { Button } from 'antd';
import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PrinterOutlined,
  RollbackOutlined,
  SendOutlined,
} from '@ant-design/icons';
import { getInvtPageList } from './service';
import { useState } from 'react';
import HeadModalForm from './form';
import Detail from './detail';
import InvtCollectForm from './form-collect';

async function getStepParams() {
  const pageType = getLastPathNameByIndex(0);
  return await getStepParameterList(pageType);
}

function createActionColumnRender(): JSX.Element[] {
  return [<a>申报</a>, <a>删除</a>];
}

function getTableColumns(): ProColumns<InvtTableItem>[] {
  return [
    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'index',
      width: 45,
      align: 'center',
      fixed: 'left',
    },
    {
      title: '操作',
      valueType: 'option',
      width: 120,
      fixed: 'left',
      align: 'center',
      render: createActionColumnRender,
    },
    {
      title: '预录入统一编号',
      dataIndex: 'invt_preent_no',
      width: 180,
      align: 'center',
    },
    {
      title: '核注清单编号',
      dataIndex: 'bond_invt_no',
      width: 180,
      align: 'center',
    },
    {
      title: '企业内部编号',
      dataIndex: 'work_no',
      width: 150,
      align: 'center',
    },
    {
      title: '账册编号',
      dataIndex: 'ems_no',
      width: 120,
      align: 'center',
    },
    {
      title: '数据状态',
      dataIndex: 'step_id',
      width: 80,
      valueType: 'select',
      align: 'center',
      request: getStepParams,
    },
    {
      title: '清单状态',
      dataIndex: 'invt_stucd',
      width: 80,
      valueType: 'select',
      align: 'center',
      request: () => getParamList(ParamKeyEnum.INVT_STUCD),
    },
    {
      title: '核注清单类型',
      dataIndex: 'bond_invt_typecd',
      width: 120,
      valueType: 'select',
      align: 'center',
      request: () => getParamList(ParamKeyEnum.BOND_INVT_TYPECD),
    },
    {
      title: '录入起止时间',
      dataIndex: 'create_dates',
      width: 120,
      valueType: 'dateRange',
      align: 'center',
      hideInTable: true,
    },
    {
      title: '录入时间',
      dataIndex: 'create_date',
      width: 120,
      valueType: 'date',
      align: 'center',
      search: false,
    },
    {
      title: '申报表编号',
      dataIndex: 'apply_no',
      width: 150,
      align: 'center',
    },
    {
      title: '经营企业编号',
      dataIndex: 'trade_code',
      width: 120,
      search: false,
      align: 'center',
    },
    {
      title: '经营企业名称',
      dataIndex: 'trade_name',
      width: 250,
      search: false,
      align: 'center',
      ellipsis: true,
    },
    // {
    //   title: '关联单证号',
    //   dataIndex: 'relative_doc_no',
    //   width: 180,
    //   align: 'center',
    // },
    {
      title: '申报起止时间',
      dataIndex: 'declare_dates',
      width: 180,
      align: 'center',
      valueType: 'dateRange',
      hideInTable: true,
    },
    {
      title: '申报时间',
      dataIndex: 'declare_date',
      width: 180,
      align: 'center',
      valueType: 'date',
      search: false,
    },
    {
      title: '审批起止时间',
      dataIndex: 'approve_dates',
      width: 180,
      align: 'center',
      valueType: 'dateRange',
      hideInTable: true,
    },
    {
      title: '审批时间',
      dataIndex: 'approve_date',
      width: 180,
      align: 'center',
      valueType: 'date',
      search: false,
    },
  ];
}

function createTableToolbarRender(isSelected: boolean): JSX.Element[] {
  const btnDel = (
    <Button key="invt_delete" icon={<DeleteOutlined />} danger disabled={!isSelected}>
      删除
    </Button>
  );
  const btnBack = (
    <Button key="invt_back" icon={<RollbackOutlined />} danger disabled={!isSelected}>
      退单
    </Button>
  );
  const btnApply = (
    <Button key="invt_apply" icon={<SendOutlined />} type="primary" ghost disabled={!isSelected}>
      申报
    </Button>
  );
  const btnImport = (
    <Button key="invt_import" icon={<ImportOutlined />}>
      导入
    </Button>
  );
  const btnExport = (
    <Button key="invt_export" icon={<ExportOutlined />}>
      导出
    </Button>
  );
  const btnPrint = (
    <Button key="invt_print" icon={<PrinterOutlined />}>
      打印
    </Button>
  );

  const pageType = getLastPathName();
  if (pageType == 'declare') {
    const ioFlag = getLastPathNameByIndex(1) == 'in' ? 'I' : 'E';
    const btnAdd = <HeadModalForm IOFlag={ioFlag} />;

    return [btnAdd, btnImport, btnDel, btnApply, btnExport, btnPrint];
  }
  if (pageType == 'collect') {
    const btnAdd = <InvtCollectForm />;
    return [btnAdd, btnImport, btnDel, btnBack, btnApply, btnExport, btnPrint];
  }

  return [];
}

const InvtList: FC = () => {
  const [selectedRows, setSelectedRows] = useState<Partial<InvtTableItem>[]>([]);
  const [isShowDetail, setIsShowDetail] = useState(false);

  return (
    <>
      <ProTable<InvtTableItem>
        style={{ display: isShowDetail ? 'none' : 'block' }}
        columns={getTableColumns()}
        search={{
          labelWidth: 100,
        }}
        bordered
        scroll={{
          x: 2000,
        }}
        rowKey="invt_preent_no"
        request={getInvtPageList}
        pagination={{
          pageSize: 10,
        }}
        rowSelection={{
          fixed: true,
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
        toolBarRender={() => createTableToolbarRender(!!selectedRows.length)}
        onRow={(record) => {
          return {
            onDoubleClick: (event) => {
              setIsShowDetail(true);
            },
          };
        }}
      />

      <Detail
        isShow={isShowDetail}
        pageType={getLastPathName()}
        onBack={() => {
          setIsShowDetail(false);
        }}
      />
    </>
  );
};

export default InvtList;
