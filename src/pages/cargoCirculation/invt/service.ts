import { InvtTableQueryParam, InvtTableListResult } from './data.d';
import { request } from 'umi';

export function getInvtPageList(params: InvtTableQueryParam) {
  return request<API.ApiPageResult<InvtTableListResult>>('/api/invt', { method: 'GET', params });
}
