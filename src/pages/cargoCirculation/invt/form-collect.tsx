import { CheckOutlined, PlusOutlined, SearchOutlined, SwapLeftOutlined } from '@ant-design/icons';
import ProForm, { ProFormDateRangePicker, ProFormText } from '@ant-design/pro-form';
import { useState } from 'react';

import { Button, Col, Row, Table, Modal, Space } from 'antd';
import SelectTablePicker from '@/components/SelectTablePicker';
import { getApplyPickerSrcConfig } from '@/pages/baseInfo';

const columns: Record<string, any>[] = [
  {
    title: '出入库单编号',
    dataIndex: 'sasStockNo',
    width: 120,
    align: 'center',
  },
  {
    title: '出入库单预录入编号',
    dataIndex: 'gateJobNo',
    width: 180,
    align: 'center',
  },
  {
    title: '申报表预录入编号',
    dataIndex: 'gatepassNo',
    width: 180,
    align: 'center',
  },
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    width: 120,
    align: 'center',
  },
  {
    title: '区内企业编号',
    dataIndex: 'tradeCode',
    width: 120,
    align: 'center',
  },
  {
    title: '区内企业名称',
    dataIndex: 'tradeName',
    width: 120,
    align: 'center',
  },
  {
    title: '业务类型',
    dataIndex: 'bizTypeName',
    width: 120,
    align: 'center',
  },
  {
    title: '进出标记',
    dataIndex: 'ieMarkName',
    width: 120,
    align: 'center',
  },
  {
    title: '审核通过日期',
    dataIndex: 'approveDate',
    width: 120,
    align: 'center',
  },
];

const InvtCollectForm: React.FC = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  return (
    <>
      <Button
        icon={<PlusOutlined />}
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        新增
      </Button>
      <Modal
        title="集报新增"
        visible={isModalVisible}
        width="1100px"
        onCancel={() => {
          setIsModalVisible(false);
        }}
      >
        <ProForm
          layout="horizontal"
          labelAlign="right"
          labelCol={{ style: { width: '140px' } }}
          submitter={{
            render: (props, _) => {
              return (
                <Space
                  style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '20px' }}
                >
                  <Button
                    type="primary"
                    icon={<SearchOutlined />}
                    ghost
                    onClick={() => props.form?.submit?.()}
                  >
                    查询
                  </Button>
                  <Button icon={<SwapLeftOutlined />} onClick={() => props.form?.resetFields()}>
                    重置
                  </Button>
                  <Button icon={<CheckOutlined />}>生成核注清单</Button>
                </Space>
              );
            },
          }}
          onFinish={async (formData) => {}}
        >
          <Row gutter={20}>
            <Col span={12}>
              <SelectTablePicker
                label="申报表预录入编号"
                name="gatepassNo"
                rules={[{ required: true }]}
                {...getApplyPickerSrcConfig()}
              />
            </Col>
            <Col span={12}>
              <ProFormDateRangePicker
                label="审核通过日期"
                name="approveDates"
                fieldProps={{ style: { width: '100%' } }}
              />
            </Col>
            <Col span={12}>
              <ProFormText label="出入库单预录入编号" name="gatejobNo" />
            </Col>
            <Col span={12}>
              <ProFormText label="出入库单正式编号" name="sasStockNo" />
            </Col>
          </Row>
        </ProForm>
        <Table
          bordered
          size="middle"
          scroll={{
            x: 'max-content',
          }}
          columns={columns}
          rowSelection={{ onChange: (selectedKeys) => {} }}
          pagination={{ pageSize: 10 }}
        />
      </Modal>
    </>
  );
};

export default InvtCollectForm;
