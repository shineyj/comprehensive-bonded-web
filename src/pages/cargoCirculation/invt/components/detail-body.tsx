import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';
import ProForm, { ProFormText } from '@ant-design/pro-form';

import { Button, Card, Col, Row, Space, Table } from 'antd';

const columns: Record<string, any>[] = [
  {
    title: '核注清单序号',
    dataIndex: 'g_no',
    width: 120,
    align: 'center',
  },
  {
    title: '底账备案序号',
    dataIndex: 'putrec_seqno',
    width: 120,
    align: 'center',
  },
  {
    title: '商品料号',
    dataIndex: 'cop_g_no',
    width: 100,
    align: 'center',
  },
  {
    title: '报关单商品序号',
    dataIndex: 'entry_gds_seqno',
    width: 140,
    align: 'center',
  },
  {
    title: '流转单商品序号',
    dataIndex: 'apply_tb_seqno',
    width: 140,
    align: 'center',
  },
  {
    title: '商品编码',
    dataIndex: 'code_ts',
    width: 100,
    align: 'center',
  },
  {
    title: '商品名称',
    dataIndex: 'g_name',
    width: 100,
    align: 'center',
  },
  {
    title: '规格型号',
    dataIndex: 'g_model',
    width: 100,
    align: 'center',
  },
  {
    title: '原产国',
    dataIndex: 'country_name',
    width: 100,
    align: 'center',
  },
  {
    title: '最终目的国',
    dataIndex: 'destination_natcd_name',
    width: 120,
    align: 'center',
  },
  {
    title: '申报单价',
    dataIndex: 'dec_price',
    width: 100,
    align: 'center',
  },
  {
    title: '申报总价',
    dataIndex: 'total_price',
    width: 100,
    align: 'center',
  },
  {
    title: '币制',
    dataIndex: 'curr_name',
    width: 100,
    align: 'center',
  },
  {
    title: '申报数量',
    dataIndex: 'app_qty',
    width: 100,
    align: 'center',
  },
  {
    title: '计量单价',
    dataIndex: 'app_unit_name',
    width: 100,
    align: 'center',
  },
  {
    title: '毛重',
    dataIndex: 'gross_wt',
    width: 100,
    align: 'center',
  },
  {
    title: '净重',
    dataIndex: 'net_wt',
    width: 100,
    align: 'center',
  },
  {
    title: '征免方式',
    dataIndex: 'duty_mode_name',
    width: 100,
    align: 'center',
  },
  {
    title: '单耗版本号',
    dataIndex: 'version_no',
    width: 120,
    align: 'center',
  },
];

const DetailBodyList: React.FC = () => {
  return (
    <Card bordered={false}>
      <ProForm layout="horizontal" submitter={false}>
        <Row gutter={20}>
          <Col span={8}>
            <ProFormText label="商品料号" name="cop_g_no" />
          </Col>
          <Col span={8}>
            <ProFormText label="备案序号" name="putrec_seqno" />
          </Col>
          <Col span={8}>
            <ProFormText label="商品编码" name="code_ts" />
          </Col>
        </Row>
        <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '20px' }}>
          <Space>
            <Button type="primary" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button icon={<SwapLeftOutlined />}>重置</Button>
            <Button icon={<PlusOutlined />}>录入</Button>
            <Button icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button icon={<ImportOutlined />}>导入</Button>
            <Button icon={<ExportOutlined />}>导出</Button>
          </Space>
        </div>
      </ProForm>

      <Table
        bordered
        size="middle"
        scroll={{
          x: 2000,
        }}
        columns={columns}
        rowSelection={{ onChange: (selectedKeys) => {} }}
        pagination={{ pageSize: 10 }}
      />
    </Card>
  );
};

export default DetailBodyList;
