import ProDescriptions from '@ant-design/pro-descriptions';
// import { InvtHead } from '../data.d';

const headColumns = [
  {
    title: '清单预录入编号',
    key: 'invt_preent_no',
    dataIndex: 'invt_preent_no',
  },
  {
    title: '清单正式编号',
    key: 'bond_invt_no',
    dataIndex: 'bond_invt_no',
  },
  {
    title: '企业内部清单编号',
    key: 'work_no',
    dataIndex: 'work_no',
  },
  {
    title: '核注清单类型',
    key: 'bond_invt_typecd',
    dataIndex: 'bond_invt_typecd',
  },
  {
    title: '备案号',
    key: 'ems_no',
    dataIndex: 'ems_no',
  },
  {
    title: '进出口标记',
    key: 'i_e_flag',
    dataIndex: 'i_e_flag',
    valueType: 'select',
  },
  {
    title: '经营单位编码',
    key: 'trade_code',
    dataIndex: 'trade_code',
  },
  {
    title: '经营单位名称',
    key: 'trade_name',
    dataIndex: 'trade_name',
  },
  {
    title: '经营单位信用代码',
    key: 'bizop_etps_sccd',
    dataIndex: 'bizop_etps_sccd',
  },
  {
    title: '收发货单位编码',
    key: 'owner_code',
    dataIndex: 'owner_code',
  },
  {
    title: '收发货单位名称',
    key: 'owner_name',
    dataIndex: 'owner_name',
  },
  {
    title: '收发货单位信用代码',
    key: 'rvsngd_etps_sccd',
    dataIndex: 'rvsngd_etps_sccd',
  },
  {
    title: '申报单位代码',
    key: 'declare_code',
    dataIndex: 'declare_code',
  },
  {
    title: '申报单位名称',
    key: 'declare_name',
    dataIndex: 'declare_name',
  },
  {
    title: '申报单位信用代码',
    key: 'dcl_etps_sccd',
    dataIndex: 'dcl_etps_sccd',
  },
  {
    title: '报关标志',
    key: 'dclcus_flag',
    dataIndex: 'dclcus_flag',
    valueType: 'select',
  },
  {
    title: '报关类型',
    key: 'dclcus_typecd',
    dataIndex: 'dclcus_typecd',
    valueType: 'select',
  },
  {
    title: '报关单类型',
    key: 'entry_type',
    dataIndex: 'entry_type',
    valueType: 'select',
  },
  {
    title: '料件/成品标志',
    key: 'm_p',
    dataIndex: 'm_p',
  },
  {
    title: '监管方式',
    key: 'trade_mode',
    dataIndex: 'trade_mode',
  },
  {
    title: '运输方式',
    key: 'traf_mode',
    dataIndex: 'traf_mode',
  },
  {
    title: '进出境关别',
    key: 'i_e_port',
    dataIndex: 'i_e_port',
  },
  {
    title: '申报地海关',
    key: 'decl_port',
    dataIndex: 'decl_port',
  },
  {
    title: '起运/运抵国(地区)',
    key: 'trade_country',
    dataIndex: 'trade_country',
  },
  {
    title: '清单进出卡口状态',
    key: 'port_status',
    dataIndex: 'port_status',
  },
  {
    title: '流转类型',
    key: 'transfer_mode',
    dataIndex: 'transfer_mode',
  },
  {
    title: '申报表编号',
    key: 'apply_no',
    dataIndex: 'apply_no',
  },
  {
    title: '核扣标志',
    key: 'vrfded_markcd',
    dataIndex: 'vrfded_markcd',
  },
  {
    title: '预核扣日期',
    key: 'prevd_time',
    dataIndex: 'prevd_time',
  },
  {
    title: '正式核扣日期',
    key: 'formal_vrfded_time',
    dataIndex: 'formal_vrfded_time',
  },
  {
    title: '报关单编号',
    key: 'entry_id',
    dataIndex: 'entry_id',
  },
  {
    title: '区域代码',
    key: 'area_code',
    dataIndex: 'area_code',
  },
  {
    title: '清单状态',
    key: 'invt_stucd',
    dataIndex: 'invt_stucd',
  },
  {
    title: '关联报关单编号',
    key: 'relative_id',
    dataIndex: 'relative_id',
  },
  {
    title: '关联清单编号',
    key: 'correspond_no',
    dataIndex: 'correspond_no',
  },
  {
    title: '关联备案号',
    key: 'rlt_putrec_no',
    dataIndex: 'rlt_putrec_no',
  },
  {
    title: '对应报关单编号',
    key: 'entry_no',
    dataIndex: 'entry_no',
  },
  {
    title: '商品项数',
    key: 'body_count',
    dataIndex: 'body_count',
  },
  {
    title: '主管海关',
    key: 'customscode',
    dataIndex: 'customscode',
  },
  {
    title: '功能区',
    key: 'function_code',
    dataIndex: 'function_code',
  },
  {
    title: '是否生成报关',
    key: 'gen_decflag',
    dataIndex: 'gen_decflag',
  },
  {
    title: '申报标志',
    key: 'delcareflag',
    dataIndex: 'delcareflag',
  },
  {
    title: '关联单证号',
    key: 'relative_doc_no',
    dataIndex: 'relative_doc_no',
  },
  {
    title: '总毛重',
    key: 'gross_wt',
    dataIndex: 'gross_wt',
  },
  {
    title: '总件数',
    key: 'pack_no',
    dataIndex: 'pack_no',
  },
  {
    title: '备注',
    key: 'remark',
    dataIndex: 'remark',
  },
  {
    title: '录入单位代码',
    key: 'input_code',
    dataIndex: 'input_code',
  },
  {
    title: '录入单位名称',
    key: 'input_name',
    dataIndex: 'input_name',
  },
  {
    title: '录入单位信用代码',
    key: 'copcodescc',
    dataIndex: 'copcodescc',
  },
  {
    title: '报关单申报日期',
    key: 'd_date',
    dataIndex: 'd_date',
    valueType: 'dateTime',
  },
  {
    title: '录入日期',
    key: 'create_date',
    dataIndex: 'create_date',
  },
  {
    title: '清单申报日期',
    key: 'declare_date',
    dataIndex: 'declare_date',
  },
  {
    title: '申报人IC卡号',
    key: 'op_ic_card',
    dataIndex: 'op_ic_card',
  },
  {
    title: '申报人证书号',
    key: 'op_cert_no',
    dataIndex: 'op_cert_no',
  },
  {
    title: '修改次数（删除）',
    key: 'chg_tms_cnt',
    dataIndex: 'chg_tms_cnt',
  },
];

const DetialHead: React.FC = () => {
  return (
    <ProDescriptions
      bordered
      size="small"
      // request={}
      columns={headColumns}
    />
  );
};

export default DetialHead;
