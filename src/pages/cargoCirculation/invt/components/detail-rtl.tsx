import { ExportOutlined, SearchOutlined, SwapLeftOutlined } from '@ant-design/icons';
import ProForm, { ProFormText } from '@ant-design/pro-form';

import { Button, Card, Col, Row, Space, Table } from 'antd';

const columns: Record<string, any>[] = [
  {
    title: '系统预录入号',
    dataIndex: 'gateJobNo',
    width: 120,
    align: 'center',
  },
  {
    title: '出入库单预录入号',
    dataIndex: 'conGatejobNo',
    width: 120,
    align: 'center',
  },
  {
    title: '保税清单编号',
    dataIndex: 'bondInvtNo',
    width: 120,
    align: 'center',
  },
  {
    title: '出入库单编号',
    dataIndex: 'sasStockNo',
    width: 120,
    align: 'center',
  },
];

const DetailRtlList: React.FC = () => {
  return (
    <Card bordered={false}>
      <ProForm
        layout="horizontal"
        submitter={{
          render: (props) => {
            return (
              <Space style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '20px' }}>
                <Button
                  type="primary"
                  icon={<SearchOutlined />}
                  ghost
                  onClick={() => props.form?.submit()}
                >
                  查询
                </Button>
                <Button icon={<SwapLeftOutlined />} onClick={() => props.form?.resetFields()}>
                  重置
                </Button>
                <Button icon={<ExportOutlined />}>导出</Button>
              </Space>
            );
          },
        }}
      >
        <Row gutter={20}>
          <Col span={12}>
            <ProFormText label="出入库单预录入号" name="conGatejobNo" />
          </Col>
          <Col span={12}>
            <ProFormText label="出入库单编号" name="sasStockNo" />
          </Col>
        </Row>
      </ProForm>

      <Table
        bordered
        size="middle"
        scroll={{
          x: 'max-content',
        }}
        columns={columns}
        rowSelection={{ onChange: (selectedKeys) => {} }}
        pagination={{ pageSize: 10 }}
      />
    </Card>
  );
};

export default DetailRtlList;
