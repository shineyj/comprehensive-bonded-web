import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';
import ProForm, { ProFormSelect, ProFormText } from '@ant-design/pro-form';

import { Button, Card, Col, Row, Space, Table } from 'antd';

const columns: Record<string, any>[] = [
  {
    title: '申请单号',
    dataIndex: 'gatejob_no',
    width: 120,
    align: 'center',
  },
  {
    title: '核注清单预录入编号',
    dataIndex: 'invt_preent_no',
    width: 150,
    align: 'center',
  },
  {
    title: '保税清单编号',
    dataIndex: 'bond_invt_no',
    width: 120,
    align: 'center',
  },
  {
    title: '随附单证序号',
    dataIndex: 'acmp_form_seqno',
    width: 120,
    align: 'center',
  },
  {
    title: '清单商品序号',
    dataIndex: 'invt_gds_seqno',
    width: 120,
    align: 'center',
  },
  {
    title: '随附单证类型',
    dataIndex: 'form_typecd_name',
    width: 120,
    align: 'center',
  },
  {
    title: '随附单证编号',
    dataIndex: 'form_no',
    width: 120,
    align: 'center',
  },
  {
    title: '随附单证文件名称',
    dataIndex: 'acmp_form_file_nm',
    width: 180,
    align: 'center',
  },
  {
    title: '备注',
    dataIndex: 'rmk',
    width: 200,
    align: 'center',
  },
];

const DetailAttachedList: React.FC = () => {
  return (
    <Card bordered={false}>
      <ProForm layout="horizontal" submitter={false}>
        <Row gutter={20}>
          <Col span={8}>
            <ProFormSelect label="随附单证类型" name="form_typecd_name" />
          </Col>
          <Col span={8}>
            <ProFormText label="随附单证编号" name="form_no" />
          </Col>
          <Col span={8}>
            <ProFormText label="随附单证文件名称" name="acmp_form_file_nm" />
          </Col>
        </Row>
        <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '20px' }}>
          <Space>
            <Button type="primary" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button icon={<SwapLeftOutlined />}>重置</Button>
            <Button icon={<PlusOutlined />}>录入</Button>
            <Button icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button icon={<ImportOutlined />}>导入</Button>
            <Button icon={<ExportOutlined />}>导出</Button>
          </Space>
        </div>
      </ProForm>

      <Table
        bordered
        size="middle"
        scroll={{
          x: 2000,
        }}
        columns={columns}
        rowSelection={{ onChange: (selectedKeys) => {} }}
        pagination={{ pageSize: 10 }}
      />
    </Card>
  );
};

export default DetailAttachedList;
