import { RollbackOutlined, SendOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Descriptions, Statistic } from 'antd';
import '@/pages/Page.less';
import { FC, useEffect, useState } from 'react';
import DetailHead from './components/detail-head';
import DetailBodyList from './components/detail-body';
import DetailAttachedList from './components/detail-attach';
import ApproveCard from '@/components/ApproveCard';
import DetailRtlList from './components/detail-rtl';

export type InvtDetailProps = {
  isShow: boolean;
  onBack?: () => void;
  pageType?: string;
};

const InvtDetail: FC<InvtDetailProps> = (props) => {
  const [activeTabKey, setActiveTabKey] = useState('head');
  useEffect(() => {
    if (!props.isShow) return;
    // 加载数据
  }, [props.isShow]);

  if (!props.isShow) return <></>;

  function getBaseContent() {
    return (
      <Descriptions size="small" column={2}>
        <Descriptions.Item label="主管海关">黄埔海关</Descriptions.Item>
        <Descriptions.Item label="账册编号">T5165W000075</Descriptions.Item>
        <Descriptions.Item label="经营单位代码">1100696075</Descriptions.Item>
        <Descriptions.Item label="经营单位名称">北京世纪周峰商业设备有限公司</Descriptions.Item>
        <Descriptions.Item label="录入时间">2020-08-13 09:59:51</Descriptions.Item>
        <Descriptions.Item label="申报时间">2020-08-13 09:59:51</Descriptions.Item>
      </Descriptions>
    );
  }

  function getExtraContent() {
    return (
      <div className="moreInfo">
        <Statistic title="申报类型" value="备案申请" />
        <Statistic title="状态" value="待审批" />
      </div>
    );
  }

  function getTabList() {
    const tabList = [
      {
        key: 'head',
        tab: '表头',
      },
      {
        key: 'list',
        tab: '清单表体',
      },
    ];
    if (props.pageType == 'collect') {
      tabList.push({ key: 'rtlList', tab: '集报关联表体' });
    }

    tabList.push(
      {
        key: 'attached',
        tab: '随附单证',
      },
      {
        key: 'approveList',
        tab: '审批历史',
      },
    );

    return tabList;
  }

  const pageContents = {
    head: <DetailHead />,
    list: <DetailBodyList />,
    attached: <DetailAttachedList />,
    approveList: <ApproveCard />,
    rtlList: <DetailRtlList />,
  };

  return (
    <PageContainer
      tabActiveKey={activeTabKey}
      onTabChange={setActiveTabKey}
      className="pageHeader"
      // style={{ display: props.isShow ? 'block' : 'none' }}
      header={{ title: `单号：GZHZ010021I0000141`, breadcrumb: undefined }}
      extra={
        <>
          <Button type="primary" icon={<SendOutlined />}>
            申报
          </Button>
          <Button
            icon={<RollbackOutlined />}
            onClick={() => {
              props.onBack && props.onBack();
            }}
          >
            返回
          </Button>
        </>
      }
      content={getBaseContent()}
      extraContent={getExtraContent()}
      tabList={getTabList()}
    >
      <Card>{pageContents[activeTabKey]}</Card>
    </PageContainer>
  );
};

export default InvtDetail;
