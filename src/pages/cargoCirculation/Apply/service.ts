// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { option } from 'yargs';
import { TableListItem } from './data';

/** 获取规则列表 GET /api/apply */
export async function Apply(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableListItem[];
    total?: number;
    success?: boolean;
  }>('/api/Apply', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
