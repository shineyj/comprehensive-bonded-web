import { RollbackOutlined, SendOutlined } from '@ant-design/icons';
import { GridContent, PageContainer, RouteContext } from '@ant-design/pro-layout';
import { Button, Card, Descriptions, Statistic } from 'antd';
import React, { Fragment, useState } from 'react';

import type { AdvancedState, TableListItem } from '../data';
import stylesDetail from './Detail.less';

import DetailHead from './DetailHead';
import DetailLis from './DetailList';
import DhLis from './dhList';
import AttachDisplay from './attach'
import ApproveCard from '@/components/ApproveCard';
const description = (
  <RouteContext.Consumer>
    {({ isMobile }) => (
      <Descriptions size="small" column={isMobile ? 1 : 2}>
        <Descriptions.Item label="主管海关">黄埔海关</Descriptions.Item>
        <Descriptions.Item label="账册编号">T5165W000075</Descriptions.Item>
        <Descriptions.Item label="经营单位代码">1100696075</Descriptions.Item>
        <Descriptions.Item label="经营单位名称">北京世纪周峰商业设备有限公司</Descriptions.Item>
        <Descriptions.Item label="录入时间">2020-08-13 09:59:51</Descriptions.Item>
        <Descriptions.Item label="申报时间">2020-08-13 09:59:51</Descriptions.Item>
      </Descriptions>
    )}
  </RouteContext.Consumer>
);

const extra = (
  <div className={stylesDetail.moreInfo}>
    <Statistic title="申报类型" value="备案申请" />
    <Statistic title="状态" value="待审批" />
  </div>
);

type ProfileDetail = {
  record: TableListItem | undefined;
  onBack: (flag?: boolean) => void;
};

const contentList = {
  head: <DetailHead />,
  list: <DetailLis />,
  dclList:<DhLis />,
  attached: <AttachDisplay />,
  approveList: <ApproveCard />,
};

const Detail: React.FC<ProfileDetail> = (props) => {
  const [tabStatus, seTabStatus] = useState<AdvancedState>({
    operationKey: 'tab1',
    tabActiveKey: 'head',
  });

  const onTabChange = (tabActiveKey: string) => {
    console.log(tabActiveKey, tabStatus);
    seTabStatus({ ...tabStatus, tabActiveKey });
  };

  const action = (
    <Fragment>
      <Button type="primary" icon={<SendOutlined />}>
        申报
      </Button>
      <Button
        icon={<RollbackOutlined />}
        onClick={() => {
          props.onBack();
        }}
      >
        返回
      </Button>
    </Fragment>
  );

  return (
    <PageContainer
      header={{
        title: '单号：W51656180010',
        breadcrumb: {},
      }}
      extra={action}
      className={stylesDetail.pageHeader}
      content={description}
      extraContent={extra}
      tabActiveKey={tabStatus.tabActiveKey}
      onTabChange={onTabChange}
      tabList={[
        {
          key: 'head',
          tab: '表头',
        },
        {
          key: 'list',
          tab: '表体',
        },
        {
          key: 'dclList',
          tab: '单耗表体',
        },
        {
          key: 'attached',
          tab: '随附单证',
        },
        {
          key: 'approveList',
          tab: '审批历史',
        },
      ]}
    >
      <div className={stylesDetail.main}>
        <GridContent>
          <Card className={stylesDetail.tabsCard} bordered={false}>
            {contentList[tabStatus.tabActiveKey]}
          </Card>
        </GridContent>
      </div>
    </PageContainer>
  );
};

export default Detail;
