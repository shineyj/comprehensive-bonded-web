import {
  ModalForm,
  ProFormDatePicker,
  ProFormDigit,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-form';
import { Col, message, Row } from 'antd';
import React from 'react';
import type { TableListItem } from '../data';

type CreateFormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: TableListItem) => Promise<void>;
  modalVisible: boolean;
  values?: Partial<TableListItem>;
};

const businessTypecd={
  A:'分送集报',
  B:'外发加工',
  C: '保税展示交易',
  D: '设备检测',
  E: '设备维修',
  F:'模具外发',
  G:'简单加工',
  H:'其他业务',
  Y:'一纳企业进出区'
};

const CreateForm: React.FC<CreateFormProps> = (props) => {
  return (
    <ModalForm
      title="录入业务申报单"
      width="1100px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ span: 9 }}
      wrapperCol={{ span: 15 }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      onFinish={props.onSubmit}
      submitter={{
        searchConfig: {
          submitText: '保存',
          resetText: '取消',
        },
      }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="" label="申报表编号" placeholder="" disabled/>
        </Col>
        <Col span={8}>
          <ProFormText name="preNo" label="预录入编号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormSelect name="ieFlag" label="进出标志" valueEnum={{ E: '出口', I: '进口' }} />
        </Col>

        <Col span={8}>
          <ProFormSelect name="businessTypecd" label="业务类型" valueEnum={businessTypecd} rules={[{ required: true, message: '这是必填项' }]} />
        </Col>
        <Col span={8}>
          <ProFormText  name="emsNo"    label="账册编号"  rules={[{ required: true, message: '这是必填项' }]}    />
        </Col>
        <Col span={8}>
          <ProFormText  name="txtAreaOutOriactNo"    label="区外账册编号"     />
        </Col>
        <Col span={8}>
          <ProFormText name="txtTradeCode" label="经营单位代码"  rules={[{required:true,message:'这是必填项'}]} />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtTradeName"
            label="经营单位名称"
            rules={[{ required: true, message: '这是必填项' }]}
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtAreainEtpsSccd"
            label="经营单位信用代码"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText name="txtAreaoutEtpsNo" label="区外企业代码" />
        </Col>
        <Col span={8}>
          <ProFormDigit
            name="txtAreaoutEtpsNm"
            label="区外企业名称"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtAreaoutEtpsSccd"
            label="区外企业信用代码"
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            name="txtContractPeriod"
            label="合同有效期"
            fieldProps={{ style: { width: '100%' } }}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtExhibitionAddress"
            label="展示交易地址"
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtDpstLevyBlNo"
            label="保证金征收编号"
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="txtM_P"
            label="料件性质"
            valueEnum={{I:'料件',E:'成品'}}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtCustomsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="txtCOL1"
            label="边角料标志"
            valueEnum={{1:'成品',
                        2:'残次品',
                        3:'边角料',
                        4:'副产品'}}
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="txtFUNCTION_CODE"
            label="功能区"
            valueEnum={{516501:'物流区',
                        516502:'加工区',
                        516503:'港口二期'}}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtWORK_NO"
            label="企业内部编码"
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="txtDELCAREFLAG"
            label="申报标志"
            valueEnum={{0:'暂存',1:'申报'}}
          />
        </Col>
        <Col span={16}>
          <ProFormText name="note" label="备注" labelCol={{ span: 4 }} wrapperCol={{ span: 20 }} />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default CreateForm;
