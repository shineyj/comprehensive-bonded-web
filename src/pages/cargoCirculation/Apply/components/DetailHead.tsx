import { EditOutlined } from '@ant-design/icons';
import ProDescriptions from '@ant-design/pro-descriptions';
import { Button } from 'antd';

const bwlTypecd = {
  A: '保税物流中心A',
  B: '保税物流中心B',
  D: '公共保税仓库',
  E: '液体保税仓库',
  F: '寄售维修保税仓库',
  G: '暂为空',
  H: '特殊商品保税仓库',
  I: '备料保税仓库',
  P: '出口配送监管仓',
  J: '为国内结转监管仓',
  K: '保税区',
  L: '出口加工区',
  M: '保税物流园区',
  N: '保税港区',
  Z: '综合保税区',
  Q: '跨境工业园区',
  S: '特殊区域设备账册',
};

const DetailHead: React.FC = () => {
  return (
    <ProDescriptions
      title="详细信息"
      bordered
      request={async () => {
        return Promise.resolve({
          success: true,
          data: {
            preNo: 'W51656180010',
            workNo: 'W51656180010',
            emsNo: 'T5165W000075',
            declareType: '1',
            houseTypecd: 'A',
            bwlTypecd: 'A',
            appendTypecd: '1',
            usageTypecd: '1',
            dclEtpsTypecd: '1',
            customsCode: '5165|黄埔海关',
            endDate: '2022-12-31',
            modifyTimes: '0',
            houseNo: '443066K516',
            houseName: '广州亚晋国际货运代理有限公司',
            storeArea: '48016.82',
            storeVol: '111',
            houseAddress:
              '广州市南沙区龙穴岛南沙综合保税区物流区港荣三街天运仓二层5-1至5-12、6-1至6-12、7-1至7-12',
            tradeCode: '443066K516',
            tradeName: '广州亚晋国际货运代理有限公司',
            bizopEtpsSccd: '91440115088041914L',
            declareCode: '443066K516',
            declareName: '广州亚晋国际货运代理有限公司',
            dclEtpsSccd: '91440115088041914L',
            contactEr: '练应彬',
            contactTele: '020-39089295',
            dclMarkcd: '1',
            taxTypecd: 'N',
            emapvStucd: '1',
            declareDate: '2021-06-18',
            putrecApprTime: '2021-06-18',
            chgApprTime: '2021-06-18',
          },
        });
      }}
      columns={[
        {
          title:'申报表编号',
          key:'sasDclNo',
          dataIndex:'sasDclNo',
        },
        {
          title:'申报表预录入编号',
          key:'seqNo',
          dataIndex:'seqNo',
        },
        {
          title:'进出标志',
          key:'ieMark',
          dataIndex:'ieMark',
        },
        {
          title:'业务类型',
          key:'businessType',
          dataIndex:'businessType',
          valueEnum:{
            A:{text:'分送集报'},
            B:{text:'外发加工'},
            C:{ text:'保税展示交易'},
            D:{text: '设备检测'},
            E:{text: '设备维修'},
            F:{text:'模具外发'},
            G:{text:'简单加工'},
            H:{text:'其他业务'},
            Y:{text:'一纳企业进出区'}},
        },
        {
          title: '账册编号',
          key: 'emsNo',
          dataIndex: 'emsNo',
        },
        {
          title:'区外账册编号',
          key:'areaOutOriactNo',
          dataIndex:'areaOutOriactNo',
        },
        {
          title: '经营单位代码',
          key: 'tradeCode',
          dataIndex: 'tradeCode',
        },
        {
          title: '经营单位名称',
          key: 'tradeName',
          dataIndex: 'tradeName',
        },
        {
          title: '经营单位信用代码',
          key: 'bizopEtpsSccd',
          dataIndex: 'bizopEtpsSccd',
        },
        {
          title:'区外企业代码',
          key:'PROVIDER_CODE',
          dataIndex:'PROVIDER_CODE'
        },
        {
          title:'区外企业信用代码',
          key:'areaoutEtpsNm',
          dataIndex:'areaoutEtpsNm'
        },
        {
          title:'区外企业信用代码',
          key:'areaoutEtpsSccd',
          dataIndex:'areaoutEtpsSccd'
        },
        {
          title:'合同有效期',
          key:'validTime',
          dataIndex:'validTime',
          valueType:'date'
        },
        {
          title: '展示交易地址',
          key: 'xhibitionAddress',
          dataIndex: 'exhibitionAddress',
        },
        {
          title:'保证金征收编号',
          key:'dpstLevyBlNo',
          dataIndex:'dpstLevyBlNo'
        },
        {
          title:'料件性质',
          key:'M_P',
          dataIndex:'M_P',
        },
        {
          title: '主管海关',
          key: 'customsCode',
          dataIndex: 'customsCode',
        },

        {
          title: '边角料标志',
          key: 'COL1"',
          dataIndex: 'COL1',
          valueEnum:{
            1:'成品',
            2:'残次品',
            3:'边角料',
            4:'副产品'}
        },
        {
          title: '功能区',
          key: 'FUNCTION_CODE',
          dataIndex: 'FUNCTION_CODE',
        },
        {
          title: '企业内部编码',
          key: 'workNo',
          dataIndex: 'workNo',
        },
        {
          title: '申报标志',
          key: 'declareType',
          dataIndex: 'declareType',
          valueType: 'select',
          valueEnum: { 0: '暂存', 1: '申报' },
        },
        {
          title:'备注',
          key:'remark',
          dataIndex:'remark'
        },

        {
          title: '操作',
          valueType: 'option',
          render: () => [
            <Button type="default" icon={<EditOutlined />}>
              修改
            </Button>,
          ],
        },
      ]}
    />
  );
};

export default DetailHead;
