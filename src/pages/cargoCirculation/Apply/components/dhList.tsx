import { ExportOutlined ,  SearchOutlined,SwapLeftOutlined,PlusOutlined,DeleteOutlined,ImportOutlined} from '@ant-design/icons';

import {Button, Col, Form, Input, Row,Table} from 'antd';
import { useState} from 'react';

const columns = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a key="detail">明细</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '成品备案序号',
    dataIndex: 'gNo',
    align: 'center',
  },
  {
    title:'料件备案序号',
    dataIndex:'seqNo',
    align:'center',
  },
  {
    title: '净耗数量',
    dataIndex: 'quantity',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '耗损率',
    dataIndex: 'rate',
    align: 'center',
    hideInSearch: true,
  },

  {
    title: '修改标志',
    dataIndex: 'modifyMark',
    align: 'center',
    hideInSearch: true,
  },
];

const DhLis: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();
  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="成品备案序号">
              <Form.Item name="acmpFormNo" noStyle>
                <Input name="acmpFormNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="料件备案序号">
              <Form.Item name="acmpFormItemNo" noStyle>
                <Input name="acmpFormItemNo" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        pagination={{
          pageSize: 10,total:100, showQuickJumper: true
        }}
        scroll={{
          x: 2000,
        }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
      />
    </>
  );
};

export default DhLis;



