import { ExportOutlined ,  SearchOutlined,SwapLeftOutlined,PlusOutlined,DeleteOutlined,ImportOutlined} from '@ant-design/icons';

import {Button, Col, Form, Input, Row,Table} from 'antd';
import { useState} from 'react';

const columns = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a key="detail">明细</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '申报表序号',
    dataIndex: 'gNo',
    align: 'center',
  },
  {
    title:'底账商品序号',
    dataIndex:'seqNo',
    align:'center',
  },
  {
    title:'料件成品标志',
    dataIndex:'flag',
    align:'center',
    hideInSearch: true,
  },
  {
    title: '商品料号',
    dataIndex: 'copGNo',
    align: 'center',
  },

  {
    title: '商品编码',
    dataIndex: 'codeTs',
    align: 'center',
  },
  {
    title: '商品名称',
    dataIndex: 'gName',
    align: 'center',
    hideInSearch: true,
  },

  {
    title:'规则型号',
    dataIndex:'size',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'申报计量单位',
    dataIndex:'unit',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'法定第一单位',
    dataIndex:'unit1',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'法定第二单位',
    dataIndex:'unit2',
    align:'center',
    hideInSearch: true,
  },


  {
    title: '数量',
    dataIndex: 'quantity',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '单价',
    dataIndex: 'price',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '总价',
    dataIndex: 'totalPrice',
    align: 'center',
    hideInSearch: true,
  },

  {
    title: '修改标志',
    dataIndex: 'modifyMark',
    align: 'center',
    hideInSearch: true,
  },
];

const DetailLis: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();
  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="申报表序号">
              <Form.Item name="acmpFormNo" noStyle>
                <Input name="acmpFormNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="底账商品序号">
              <Form.Item name="acmpFormItemNo" noStyle>
                <Input name="acmpFormItemNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品料号">
              <Form.Item name="acmpFormGno" noStyle>
                <Input name="acmpFormGno" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品编码">
              <Form.Item name="acmpFormSerialNo" noStyle>
                <Input name="acmpFormSerialNo" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>

      <Table
        columns={columns}
        pagination={{
          pageSize: 10,total:100, showQuickJumper: true
        }}
        scroll={{
          x: 2500,
        }}
        bordered
        style={{ margin: '20px' }}
        size="middle"
        rowSelection={rowSelection}
      />
    </>
  );
};

export default DetailLis;
