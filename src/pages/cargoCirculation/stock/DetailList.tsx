import {
  DeleteOutlined,
  ExportOutlined,
  ImportOutlined,
  PlusOutlined,
  SearchOutlined,
  SwapLeftOutlined,
} from '@ant-design/icons';

import { Button, Col, Form, Input, Row, Table } from 'antd';
import { useState } from 'react';

const columns = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '30',
    align: 'center',
  },
  {
    title: '操作',
    width: 120,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">编辑</a>,
      <a href="" key="detail" target="_blank" >明细</a> ,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },

  {
    title: '商品序号',
    dataIndex: 'copGNo',
    align: 'center',
  },
  {
    title: '申报表序号',
    dataIndex: 'gNo',
    align: 'center',
  },
  {
    title: '备案序号',
    dataIndex: 'emsNo',
    align: 'center',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title:'商品料号',
    dataIndex:'liaohao',
    hideInSearch: true,
  },
  {
    title: '商品编码',
    dataIndex: 'codeTs',
    align: 'center',
  },
  {
    title: '商品名称',
    dataIndex: 'gName',
    align: 'center',
  },
  {
    title:'规格型号',
    dataIndex:'size',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'申报单价',
    dataIndex:'unitPrice',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'申报总价',
    dataIndex:'totalPrice',
    align:'center',
    hideInSearch: true,
  },
  {
    title:'申报币值',
    dataIndex:'curr',
    align:'center',
    hideInSearch: true,
  },
  {
    title: '申报数量',
    dataIndex: 'quantity',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '法定数量',
    dataIndex: 'quantity1',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '第二法定数量',
    dataIndex: 'quantity2',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '申报单位',
    dataIndex: 'unit',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '法定单位',
    dataIndex: 'unit1',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '法定第二单位',
    dataIndex: 'unit2',
    align: 'center',
    hideInSearch: true,
  },
  {
    title:'毛重',
    dataIndex:'grossWeight',
    align:'center',
    hideInSearch:true,
  },
  {
    title:'净重',
    dataIndex:'netWeight',
    align:'center',
    hideInSearch:true,
  },
  {
    title:'单耗版本号',
    dataIndex:'version',
    align:'center',
    hideInSearch:true,
  },
  {
    title:'是否参与合并',
    dataIndex:'combine',
    align:'center',
    hideInSearch:true,
  },
  {
    title: '修改标志',
    dataIndex: 'modifyMark',
    align: 'center',
    hideInSearch: true,
  },

];

const DetailLis: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [form] = Form.useForm();

  const onSelectChange = (selectKeys: any[]) => {
    setSelectedRowKeys(selectKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <>
      <Form name="AttachSearch" form={form} style={{ margin: '0 20px' }}>
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="申报表序号">
              <Form.Item name="acmpFormDclNo" noStyle>
                <Input name="acmpFormNo" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="备案序号">
              <Form.Item name="acmpFormNo" noStyle>
                <Input name="acmpFormFileNm" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品料号">
              <Form.Item name="acmpFormGno" noStyle>
                <Input name="acmpFormTypecd" />
              </Form.Item>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="商品编码">
              <Form.Item name="acmpFormSerialNo" noStyle>
                <Input name="acmpFormTypecd" />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit" icon={<SearchOutlined />} ghost>
              查询
            </Button>
            <Button
              style={{ marginLeft: '8px' }}
              icon={<SwapLeftOutlined />}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<PlusOutlined />}>
              录入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<DeleteOutlined />} danger>
              删除
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ImportOutlined />}>
              导入
            </Button>
            <Button style={{ marginLeft: '8px' }} key="button" icon={<ExportOutlined />}>
              导出
            </Button>
          </Col>
        </Row>
      </Form>
      <Table
        columns={columns}
        style={{ margin: '20px' }}
        scroll={{
          x: 2600,
        }}
        bordered
        pagination={{ pageSize: 20, total: 200, showQuickJumper: true }}
      />
    </>
  );
};

export default DetailLis;
