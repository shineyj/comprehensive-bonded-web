import { EditOutlined } from '@ant-design/icons';
import ProDescriptions from '@ant-design/pro-descriptions';
import { Button } from 'antd';

const bwlTypecd = {
  A: '保税物流中心A',
  B: '保税物流中心B',
  D: '公共保税仓库',
  E: '液体保税仓库',
  F: '寄售维修保税仓库',
  G: '暂为空',
  H: '特殊商品保税仓库',
  I: '备料保税仓库',
  P: '出口配送监管仓',
  J: '为国内结转监管仓',
  K: '保税区',
  L: '出口加工区',
  M: '保税物流园区',
  N: '保税港区',
  Z: '综合保税区',
  Q: '跨境工业园区',
  S: '特殊区域设备账册',
};

const DetailHead: React.FC = () => {
  return (
    <ProDescriptions
      title="详细信息"
      bordered
      request={async () => {
        return Promise.resolve({
          success: true,
          data: {
            preNo: 'W51656180010',
            workNo: 'W51656180010',
            emsNo: 'T5165W000075',
            declareType: '1',
            houseTypecd: 'A',
            bwlTypecd: 'A',
            appendTypecd: '1',
            usageTypecd: '1',
            dclEtpsTypecd: '1',
            customsCode: '5165|黄埔海关',
            endDate: '2022-12-31',
            modifyTimes: '0',
            houseNo: '443066K516',
            houseName: '广州亚晋国际货运代理有限公司',
            storeArea: '48016.82',
            storeVol: '111',
            houseAddress:
              '广州市南沙区龙穴岛南沙综合保税区物流区港荣三街天运仓二层5-1至5-12、6-1至6-12、7-1至7-12',
            tradeCode: '443066K516',
            tradeName: '广州亚晋国际货运代理有限公司',
            bizopEtpsSccd: '91440115088041914L',
            declareCode: '443066K516',
            declareName: '广州亚晋国际货运代理有限公司',
            dclEtpsSccd: '91440115088041914L',
            contactEr: '练应彬',
            contactTele: '020-39089295',
            dclMarkcd: '1',
            taxTypecd: 'N',
            emapvStucd: '1',
            declareDate: '2021-06-18',
            putrecApprTime: '2021-06-18',
            chgApprTime: '2021-06-18',
          },
        });
      }}
      columns={[
        {
          title:'出入库单编号',
          key:'sasStockNo',
          dataIndex:'sasStockNo',
        },
        {
          title:'出入库单预录入编号',
          key:'seqNo',
          dataIndex:'seqNo',
        },
        {
          title:'区域代码',
          key:'areaCode',
          dataIndex:'areaCode',
        },
        {
          title:'申报表编号',
          key:'sasDclNo',
          dataIndex:'sasDclNo',
        },
        {
          title:'业务类型',
          key:'businessType',
          dataIndex:'businessType',
          valueEnum:{
            A:{text:'分送集报'},
            B:{text:'外发加工'},
            C:{ text:'保税展示交易'},
            D:{text: '设备检测'},
            E:{text: '设备维修'},
            F:{text:'模具外发'},
            G:{text:'简单加工'},
            H:{text:'其他业务'},
            Y:{text:'一纳企业进出区'}},
        },
        {
          title:'进出标志',
          key:'ieMark',
          dataIndex:'ieMark',
          valueType:'select',
          valueEnum:{
            I:'进口',
            E:'出口'
          }
        },
        {
          title:'件数',
          key:'packNo',
          dataIndex:'packNo'
        },
        {
          title:'毛重',
          key:'grossWeight',
          dataIndex:'grossWeight',
        },
        {
          title:'净重',
          key:'netWeight',
          dataIndex:'netWeight'
        },
        {
          title: '账册编号',
          key: 'emsNo',
          dataIndex: 'emsNo',
        },
        {
          title:'关联申请单号',
          key:'correspondNo',
          dataIndex:'correspondNo',
        },
        {
          title: '包装种类',
          key: 'packType',
          dataIndex: 'packType',
          valueType:'select',
          valueEnum:{
            1:'木箱',
            2:'纸箱',
            3:'桶装',
            4:'散装',
            5:'托盘',
            6:'包',
            7:'其他'
          }
        },
        {
          title: '材料性质',
          key: 'mp',
          dataIndex: 'mp',
          valueType:'select',
          valueEnum:{
            E:'成品',
            I:'料件',
          }
        },
        {
          title: '主管海关',
          key: 'customsCode',
          dataIndex: 'customsCode',
        },
        {
          title: '经营单位代码',
          key: 'tradeCode',
          dataIndex: 'tradeCode',
        },
        {
          title: '经营单位名称',
          key: 'tradeName',
          dataIndex: 'tradeName',
        },
        {
          title:'关联出入库单编号',
          key:'rltSasStockNo',
          dataIndex:'rltSasStockNo'
        },
        {
          title:'退货单标志',
          key:'eportReplaceMark',
          dataIndex:'eportReplaceMark',
          valueType:'select',
          valueEnum:{
            0:'退货单',
            1:'非退货单'
          }
        },
        {
          title: '企业内部编码',
          key: 'workNo',
          dataIndex: 'workNo',
        },
        {
          title: '申报标志',
          key: 'declareType',
          dataIndex: 'declareType',
          valueType: 'select',
          valueEnum: { 0: '暂存', 1: '申报' },
        },
        {
          title:'备注',
          key:'remark',
          dataIndex:'remark'
        },
        {
          title: '功能区',
          key: 'FUNCTION_CODE',
          dataIndex: 'FUNCTION_CODE',
        },
        {
          title: '操作',
          valueType: 'option',
          render: () => [
            <Button type="default" icon={<EditOutlined />}>
              修改
            </Button>,
          ],
        },
      ]}
    />
  );
};

export default DetailHead;
