import type { Request, Response } from 'express';
import { parse } from 'url';
import type { TableListItem, TableListParams } from './data.d';

// mock tableListDataSource
const getList = () => {
  const tableListDataSource: TableListItem[] = [];
  for (let i = 0; i < 21; i++) {
    tableListDataSource.push({
      workNo: 'W01006200002',
      sasDclNo: '3234234',
      preNo: 'W01006200002',
      emsNo: 'L0100B19A004',
      dclTypeCd: '1',
      stockTypeCd:'I',
      businessType: 'A',
      sasStockNo:'595416568',
      relSasStockNo:'4562452324',
      statusCode:'1',
      approveStatus: '5',
      createDate: '	2020/08/29',
      declareDate: '2020/08/13 09:59:51',
      approveDate: '2020/08/13 09:59:51',
      areaInEtpsNo:'342355',
      areaInEtpsNm:'区内企业名称',
    });
  }
  return tableListDataSource;
};

const tableListDataSource: TableListItem[] = getList();

function getStock(req: Request, res: Response, u: string) {
  debugger;
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query as unknown as TableListParams;

  let dataSource = [...tableListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  if (params.sorter) {
    const sorter = JSON.parse(params.sorter as any);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as Record<string, string[]>;
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }


  let finalPageSize = 10;
  if (params.pageSize) {
    finalPageSize = parseInt(`${params.pageSize}`, 10);
  }

  const result = {
    data: dataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize: finalPageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

export default {
  'GET /api/stock': getStock,
};
