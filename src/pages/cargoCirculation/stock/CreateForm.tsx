import {
  ModalForm,
  ProFormDatePicker,
  ProFormDigit,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-form';
import { Col, message, Row } from 'antd';
import React from 'react';
import type { TableListItem } from '../data';

type CreateFormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: TableListItem) => Promise<void>;
  modalVisible: boolean;
  values?: Partial<TableListItem>;
};

const businessTypecd={
  A:'分送集报',
  B:'外发加工',
  C: '保税展示交易',
  D: '设备检测',
  E: '设备维修',
  F:'模具外发',
  G:'简单加工',
  H:'其他业务',
  Y:'一纳企业进出区'
};

const CreateForm: React.FC<CreateFormProps> = (props) => {
  return (
    <ModalForm
      title="录入出入库单"
      width="1100px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ span: 9 }}
      wrapperCol={{ span: 15 }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      onFinish={props.onSubmit}
      submitter={{
        searchConfig: {
          submitText: '保存',
          resetText: '取消',
        },
      }}
    >
      <Row>
        <Col span={8}>
          <ProFormText name="sasStockNo" label="出入库单编号" placeholder="" disabled/>
        </Col>
        <Col span={8}>
          <ProFormText name="sasStockPreentNo" label="出入库单预录入编号" placeholder="" disabled />
        </Col>
        <Col span={8}>
          <ProFormText name="areaCode" label="区域代码" placeholder="" rules={[{ required: true, message: '这是必填项' }]}/>
        </Col>
        <Col span={8}>
          <ProFormSelect name="sasDclNo" label="申报表编号"  rules={[{ required: true, message: '这是必填项' }]}/>
        </Col>
        <Col span={8}>
          <ProFormSelect name="businessTypecd" label="业务类型" valueEnum={businessTypecd} rules={[{ required: true, message: '这是必填项' }]} />
        </Col>
        <Col span={8}>
          <ProFormSelect name="ieFlag" label="进出标志" valueEnum={{ E: '出口', I: '进口' }} rules={[{ required: true, message: '这是必填项' }]}/>
        </Col>
        <Col span={8}>
          <ProFormText  name="packNo"  label="件数"  />
        </Col>
        <Col span={8}>
          <ProFormText  name="grossWeight"    label="毛重" disabled     />
        </Col>
        <Col span={8}>
          <ProFormText  name="netWeight"    label="净重" disabled     />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="packType"
            label="包装种类"
            valueEnum={{1:'木箱',
              2:'纸箱',
              3:'桶装',
              4:'散装',
              5:'托盘',
              6:'包',
              7:'其他'}}
          />
        </Col>
        <Col span={8}>
          <ProFormText  name="correspondNo" label="关联申请单号" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="M_P"
            label="料件性质"
            valueEnum={{I:'料件/半成品',E:'成品/残次品',R:'边角料、副产品'}}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtCustomsCode"
            label="主管海关"
            rules={[{ required: true, message: '这是必填项' }]}
          />
        </Col>
        <Col span={8}>
          <ProFormText name="txtTradeCode" label="经营单位编码"  rules={[{required:true,message:'这是必填项'}]} />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtTradeName"
            label="经营单位名称"
            rules={[{ required: true, message: '这是必填项' }]}
            disabled
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="rltSasStockNo"
            label="关联出入库单编号"

          />
        </Col>
        <Col span={8}>
          <ProFormText name="txtAreaoutEtpsNo" label="区外企业代码" />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="eportReplaceMark"
            label="退货单标志"
            valueEnum={{0:'退货单',1:'非退货单'}}
          />
        </Col>
        <Col span={8}>
          <ProFormText
            name="txtWORK_NO"
            label="企业内部编码"
          />
        </Col>
        <Col span={8}>
          <ProFormSelect
            name="txtDELCAREFLAG"
            label="申报标志"
            valueEnum={{0:'暂存',1:'申报'}}
          />
        </Col>
        <Col span={16}>
          <ProFormText name="note" label="备注" labelCol={{ span: 4 }} wrapperCol={{ span: 20 }} />
        </Col>

        <Col span={8}>
          <ProFormSelect
            name="txtFUNCTION_CODE"
            label="功能区"
            valueEnum={{516501:'物流区',
                        516502:'加工区',
                        516503:'港口二期'}}
          />
        </Col>


      </Row>
    </ModalForm>
  );
};

export default CreateForm;
