import {
  PlusOutlined,
  DeleteOutlined,
  SendOutlined,
  ImportOutlined,
  PrinterOutlined,
  ExportOutlined,
} from '@ant-design/icons';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { Button, message } from 'antd';
import { history } from 'umi';
import { useRef, useState } from 'react';
import type { TableListItem } from './data';
import { stock } from './service';

import CreateForm from './CreateForm';
import Detail from './Detail';

const handleAdd = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    //await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};


const columns: ProColumns<TableListItem>[] = [
  {
    dataIndex: 'index',
    valueType: 'index',
    width: '3',
    align: 'center',
  },
  {
    title: '操作',
    width: 160,
    align: 'center',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a key="editable">申报</a>,
      <a key="copy">复制</a>,
      <a href="" target="_blank" rel="noopener noreferrer" key="view">
        删除
      </a>,
    ],
  },
  {
    title: '预录入号',
    dataIndex: 'preNo',
    width: 180,
    align: 'center',
    copyable: true,
    order: 13,
  },
  {
    title: '出入库单编号',
    dataIndex: 'sasStockNo',
    width: 180,
    align: 'center',
    copyable: true,
    order: 12,
  },
  {
    title: '企业内部编号',
    dataIndex: 'workNo',
    width: 180,
    align: 'center',
    copyable: true,
    order: 11,
  },
  {
    title:'申报表编号',
    dataIndex:'sasDclNo',
    width:180,
    align:'center',
    copyable:true,
    order:10
  },
  {
    title:'申报类型',
    dataIndex:'dclTypeCd',
    width:120,
    align:'center',
    valueType:'select',
    valueEnum: {
      1: { text: '备案',value:'1' },
      2: { text: '变更' ,value:'2'},
      3: { text: '结案',value:'3' },
    },
    hideInSearch:true,
  },


  {
    title: '数据状态',
    dataIndex: 'approveStatus',
    width: 120,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      5: { text: '预录入',value:'' },
      6: { text: '已申报' ,value:''},
      1: { text: '通过',value:'1' },
      2: { text: '转岗',value:'2' },
      3: { text: '退单' ,value:'3' },
    },
    order: 9,
  },
  {
    title:'状态代码',
    dataIndex:'statusCode',
    valueType:'select',
    valueEnum:{0:'未审批',1:'已审批',2:'已作废'},
    hideInSearch:false,
    order:8,
  },
  {
    title:'出入库单类型',
    dataIndex:'stockTypeCd',
    valueType:'select',
    valueEnum:{I:'进区',E:'出区'},
    order:6,
  },
  {
    title: '业务类型',
    dataIndex: 'businessType',
    width: 120,
    align: 'center',
    valueType: 'select',
    valueEnum: {
      A:{text:'分送集报'},
      B:{text:'外发加工'},
      C:{ text:'保税展示交易'},
      D:{text: '设备检测'},
      E:{text: '设备维修'},
      F:{text:'模具外发'},
      G:{text:'简单加工'},
      H:{text:'其他业务'},
      Y:{text:'一纳企业进出区'}
    },
    hideInSearch: false,
    order:5,
  },
  {
    title: '账册编号',
    dataIndex: 'emsNo',
    width: 140,
    align: 'center',
    copyable: true,
    order: 7,
    hideInSearch:false,
  },
  {
    title:'关联出库单编号',
    dataIndex:'relSasStockNo',
    width:120,
    align:'center',
    hideInSearch:false,
    order:4,
  },
  {
    title: '区内企业编号',
    dataIndex: 'areaInEtpsNo',
    width: 150,
    align: 'center',
    copyable: true,
    hideInSearch: true,
    hideInTable:false,
  },
  {
    title: '区内企业名称',
    dataIndex: 'areaInEtpsNm',
    width: 200,
    align: 'center',
    copyable: true,
    hideInSearch: true,
    hideInTable:false,
  },
  {
    title: '创建起止时间',
    dataIndex: 'createDate',
    width: 130,
    align: 'center',
    valueType: 'dateRange',
    hideInSearch: false,
    hideInTable:true,
    order:3
  },
  {
    title: '创建时间',
    dataIndex: 'createDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    sorter: true,
    hideInSearch: true,
    hideInTable:false,
  },
  {
    title: '申报起止时间',
    dataIndex: 'declareDate',
    width: 130,
    align: 'center',
    valueType: 'dateRange',
    hideInSearch: false,
    hideInTable:true,
    order:2
  },
  {
    title: '申报时间',
    dataIndex: 'declareDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    hideInSearch: true,
    hideInTable:false,
    sorter:true,
  },
  {
    title: '审批起止时间',
    dataIndex: 'approveDate',
    width: 130,
    align: 'center',
    valueType: 'dateRange',
    hideInSearch:  false,
    hideInTable:true,
    order:1
  },
  {
    title: '备案审批时间',
    dataIndex: 'approveDate',
    width: 260,
    align: 'center',
    valueType: 'dateTime',
    hideInSearch: true,
    hideInTable:false,
    sorter:true,
  },


];

export default () => {
  const applyType = history.location.pathname.substring(
    history.location.pathname.lastIndexOf('/') + 1,
  );
  console.log(applyType);
  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  /**
   * 新建窗口的弹窗
   */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * 详情是否显示
   */
  const [detailVisible, handleDetailVisible] = useState<boolean>(false);

  const [doubleClickRowState, setDoubleClickRowState] = useState<TableListItem>();

  const listTable = (
    <>
      <ProTable<TableListItem>
        columns={columns}
        actionRef={actionRef}
        request={stock}
        pagination={{
          pageSize: 10,
        }}
        bordered
        scroll={{
          x:3000,
        }}
        rowSelection={{
          onChange: (_, selectedRow) => {
            setSelectedRows(selectedRow);
          },
        }}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={() => [
          'declare' == applyType && (
            <Button
              key="button"
              icon={<PlusOutlined />}
              onClick={() => {
                handleModalVisible(true);
              }}
            >
              录入
            </Button>
          ),
          'declare' == applyType && (
            <Button key="button" icon={<ImportOutlined />}>
              导入
            </Button>
          ),
          'declare' == applyType && (
            <Button
              key="button"
              icon={<DeleteOutlined />}
              danger
              disabled={selectedRowsState.length == 0}
            >
              删除
            </Button>
          ),
          'search' != applyType && (
            <Button
              key="button"
              icon={<SendOutlined />}
              type="primary"
              ghost
              disabled={selectedRowsState.length == 0}
            >
              申报
            </Button>
          ),
          <Button key="button" icon={<ExportOutlined />}>
            导出
          </Button>,
          <Button key="button" icon={<PrinterOutlined />}>
            打印
          </Button>,
        ]}
        onRow={(record) => {
          return {
            onClick: (event) => {}, // 点击行
            onDoubleClick: (event) => {
              setDoubleClickRowState(record);
              handleDetailVisible(true);
            },
            onContextMenu: (event) => {},
            onMouseEnter: (event) => {}, // 鼠标移入行
            onMouseLeave: (event) => {},
          };
        }}
      />

      <CreateForm
        modalVisible={createModalVisible}
        onSubmit={async (value) => {
          const success = await handleAdd(value);
          if (success) {
            handleModalVisible(false);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleModalVisible(false);
        }}
      />
    </>
  );

  if (!detailVisible) {
    return listTable;
  } else {
    return (
      <Detail
        record={doubleClickRowState}
        onBack={() => {
          handleDetailVisible(false);
        }}
      />
    );
  }
};
