import { ProColumns } from '@ant-design/pro-table';
import { getOutCompanyList } from './outCompany/service';
import { ems } from '../accoutBook/ems/service';
import { getTradeList } from './trade/service';
import { getTransfList } from './transf/service';
import { getCustomsList } from './customs/service';
import { getCountryList } from './country/service';
import { Apply } from '../cargoCirculation/Apply/service';

export function UseFieldConcatFormatter(fields: string[]) {
  const formatterFn = (recode: Record<string, any>): string => {
    const formatterValues: string[] = [];
    fields.forEach((field) => formatterValues.push(recode[field]));
    return formatterValues.join('|');
  };

  return formatterFn;
}

export function getOutCompanyPickerSrcConfig() {
  const columns: ProColumns<Record<string, any>, 'text'>[] = [
    { title: '企业编码', dataIndex: 'provider_code' },
    { title: '企业名称', dataIndex: 'provider_name' },
    { title: '信用编号', dataIndex: 'uscCode', search: false },
  ];

  return {
    columns: columns,
    request: getOutCompanyList,
    valueName: 'provider_code',
  };
}

export function getEmsPickerSrcConfig() {
  return {
    columns: [
      { title: '账册编号', dataIndex: 'emsNo' },
      { title: '经营单位代码', dataIndex: 'tradeCode' },
      { title: '经营单位名称', dataIndex: 'tradeName', search: false },
      { title: '经营单位社会信用代码', dataIndex: 'bizopEtpsSccd', search: false },
      { title: '收发货单位代码', dataIndex: 'ownerCode', search: false },
      { title: '收发货单位名称', dataIndex: 'ownerName', search: false },
      { title: '收发货单位社会信用代码', dataIndex: 'rvsngdEtpsSccd', search: false },
      { title: '暂停恢复标记', dataIndex: 'chgMarkcd', search: false },
      { title: '进出口暂停标记', dataIndex: 'ieMarkcd', search: false },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: ems,
    valueName: 'emsNo',
  };
}

export function getTradePickerSrcConfig() {
  return {
    columns: [
      { title: '代码', dataIndex: 'code' },
      { title: '名称', dataIndex: 'name' },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: getTradeList,
    valueName: 'code',
    formatter: UseFieldConcatFormatter(['code', 'name']),
  };
}

export function getTransfPickerSrcConfig() {
  return {
    columns: [
      { title: '代码', dataIndex: 'code' },
      { title: '名称', dataIndex: 'name' },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: getTransfList,
    valueName: 'code',
    formatter: UseFieldConcatFormatter(['code', 'name']),
  };
}

export function getCustomsPickerSrcConfig() {
  return {
    columns: [
      { title: '代码', dataIndex: 'code' },
      { title: '名称', dataIndex: 'name' },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: getCustomsList,
    valueName: 'code',
    formatter: UseFieldConcatFormatter(['code', 'name']),
  };
}

export function getCountryPickerSrcConfig() {
  return {
    columns: [
      { title: '代码', dataIndex: 'code' },
      { title: '名称', dataIndex: 'name' },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: getCountryList,
    valueName: 'code',
    formatter: UseFieldConcatFormatter(['code', 'name']),
  };
}

export function getApplyPickerSrcConfig() {
  return {
    columns: [
      { title: '申报表预录入编号', dataIndex: 'preNo' },
      { title: '申报表编号', dataIndex: 'sasDclNo' },
    ] as ProColumns<Record<string, any>, 'text'>[],
    request: Apply,
    valueName: 'preNo',
  };
}
