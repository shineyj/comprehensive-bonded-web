export type Country = { code: string; name: string };

export type CountryQueryParam = Partial<Country> & API.PageParams;
export type CountryListResult = API.BasePageResult<Country>;
