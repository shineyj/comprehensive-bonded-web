import { Request, Response } from 'express';

const allCountryList = [
  {
    code: '259',
    name: '马约特',
  },
  {
    code: '299',
    name: '非洲其他国家(地区)',
  },
  {
    code: '301',
    name: '比利时',
  },
  {
    code: '302',
    name: '丹麦',
  },
  {
    code: '303',
    name: '英国',
  },
  {
    code: '304',
    name: '德国',
  },
];

function getCountryList(req: Request, res: Response) {
  return res.json({
    data: allCountryList,
    total: 50,
    success: true,
  });
}

export default {
  'GET /api/base-country': getCountryList,
};
