import { request } from 'umi';
import { CountryQueryParam, CountryListResult } from './data.d';

export function getCountryList(params: CountryQueryParam) {
  return request<API.ApiPageResult<CountryListResult>>('/api/base-country', {
    method: 'GET',
    params,
  });
}
