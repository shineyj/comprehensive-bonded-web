import { request } from 'umi';
import { TransfQueryParam, TransfListResult } from './data.d';

export function getTransfList(params: TransfQueryParam) {
  return request<API.ApiPageResult<TransfListResult>>('/api/base-transf', {
    method: 'GET',
    params,
  });
}
