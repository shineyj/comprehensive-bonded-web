import { Request, Response } from 'express';

const allTransfList = [
  {
    code: '1',
    name: '监管仓库',
  },
  {
    code: '2',
    name: '水路运输',
  },
  {
    code: '3',
    name: '铁路运输',
  },
  {
    code: '4',
    name: '公路运输',
  },
  {
    code: '5',
    name: '航空运输',
  },
  {
    code: '6',
    name: '邮件运输',
  },
  {
    code: '7',
    name: '保税区',
  },
  {
    code: '8',
    name: '保税仓库',
  },
  {
    code: '9',
    name: '其它运输',
  },
  {
    code: 'A',
    name: '全部运输方式',
  },
  {
    code: 'H',
    name: '边境特殊海关作业区',
  },
  {
    code: 'W',
    name: '物流中心',
  },
  {
    code: 'X',
    name: '物流园区',
  },
  {
    code: 'Y',
    name: '保税港区',
  },
  {
    code: 'Z',
    name: '出口加工区',
  },
  {
    code: 'L',
    name: '旅客携带',
  },
  {
    code: 'G',
    name: '固定设施运输',
  },
];

function getTransfList(req: Request, res: Response) {
  return res.json({
    data: allTransfList,
    total: allTransfList.length,
    success: true,
  });
}

export default {
  'GET /api/base-transf': getTransfList,
};
