export type Transf = {
  code: string;
  name: string;
};

export type TransfQueryParam = Partial<Transf> & API.PageParams;
export type TransfListResult = API.BasePageResult<Transf>;
