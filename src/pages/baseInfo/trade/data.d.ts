export type Trade = {
  code: string;
  name: string;
};

export type TradeQueryParam = Partial<Trade> & API.PageParams;
export type TradeListResult = API.BasePageResult<Trade>;
