import { Request, Response } from 'express';

const allTradeList = [
  {
    code: '5335',
    name: '境外设备进区',
  },
  {
    code: '1039',
    name: '市场采购',
  },
  {
    code: '1210',
    name: '保税电商',
  },
  {
    code: '1371',
    name: '保税维修',
  },
  {
    code: '9610',
    name: '电子商务',
  },
  {
    code: '5361',
    name: '区内设备退运',
  },
  {
    code: '6033',
    name: '物流中心进出境货物',
  },
];

function getTradeList(req: Request, res: Response) {
  return res.json({
    data: allTradeList,
    total: 7,
    success: true,
  });
}

export default {
  'GET /api/base-trade': getTradeList,
};
