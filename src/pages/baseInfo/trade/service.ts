import { request } from 'umi';
import { TradeQueryParam, TradeListResult } from './data.d';

export function getTradeList(params: TradeQueryParam) {
  return request<API.ApiPageResult<TradeListResult>>('/api/base-trade', {
    method: 'GET',
    params,
  });
}
