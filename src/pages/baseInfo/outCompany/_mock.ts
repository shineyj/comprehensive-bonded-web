import { Request, Response } from 'express';

const allOutCompanyList = [
  {
    trade_code: '1100696075',
    trade_name: '广州市景鸿物流有限公司',
    custom: '5165',
    provider_code: '4423980033',
    provider_name: '广东景鸿报关报检有限公司',
    rel_type: '0',
    uscCode: '914400007665732595',
  },
  {
    trade_code: '1100696075',
    trade_name: '广东卓志跨境电商供应链服务有限公司',
    custom: '5165',
    provider_code: '443096525K',
    provider_name: '广州安利商贸进出口有限公司',
    rel_type: '0',
    uscCode: '91440101MA5CBA573X',
  },
  {
    trade_code: '1100696075',
    trade_name: '广州市景鸿物流有限公司',
    custom: '5165',
    provider_code: '4210946015',
    provider_name: '松林光电科技(湖北)有限公司',
    rel_type: '0',
    uscCode: '91420900670363978N',
  },
  {
    trade_code: '1100696075',
    trade_name: '广州市景鸿物流有限公司',
    custom: '5165',
    provider_code: '3501231487',
    provider_name: '麦克赛尔数字映像(中国)有限公司',
    rel_type: '0',
    uscCode: '914400007665732595',
  },
];

function getOutCompanyList(req: Request, res: Response) {
  return res.json({
    data: allOutCompanyList,
    total: 50,
    success: true,
  });
}

export default {
  'GET /api/base-out-company': getOutCompanyList,
};
