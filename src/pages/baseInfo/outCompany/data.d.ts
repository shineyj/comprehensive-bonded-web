export type OutCompany = {
  auto_id: string;
  trade_code: string;
  trade_name: string;
  custom: string;
  provider_code: string;
  provider_name: string;
  rel_type: string;
  uscCode: string;
};

export type OutCompanyQueryParam = Partial<OutCompany> & API.PageParams;
export type OutCompanyListResult = API.BasePageResult<OutCompany>;
