import { request } from 'umi';
import { OutCompanyQueryParam, OutCompanyListResult } from './data.d';

export function getOutCompanyList(params: OutCompanyQueryParam) {
  return request<API.ApiPageResult<OutCompanyListResult>>('/api/base-out-company', {
    method: 'GET',
    params,
  });
}
