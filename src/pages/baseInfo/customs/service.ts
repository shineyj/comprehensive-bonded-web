import { request } from 'umi';
import { CustomsQueryParam, CustomsListResult } from './data.d';

export function getCustomsList(params: CustomsQueryParam) {
  return request<API.ApiPageResult<CustomsListResult>>('/api/base-customs', {
    method: 'GET',
    params,
  });
}
