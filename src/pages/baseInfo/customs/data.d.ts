export type Customs = { code: string; name: string };

export type CustomsQueryParam = Partial<Customs> & API.PageParams;
export type CustomsListResult = API.BasePageResult<Customs>;
