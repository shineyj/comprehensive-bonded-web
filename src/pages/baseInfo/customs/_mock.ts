import { Request, Response } from 'express';

const allCustomsList = [
  {
    code: '0503',
    name: '大同海关',
  },
  {
    code: '6708',
    name: '湛江高州',
  },
  {
    code: '6710',
    name: '东海岛组',
  },
  {
    code: '6711',
    name: '霞山海关',
  },
  {
    code: '6713',
    name: '湛江机场',
  },
  {
    code: '6714',
    name: '湛江博贺',
  },
];

function getCustomsList(req: Request, res: Response) {
  return res.json({
    data: allCustomsList,
    total: 50,
    success: true,
  });
}

export default {
  'GET /api/base-customs': getCustomsList,
};
