import { Col, Row } from 'antd';
import Form from 'antd/lib/form';
import type { formConfig } from './data';

const Display: React.FC<formConfig> = (props) => {
  return (
    <>
      <Form {...props.formProps}>
        <Row gutter={props.formitemGutter}>
          <Col span={props.formItemCol} />
        </Row>
      </Form>
    </>
  );
};

export default Display;
