import type { FormProps } from 'antd/lib/form/index';
import type { FormItemProps } from 'antd/lib/form/FormItem';

export type formConfig = {
  formProps: FormProps;
  formItemCol: number;
  formitemGutter: number;
  schemas: {
    FormItemProps: FormItemProps;
    component: string;
    componentProps: any;
  }[];
};
