import React, { useState, useEffect, useRef } from 'react';
import { RouteContext } from '@ant-design/pro-layout';
import type { RouteContextType } from '@ant-design/pro-layout';
import { history } from 'umi';
import Tabs from './Tabs';
import styles from './index.less';

export type TabsItemType = {
  title?: string;
  path?: string;
  active: boolean;
  query?: any;
  children: any;
  refresh: number;
};

interface IProps {
  home: string;
}

/**
 * @component PageTab 标签页组件
 */
const PageTab: React.FC<IProps> = ({ children, home }) => {
  const [tabList, setTabList] = useState<TabsItemType[]>([]);

  const routeContextRef = useRef<RouteContextType>();

  useEffect(() => {
    if (routeContextRef?.current) {
      handleOnChange(routeContextRef.current);
    }
  }, [routeContextRef?.current]);

  // 初始化 visitedViews，设置project为首页
  const initTabs = (routeContext: RouteContextType) => {
    if (tabList.length === 0 && routeContext.menuData) {
      const firstTab = routeContext.menuData.filter((el) => el.path === home)[0];
      const title = firstTab.name;
      const path = firstTab.path;
      history.push({ pathname: firstTab.path, query: firstTab.query });
      setTabList([
        {
          title,
          path,
          children,
          refresh: 0,
          active: true,
        },
      ]);
    }
  };

  // 监听路由改变
  const handleOnChange = (routeContext: RouteContextType) => {
    const { currentMenu } = routeContext;

    // tabs初始化
    if (tabList.length === 0) {
      return initTabs(routeContext);
    }

    // 判断是否已打开过该页面
    let hasOpen = false;
    const tabsCopy: TabsItemType[] = tabList.map((item) => {
      if (currentMenu?.path === item.path) {
        hasOpen = true;
        // 刷新浏览器时，重新覆盖当前 path 的 children
        return { ...item, active: true, children };
      } else {
        return { ...item, active: false };
      }
    });

    // 没有该tag时追加一个,并打开这个tag页面
    if (!hasOpen) {
      const title = routeContext.title || '';
      const path = currentMenu?.path;
      tabsCopy.push({
        title,
        path,
        children,
        refresh: 0,
        active: true,
      });
    }
    return setTabList(tabsCopy);
  };

  // 关闭标签
  const handleCloseTab = (tag: TabsItemType) => {
    const tabsCopy: TabsItemType[] = tabList.map((el, i) => ({ ...el }));

    // 判断关闭标签是否处于打开状态
    tabList.forEach((el, i) => {
      if (el.path === tag.path && tag.active) {
        const next = tabList[i - 1];
        next.active = true;
        history.push({ pathname: next?.path, query: next?.query });
      }
    });

    setTabList(tabsCopy.filter((el) => el.path !== tag?.path));
  };

  // 关闭所有标签
  const handleCloseAll = () => {
    const tabsCopy: TabsItemType[] = tabList.filter((el) => el.path === home);
    history.push(home);
    setTabList(tabsCopy);
  };

  // 关闭其他标签
  const handleCloseOther = (tag: TabsItemType) => {
    const tabsCopy: TabsItemType[] = tabList.filter(
      (el) => el.path === home || el.path === tag.path,
    );
    history.push({ pathname: tag?.path, query: tag?.query });
    setTabList(tabsCopy);
  };

  // 刷新选择的标签
  const handleRefreshTag = (tag: TabsItemType) => {
    const tabsCopy: TabsItemType[] = tabList.map((item) => {
      if (item.path === tag.path) {
        history.push({ pathname: tag?.path, query: tag?.query });
        return { ...item, refresh: item.refresh + 1, active: true };
      }
      return { ...item, active: false };
    });
    setTabList(tabsCopy);
  };

  return (
    <>
      <RouteContext.Consumer>
        {(value: RouteContextType) => {
          // console.log(value);
          routeContextRef.current = value;
          return null;
        }}
      </RouteContext.Consumer>
      <div className={styles.tabs_view}>
        <div className={styles.tabs_container}>
          <Tabs
            tabList={tabList}
            closeTab={handleCloseTab}
            closeAllTab={handleCloseAll}
            closeOtherTab={handleCloseOther}
            refreshTab={handleRefreshTag}
          />
        </div>
      </div>

      {tabList.map((item) => {
        return (
          <div key={item.path} style={{ display: item.active ? 'block' : 'none' }}>
            <div key={item.refresh}>{item.children}</div>
          </div>
        );
      })}
    </>
  );
};

export default PageTab;
