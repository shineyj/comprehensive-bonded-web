import { CheckCard } from '@ant-design/pro-card';
import ProCard from '@ant-design/pro-card';
import { FireOutlined, RocketOutlined, SendOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';

import style from './index.module.less';
import ProDescriptions from '@ant-design/pro-descriptions';

const columns = [
  {
    title: '系统编号',
    key: 'gModel',
    dataIndex: 'gModel',
  },
  {
    title: '正式单号',
    key: 'gModel',
    dataIndex: 'gModel',
  },
  {
    title: '报文时间',
    key: 'gModel',
    dataIndex: 'gModel',
  },
  {
    title: '处理结果',
    key: 'gModel',
    dataIndex: 'gModel',
  },
  {
    title: '备注',
    key: 'gModel',
    dataIndex: 'gModel',
    span: 24,
  },
];

const approveList = [
  {
    title: '金关审批回执',
    result: '备案申请转人工：2',
    workDate: '2021-11-06 11:30:55',
    type: 'G2',
  },
  {
    title: '金关入库回执',
    result: '备案申请入库成功：Y',
    workDate: '2021-11-06 11:30:55',
    type: 'G2',
  },
  {
    title: '单一技术回执',
    result: '调用成功',
    workDate: '2021-11-06 11:30:23',
    type: 'SD',
  },
  {
    title: '业务申报',
    result: '申报成功',
    workDate: '2021-11-06 11:29:13',
    type: 'APPLY',
  },
];

export default () => {
  const checkCard = approveList.map((i, index) => {
    let icon = <SendOutlined />;
    if (i.type === 'SD') {
      icon = <RocketOutlined />;
    } else if (i.type === 'G2') {
      icon = <FireOutlined />;
    }
    return (
      <CheckCard
        title={i.title}
        description={
          <Row>
            <Col>{i.result}</Col>
            <Col>{`处理时间：${i.workDate}`}</Col>
          </Row>
        }
        value={index}
        avatar={icon}
      />
    );
  });
  return (
    <ProCard split="vertical">
      <ProCard title="" colSpan="25%">
        <CheckCard.Group className="approveList">{checkCard}</CheckCard.Group>
      </ProCard>
      <ProCard headerBordered>
        <div className={style.approveDetail}>
          <table>
            <thead>
              <tr>
                <td colSpan={4} className="{style.title}">
                  业务处理详情
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>系统编号:</th>
                <td />
                <th>正式单号:</th>
                <td />
              </tr>
              <tr>
                <th>报文时间:</th>
                <td />
                <th>处理结果:</th>
                <td />
              </tr>
              <tr>
                <th>备注:</th>
                <td colSpan={3} />
              </tr>
            </tbody>
          </table>
          <table style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>
            <thead>
              <tr>
                <td colSpan={4} className="title">
                  详情
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td />
              </tr>
            </tbody>
          </table>
        </div>
      </ProCard>
    </ProCard>
  );
};

/*
<ProDescriptions
          bordered
          size="middle"
          columns={columns}
          column={2}
          labelStyle={{ width: '165px' }}
        />
*/
