import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import React, { useRef } from 'react';
import type { TableEntList } from '@/services/common/ent/typings';
import { ModalForm } from "@ant-design/pro-form";
import { pageList } from '@/services/common/ent/service';

type ModalProps = {
  modalVisible: boolean
  onSubmit: (values: TableEntList) => Promise<void>;
  onCancel: (flag?: boolean) => void;
};

//模态窗列表双击选择经营企业，并回填
const ModalSelectEntInfor: React.FC<ModalProps> = (props) => {

  const columns: ProColumns<TableEntList>[] = [
    /*{
      dataIndex: 'index',
      valueType: 'index',
      width: 30,
      align: 'center',
    },*/
    {
      title: '经营企业编码',
      dataIndex: 'tradeCode',
      width: 120,
      align: 'center',
      order: 1,
    },
    {
      title: '经营企业名称',
      dataIndex: 'tradeName',
      width: 200,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '账册编号',
      dataIndex: 'emsNo',
      width: 100,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '账册类型',
      dataIndex: 'emsClass',
      width: 100,
      align: 'center',
      hideInSearch: true,
    },
  ];

  const actionRef = useRef<ActionType>();
  return (
    <ModalForm
      title="选择经营企业"
      width="1200px"
      visible={props.modalVisible}
      layout="horizontal"
      labelAlign="right"
      labelCol={{ style: { width: '140px' } }}
      modalProps={{
        destroyOnClose: true,
        onCancel: () => props.onCancel(),
      }}
      /*隐藏模态窗的按钮*/
      submitter={false}
    >
      <ProTable<TableEntList>
        columns={columns}
        actionRef={actionRef}
        request={pageList}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          x: 1000,
        }}
        bordered
        tableAlertRender={false}
        search={{
          labelWidth: 100,
        }}
        toolBarRender={false}
        onRow={(record) => {
          return {
            onDoubleClick: () => {
              //双击选择记录，并关闭界面
              props.onSubmit(record);
            },
          };
        }}
      />
    </ModalForm>
  );
};

export default ModalSelectEntInfor;
