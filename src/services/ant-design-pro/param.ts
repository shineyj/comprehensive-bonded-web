import { request } from 'umi';

export async function getParamList(paramType: string) {
  const result = await request<API.ApiResult<API.ParameterDict[]>>('/api/parameterList', {
    method: 'GET',
    params: { paramType: paramType },
  });

  return result.data;
}

/** 从指定的参数类型，并选择其中几个参数values */
export async function getParams(paramType: string, values: string[]) {
  const result = await request<API.ApiResult<API.ParameterDict[]>>('/api/parameterList', {
    method: 'GET',
    params: { paramType: paramType, values: values },
  });

  return result.data;
}

export async function getStepParameterList(paramType: string) {
  const result = await request<API.ApiResult<API.ParameterDict[]>>('/api/stepParameterList', {
    method: 'GET',
    params: { paramType: paramType },
  });

  return result.data;
}


