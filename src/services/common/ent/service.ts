import { request } from 'umi';
import { TableEntList } from './typings';

/** 获取列表数据 GET /api/entInfoList */
export async function pageList(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableEntList[];
    total?: number;
    success?: boolean;
  }>('/api/entInfoList', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
