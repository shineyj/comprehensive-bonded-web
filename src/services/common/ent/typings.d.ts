export type TableEntList = {
  tradeCode?: string; //经营企业编码
  tradeName?: string; //经营企业名称
  emsNo?: string; //账册编号
  emsClass?: string;//账册类型
};

export type TableEntListParams = {
  tradeCode?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

