import { request } from 'umi';
import { TableListItem } from './typings';

/** 获取规则列表 GET /api/fpdbAllocEntryHead */
export async function pageList(
  params: { current?: number; pageSize?: number },
  options?: { [key: string]: any },
) {
  return request<{
    data: TableListItem[];
    total?: number;
    success?: boolean;
  }>('/api/fpdbAllocEntryHead', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
