import React from "react";

export type TableListItem = {
  id: string; //id
  preEntryNo?: string; //预录入号
  allocationNo?: string; //申请单编号
  allocationType?: string;//调拨类型
  tradeName?: string; //经营企业名称
  departureBlock?: string; //启运区块
  destinationBlock?: string; //目的区块
  stepId: string; //审批状态
  createDate: string; //创建时间
  declareDate: string; //申报时间
  approveDate: string; //审批时间
  mark: string; //区分标志
  enterpriseBooksNo: string; //账册编号
};

export type TableBodyListItem = {
  id: string; //id
  preEntryNo?: string; //预录入号
  allocationNo?: string; //申请单编号
  productDuty?: string; //商品货号
  productCode?: string; //商品编号
  productName?: string; //商品名称
  specificationModel?: string; //规格型号
  allocaDeclareCode?: string; //申报单位
  allocaLUnit?: string; //法定单位
  allocaLUnitTwo?: string; //第二法定单位
  declareAlNum?: string; //申请调拨数(申报)
  legalAlNum?: string; //申请调拨数(法定)
  legalAlNumTwo?: string; //申请调拨数(第二法定)
  accountChDeclareNum?: string; //记账核扣数(申报)
  accountChLegalNum?: string; //记账核扣数(法定)
  accountChLegalNumTwo?: string; //记账核扣数(第二法定)
  accountAdDeclareNum?: string; //记账预扣数(申报)
  accountAdLegalNum?: string; //记账预扣数(法定)
  accountAdLegalNumTwo?: string; //记账预扣数(第二法定)
  actualAlDeclareNum?: string; //实际调拨数(申报)
  actualAlLegalNum?: string; //实际调拨数(法定)
  actualAlLegalNumTwo?: string; //实际调拨数(第二法定)
  accountUDeclareNumT?: string; //已记账未调拨数(申报)
  accountULegalNum?: string; //已记账未调拨数(法定)
  accountULegalNumTwo?: string; //已记账未调拨数(第二法定)
  extendField1?: string; //备案序号
  remark?: string; //备注
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  preEntryNo?: string;
  allocationNo?: string;
  allocationType?: string;
  departureBlock?: string;
  destinationBlock?: string;
  stepId?: string;
  createDateS?: string;
  declareDateS?: string;
  pageType?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

export type AdvancedState = {
  operationKey: string;
  tabActiveKey: string;
};

type FrmFpdbAllocEntryHeadList = {
  id: React.Key;
  allocationNo?: string;//申请单编号
  productCode?: string;//商品编码
  productDuty?: string;//商品序号
  productName?: string;//商品名称
  specificationModel?: string;//规格型号
  allocaDeclareCodeName?: string;//申报单位
  allocaLegalUnitName?: string;//法定单位
  allocaLegalUnitTwoName?: string;//第二法定单位
  allocaDeclareNumSj?: string;//申请调拨数(申报)
  allocaLegalNumSj?: string;//申请调拨数(法定)
  allocaLegalNumTwoSj?: string;//申请调拨数(第二法定)
  allocaUnit?: string;//申报价格
  extendField1?: string;//备案序号
};
